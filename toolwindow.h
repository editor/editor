/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "graphics/toolscene.h"

#include <QMainWindow>
#include <QButtonGroup>
#include <QCheckBox>
#include <QFont>
#include <QLabel>
#include "toolview.h"
#include "toolbox.h"
#include "utils/log.h"
#include <QButtonGroup>
#include <QCheckBox>
#include <QFont>
#include <QLabel>

#include "editdialog.h"


class ToolWindow :
  public QMainWindow
{
  Q_OBJECT
  DECLARE_CLASS_LOGGER

private:
enum ButtonIds
{
  BID_1 = 1,
  BID_2
};

enum ColorButtonId
{
  CB_BACK,
  CB_WHITE,
  CB_RED,
  CB_YELLOW,
  CB_BLUE,
  CB_GREEN,
  _CB_MAX,
};

public:
  ToolWindow(QWidget *parent = 0);
  ~ToolWindow();

private:
  QWidget* setupScreenLayout();
  QWidget* DoScreenLayout();

private:
static QColor GetColor(ColorButtonId btn);

private:
graphics::ToolScene m_scene;
ui::ToolView* m_view;
EditDialog* m_getText;
QButtonGroup* m_imageButtons;

QFont m_textFont;
ui::ZoomTool _zoomTool;
QLabel* m_toolState;
QLabel* m_dirty;

private slots:
  void DirtyChanged(bool dirty);

  void LoadBackgroundImage();

void NewFile();
void LoadFile();
void SaveFile() {ui::SaveFileTool().Apply(m_scene);}
//      void SaveToSvg();
void LoadFromXml();
void SaveToXml();
void ZoomIn() {ui::ZoomInTool(_zoomTool).Apply(*m_view);}
void ZoomOut() {ui::ZoomOutTool(_zoomTool).Apply(*m_view);}
void NewArea() {ui::NewAreaTool().Apply(m_scene);}
void NewEllipse() {ui::NewEllipseTool().Apply(m_scene);}

void NewText() {ui::NewTextTool().Apply(m_scene);}
void NewPen() {ui::NewPenTool().Apply(m_scene);}
void Select() {ui::SelectTool().Apply(m_scene);}
void Delete() {ui::DeleteTool().Apply(m_scene);}
void MultiPt() {ui::MultiPtTool().Apply(m_scene);}
void BkgndImage1(bool checked) {if (!checked) return; ui::BackgroundImageTool(1).Apply(m_scene, *m_view);}
void BkgndImage2(bool checked) {if (!checked) return; ui::BackgroundImageTool(2).Apply(m_scene, *m_view);}
void ColorButtonClicked(int btn);
void BgColorButtonClicked(int btn);
void Finalize();

void StateChanged();

};

#endif // TOOLNWINDOW_H

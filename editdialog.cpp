/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "editdialog.h"
#include "utils/macros.h"

#include <QLineEdit>
#include <QHBoxLayout>

EditDialog::EditDialog(QWidget *parent) :
  QDialog(parent)
{
  m_name = new QLineEdit(this);
  m_Ok = new QPushButton(tr("Ok"),this);
  m_abort = new QPushButton(tr("Cancel"), this);

  QHBoxLayout* vlayout = new QHBoxLayout;
  vlayout->addWidget(m_name);
  vlayout->addWidget(m_Ok);
  vlayout->addWidget(m_abort);
  setLayout(vlayout);
  QCONNECT(m_Ok, SIGNAL(clicked()), this, SLOT(accept()));
  QCONNECT(m_abort, SIGNAL(clicked()), this, SLOT(close()));
}

bool EditDialog::GetText(QString& text)
{
  m_name->setText(text);
  int status = exec();
  text = m_name->text();
  return status;
}

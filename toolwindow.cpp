/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#include "toolwindow.h"

#include "utils/macros.h"

#include <QButtonGroup>
#include <QPushButton>
#include <QRadioButton>
#include <QTextStream>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QMessageBox>
#include <QPainter>
#include "graphics/outline.h"


ToolWindow::ToolWindow(QWidget *parent)
  : QMainWindow(parent)
{
  m_scene.setParent(this);
  //m_scene.SetIGetText(&m_textFont);
  m_textFont.setFamily("Nimbus Sans L");
  m_textFont.setPointSize(14);
  m_scene.SetPropertyValue(graphics::PROP_FONT, m_textFont);
  setWindowTitle(tr("Editor Demo"));

  setCentralWidget(setupScreenLayout());
}

ToolWindow::~ToolWindow()
{

}

QWidget* ToolWindow::setupScreenLayout()
{
  // helper function that does the screen layout
  // returns ptr to layout

  QPushButton* newButton = new QPushButton(tr("New"));
  QPushButton* saveButton = new QPushButton(tr("Save"));
  //QPushButton* saveSvgButton = new QPushButton(tr("Save2Svg"));
  QPushButton* loadXmlButton = new QPushButton(tr("LoadFrmXml"));
  QPushButton* saveXmlButton = new QPushButton(tr("Save2Xml"));
  QPushButton* loadButton = new QPushButton(tr("Load"));
  QPushButton* zoomInButton = new QPushButton(tr("Zoom In"));
  QPushButton* zoomOutButton = new QPushButton(tr("Zoom Out"));
  QPushButton* newAreaButton = new QPushButton(tr("Area"));
  QPushButton* ellipseButton = new QPushButton(tr("Ellipse"));
  QPushButton* textButton = new QPushButton(tr("Text"));
  QPushButton* penButton = new QPushButton(tr("Pen"));
  QPushButton* selectButton = new QPushButton(tr("Select"));
  QPushButton* deleteButton = new QPushButton(tr("Delete"));

  QPushButton* multiPtButton = new QPushButton(tr("Multipoint"));

  QPushButton* finalizeButton = new QPushButton(tr("Finalize"));


  QRadioButton* image1Button = new QRadioButton(tr("Background Image 1"));
  QRadioButton* image2Button = new QRadioButton(tr("Background Image 2"));

  m_toolState = new QLabel();
  m_toolState->setText(CHAR_2_QSTR(m_scene.GetState().GetStateName()));

  m_dirty =new QLabel();
  DirtyChanged(false);

  m_imageButtons = new QButtonGroup;
  m_imageButtons->addButton(image1Button, BID_1);
  m_imageButtons->addButton(image2Button, BID_2);

  const int bw = 100;		// button width
  newButton->setFixedWidth(bw);
  saveButton->setFixedWidth(bw);
  //	saveSvgButton->setFixedWidth(bw);
  loadXmlButton->setFixedWidth(bw);
  saveXmlButton->setFixedWidth(bw);
  loadButton->setFixedWidth(bw);
  zoomInButton->setFixedWidth(bw);
  zoomOutButton->setFixedWidth(bw);
  newAreaButton->setFixedWidth(bw);
  ellipseButton->setFixedWidth(bw);
  textButton->setFixedWidth(bw);
  penButton->setFixedWidth(bw);
  multiPtButton->setFixedWidth(bw);
  selectButton->setFixedWidth(bw);
  deleteButton->setFixedWidth(bw);
  finalizeButton->setFixedWidth(bw);


  QCONNECT(newButton, SIGNAL(clicked()), SLOT(NewFile()));
  QCONNECT(loadButton, SIGNAL(clicked()), SLOT(LoadFile()));
  QCONNECT(saveButton, SIGNAL(clicked()), SLOT(SaveFile()));
  //	QCONNECT(saveSvgButton, SIGNAL(clicked()), SLOT(SaveToSvg()));
  QCONNECT(loadXmlButton, SIGNAL(clicked()), SLOT(LoadFromXml()));
  QCONNECT(saveXmlButton, SIGNAL(clicked()), SLOT(SaveToXml()));
  QCONNECT(zoomInButton, SIGNAL(clicked()), SLOT(ZoomIn()));
  QCONNECT(zoomOutButton, SIGNAL(clicked()), SLOT(ZoomOut()));
  QCONNECT(newAreaButton, SIGNAL(clicked()), SLOT(NewArea()));
  QCONNECT(ellipseButton, SIGNAL(clicked()), SLOT(NewEllipse()));
  QCONNECT(textButton, SIGNAL(clicked()), SLOT(NewText()));
  QCONNECT(penButton, SIGNAL(clicked()), SLOT(NewPen()));
  QCONNECT(multiPtButton, SIGNAL(clicked()), SLOT(MultiPt()));
  QCONNECT(selectButton, SIGNAL(clicked()), SLOT(Select()));
  QCONNECT(deleteButton, SIGNAL(clicked()), SLOT(Delete()));
  QCONNECT(finalizeButton, SIGNAL(clicked()), SLOT(Finalize()));
  QCONNECT(image1Button, SIGNAL(toggled(bool)), SLOT(BkgndImage1(bool)));
  QCONNECT(image2Button, SIGNAL(toggled(bool)), SLOT(BkgndImage2(bool)));
  QCONNECT(&m_scene, SIGNAL(SigStateChanged()), SLOT(StateChanged()));
  QCONNECT(&m_scene, SIGNAL(SigDirtyChanged(bool)), SLOT(DirtyChanged(bool)));

  QGroupBox* fileBox = new QGroupBox(tr("File"));
  QVBoxLayout* fileButtonLayout = new QVBoxLayout;
  fileButtonLayout->addWidget(newButton);
  fileButtonLayout->addWidget(loadButton);
  fileButtonLayout->addWidget(saveButton);
  //	fileButtonLayout->addWidget(saveSvgButton);
  fileButtonLayout->addWidget(loadXmlButton);
  fileButtonLayout->addWidget(saveXmlButton);
  fileButtonLayout->addStretch(0);  // move buttons to top of layout
  fileButtonLayout->setAlignment(Qt::AlignHCenter);
  fileBox->setLayout(fileButtonLayout);

  QGroupBox* viewControlBox = new QGroupBox(tr("View Control"));
  QVBoxLayout* viewButtonLayout = new QVBoxLayout;
  viewButtonLayout->addWidget(zoomInButton);
  viewButtonLayout->addWidget(zoomOutButton);
  viewButtonLayout->addStretch(0);  // move buttons to top of layout
  viewButtonLayout->setAlignment(Qt::AlignHCenter);
  viewControlBox->setLayout(viewButtonLayout);

  QGroupBox* toolBox = new QGroupBox(tr("Tools"));
  QVBoxLayout* toolsButtonLayout = new QVBoxLayout;
  toolsButtonLayout->addWidget(selectButton);
  toolsButtonLayout->addWidget(deleteButton);
  toolsButtonLayout->addWidget(finalizeButton);
  toolsButtonLayout->addStretch(0); // move buttons to top of layout
  toolsButtonLayout->setAlignment(Qt::AlignHCenter);
  toolBox->setLayout(toolsButtonLayout);

  QGroupBox* newBox = new QGroupBox(tr("New"));
  QVBoxLayout* newButtonLayout = new QVBoxLayout;
  newButtonLayout->addWidget(newAreaButton);
  newButtonLayout->addWidget(ellipseButton);
  newButtonLayout->addWidget(textButton);
  newButtonLayout->addWidget(penButton);
  newButtonLayout->addWidget(multiPtButton);
  newButtonLayout->addStretch(0); // move buttons to top of layout
  newButtonLayout->setAlignment(Qt::AlignHCenter);
  newBox->setLayout(newButtonLayout);

  QGroupBox* displayBox = new QGroupBox(tr("Display"));
  QGridLayout* displayBoxLayout = new QGridLayout;
  displayBoxLayout->addWidget(image1Button, 0, 0);
  displayBoxLayout->addWidget(image2Button, 1, 0);
  QGroupBox* stateBox = new QGroupBox(tr("Info"));
  QVBoxLayout* stateLayoutVertical = new QVBoxLayout;
  stateBox->setLayout(stateLayoutVertical);

  QHBoxLayout* stateLayoutHoriz = new QHBoxLayout;
  stateLayoutVertical->addLayout(stateLayoutHoriz);
  stateLayoutHoriz->setAlignment(Qt::AlignHCenter);
  stateLayoutHoriz->addWidget(new QLabel("State:"));
  stateLayoutHoriz->addWidget(m_toolState);

  QHBoxLayout* dirtyLayout = new QHBoxLayout;
  stateLayoutVertical->addLayout(dirtyLayout);
  dirtyLayout->setAlignment(Qt::AlignHCenter);
  dirtyLayout->addWidget(new QLabel("Dirty:"));
  dirtyLayout->addWidget(m_dirty);

  displayBoxLayout->setHorizontalSpacing(50);
  displayBoxLayout->setAlignment(Qt::AlignHCenter);
  displayBox->setLayout(displayBoxLayout);

  QHBoxLayout* colorBox = new QHBoxLayout();
  QButtonGroup* bg = new QButtonGroup(this);
  QCONNECT(bg, SIGNAL(buttonClicked(int)), SLOT(ColorButtonClicked(int)));

  QHBoxLayout* bgColorBox = new QHBoxLayout();
  QButtonGroup* bgBack = new QButtonGroup(this);
  QCONNECT(bgBack, SIGNAL(buttonClicked(int)), SLOT(BgColorButtonClicked(int)));

  for (unsigned i = 0; i < _CB_MAX; ++i)
  {
    QPushButton* b = new QPushButton();
    colorBox->addWidget(b);
    bg->addButton(b, i);
    b->setStyleSheet(QString("background-color: %1").arg(GetColor((ColorButtonId)(i)).name()));

    b = new QPushButton();
    bgColorBox->addWidget(b);
    bgBack->addButton(b, i);
    b->setStyleSheet(QString("background-color: %1").arg(GetColor((ColorButtonId)(i)).name()));
  }

  m_view = new ui::ToolView();
  m_view->setMouseTracking(true);
  m_view->setScene(&m_scene.GetGraphicsScene());
  m_view->setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);
  m_view->setObjectName("ToolView");

  QGridLayout* screenLayout = new QGridLayout;
  screenLayout->addWidget(fileBox, 0, 0);
  screenLayout->addWidget(viewControlBox, 0, 1);
  screenLayout->addWidget(toolBox, 0, 2);
  screenLayout->addWidget(newBox, 0, 4, 2, 1);
  screenLayout->addWidget(displayBox, 1, 0, 1, 3);
  screenLayout->addWidget(stateBox, 1, 3, 1, 1);
  screenLayout->addWidget(m_view, 2, 0, 1, 5);	// extend accross all columns
  screenLayout->addLayout(colorBox, 3, 0, 1, 5);
  screenLayout->addLayout(bgColorBox, 4, 0, 1, 5);

  // the view's height is the only height allowed to change when resizing
  //screenLayout->setRowStretch(0, 0);
  //screenLayout->setRowStretch(1, 0);
  screenLayout->setRowStretch(2, 1);

  QWidget* layoutWidget = new QWidget;
  layoutWidget->setLayout(screenLayout);

  // notify tool scene of initial states
  image1Button->setChecked(true); // default to background image 1

  m_getText = new EditDialog(this);
  m_scene.SetIGetText(m_getText);


  return layoutWidget;
}

void ToolWindow::DirtyChanged(bool dirty)
{
  if (dirty)
  {
    m_dirty->setText("Dirty");
  }
  else
  {
    m_dirty->setText("Untouched");
  }
}

QColor ToolWindow::GetColor(ColorButtonId btn)
{
  switch (btn)
  {
    // TODO alph blending -> colors.. frame, content..

  case CB_BACK:   return Qt::black;
  case CB_WHITE:  return QColor(255, 255, 255, 200);
  case CB_RED:    return Qt::red;
  case CB_YELLOW: return Qt::yellow;
  case CB_BLUE:   return Qt::blue;
  case CB_GREEN:  return Qt::green;
  default: ASSERT(false); return Qt::black;
  }
}

void ToolWindow::StateChanged()
{
  m_toolState->setText(m_scene.GetState().GetStateName());
}

void ToolWindow::NewFile()
{
  //  _zoomTool.ResetScale();
  //NewFileTool().Apply(_scene, *_view);
  LoadBackgroundImage();
}

void ToolWindow::LoadFile()
{
  //  _zoomTool.ResetScale();
  //  NewFileTool().Apply(_scene, *_view);
  //  LoadBackgroundImage();
}

void ToolWindow::Finalize()
{
  m_scene.FinalizeState();
}

void ToolWindow::ColorButtonClicked(int btn)
{
  ColorButtonId b = (ColorButtonId)(btn);

  QPen pen(GetColor(b));
  pen.setWidth(3);

  int l = qGray(pen.color().red(), pen.color().green(), pen.color().blue());
  QColor c = (l <= 128) ? QColor(255, 255, 255, 128) : QColor(0, 0, 0, 128);
  graphics::Outline outline(c, pen.widthF() + 4);

  QVariant var;
  var.setValue(outline);
  m_scene.SetSelectedItemsPropertyValue(graphics::PROP_PEN, pen);
  m_scene.SetSelectedItemsPropertyValue(graphics::PROP_OUTLINE, var);
  m_scene.SetPropertyValue(graphics::PROP_PEN, pen);
  m_scene.SetPropertyValue(graphics::PROP_OUTLINE, var);
}

void ToolWindow::BgColorButtonClicked(int btn)
{
  ColorButtonId b = (ColorButtonId)(btn);
  m_scene.SetSelectedItemsPropertyValue(graphics::PROP_BRUSH, QBrush(GetColor(b)));
  m_scene.SetPropertyValue(graphics::PROP_BRUSH, QBrush(GetColor(b)));
}

void ToolWindow::LoadFromXml()
{
  try
  {
    m_scene.Load("/tmp/graphics.xml");
  }
  catch (const std::exception& ex)
  {
    //TODO enable logging
    QMessageBox::critical(this, "Error", QString(typeid(ex).name()) + ex.what());
    LOG_ERROR("%s", ex.what());
  }
}

void ToolWindow::SaveToXml()
{
  m_scene.Save("/tmp/graphics.xml");
}

void ToolWindow::LoadBackgroundImage()
{
  // Set background Image based on checked button
  if (m_imageButtons)
  {
    int id = m_imageButtons->checkedId();
    if (BID_1 == id)
    {
      BkgndImage1(true);
    }
    else if (BID_2 == id)
    {
      BkgndImage2(true);
    }
    else
    {
      LOG_WARN("Button id %d not valid", id);
    }
  }
}



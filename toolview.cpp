/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "toolview.h"
#include "graphics/toolscene.h"

namespace ui
{

DEFINE_CLASS_LOGGER(ToolView);

void ToolView::resizeEvent(QResizeEvent* event)
{
  UNUSED_VAR(event);
  QRectF rect = scene()->sceneRect();
  centerOn(rect.width() / 2.0 + rect.x(), rect.height() / 2.0 + rect.y());
  //graphics::ToolScene *ts = dynamic_cast<graphics::ToolScene*>(scene());
  //TODO center the view
}

}

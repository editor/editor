/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef EDITDIALOG_H
#define EDITDIALOG_H

#include <QDialog>
#include <QPushButton>
#include <QLineEdit>
#include "graphics/igettext.h"

class EditDialog :
  public QDialog,
  public graphics::IGetText
{
  Q_OBJECT
public:
  explicit EditDialog(QWidget *parent = 0);
  virtual bool GetText(QString& text);

private:
  QLineEdit* m_name;
  QPushButton* m_Ok;
  QPushButton* m_abort;
};

#endif

#-------------------------------------------------
#
# Project created by QtCreator 2014-11-23T19:47:32
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Editor
TEMPLATE = app
CONFIG += c++17


SOURCES += main.cpp \
    toolwindow.cpp \
    graphics/toolscene.cpp \
    utils/log.cpp \
    toolbox.cpp \
    toolview.cpp \
    graphics/graphicsscene.cpp \
    graphics/items/abstractitem.cpp \
    graphics/items/abstractpolygonitem.cpp \
    graphics/items/areaitem.cpp \
    graphics/items/customitemtype.cpp \
    graphics/items/ellipseitem.cpp \
    graphics/items/itemregistry.cpp \
    graphics/items/penitem.cpp \
    graphics/items/polylineitem.cpp \
    graphics/items/textitem.cpp \
    graphics/itemstate/abstractitemstate.cpp \
    graphics/itemstate/nodeselectitemstate.cpp \
    graphics/nodeitem/customitemtypeprivate.cpp \
    graphics/nodeitem/editnodeitem.cpp \
    graphics/scenestates/abstractnewtoolscenestate.cpp \
    graphics/scenestates/abstractscenestate.cpp \
    graphics/scenestates/idlescenestate.cpp \
    graphics/scenestates/newareascenestate.cpp \
    graphics/scenestates/newellipsescenestate.cpp \
    graphics/scenestates/newpenscenestate.cpp \
    graphics/scenestates/newpolylinescenestate.cpp \
    graphics/scenestates/newtextscenestate.cpp \
    graphics/scenestates/selectscenestate.cpp \
    graphics/persistency/elements/abstractpolygonitempersistency.cpp \
    graphics/persistency/elements/areaitempersistency.cpp \
    graphics/persistency/elements/ellipseitempersistency.cpp \
    graphics/persistency/elements/propertiespersistency.cpp \
    graphics/persistency/elements/outlinepersistency.cpp \
    graphics/persistency/elements/qbrushpersistency.cpp \
    graphics/persistency/elements/qcolorpersistency.cpp \
    graphics/persistency/elements/qdatepersistency.cpp \
    graphics/persistency/elements/qdatetimepersistency.cpp \
    graphics/persistency/elements/qfontpersistency.cpp \
    graphics/persistency/elements/qpenpersistency.cpp \
    graphics/persistency/elements/penitempersistency.cpp \
    graphics/persistency/elements/polylineitempersistency.cpp \
    graphics/persistency/elements/qstringpersistency.cpp \
    graphics/persistency/elements/textitempersistency.cpp \
    graphics/persistency/elements/qtpenstyle2charconvtable.cpp \
    graphics/persistency/elements/qtpenjoinstyle2charconvtable.cpp \
    graphics/persistency/elements/qtpencapstyle2charconvtable.cpp \
    graphics/persistency/elements/qtbrushstyle2charconvtable.cpp \
    graphics/persistency/abstractcontentreader.cpp \
    graphics/persistency/abstractdocumentreader.cpp \
    graphics/persistency/abstractelementreader.cpp \
    graphics/persistency/abstractitempersistency.cpp \
    graphics/persistency/abstractreader.cpp \
    graphics/persistency/defaultcreateitemreadervisitor.cpp \
    graphics/persistency/defaultprintitemvisitor.cpp \
    graphics/persistency/documentprinter.cpp \
    graphics/persistency/elementprinter.cpp \
    graphics/persistency/stackhandler.cpp \
    graphics/persistency/toolsceneitemspersistency.cpp \
    graphics/persistency/toolscenepersistency.cpp \
    graphics/persistency/xercesinitguard.cpp \
    graphics/persistency/xercespanichandler.cpp \
    graphics/persistency/xmlprinter.cpp \
    editdialog.cpp

HEADERS  += \
    toolwindow.h \
    graphics/toolscene.h \
    utils/baseitem.h \
    utils/macros.h \
    graphics/property.h \
    graphics/igettext.h \
    graphics/iproperties.h \
    utils/assert.h \
    utils/cast.h \
    utils/defaultfactory.h \
    utils/ifactory.h \
    utils/log.h \
    utils/nullmutex.h \
    utils/registrar.h \
    utils/registry.h \
    utils/singleton.h \
    utils/types.h \
    utils/unknownkeyexception.h \
    utils/basevisitor.h \
    toolbox.h \
    toolview.h \
    graphics/qpointlist.h \
    graphics/qitemlist.h \
    graphics/outline.h \
    graphics/graphicsscene.h \
    graphics/items/abstractitem.h \
    graphics/items/abstractpolygonitem.h \
    graphics/items/areaitem.h \
    graphics/items/customitemtype.h \
    graphics/items/ellipseitem.h \
    graphics/items/itemlist.h \
    graphics/items/itemregistry.h \
    graphics/items/penitem.h \
    graphics/items/polylineitem.h \
    graphics/items/textitem.h \
    graphics/itemstate/abstractitemstate.h \
    graphics/itemstate/nodeselectitemstate.h \
    graphics/itemstate/notselecteditemstate.h \
    graphics/itemstate/simpleselectitemstate.h \
    graphics/nodeitem/customitemtypeprivate.h \
    graphics/nodeitem/editnodeitem.h \
    graphics/scenestates/abstractnewtoolscenestate.h \
    graphics/scenestates/abstractscenestate.h \
    graphics/scenestates/autoqpointer.h \
    graphics/scenestates/idlescenestate.h \
    graphics/scenestates/newareascenestate.h \
    graphics/scenestates/newellipsescenestate.h \
    graphics/scenestates/newpenscenestate.h \
    graphics/scenestates/newpolylinescenestate.h \
    graphics/scenestates/newtextscenestate.h \
    graphics/scenestates/selectscenestate.h \
    graphics/persistency/elements/qfontpersistency.h \
    graphics/ThrowExceptionCatchAll.h \
    graphics/persistency/utilpersistency.h \
    graphics/persistency/elements/abstractpolygonitempersistency.h \
    graphics/persistency/elements/areaitempersistency.h \
    graphics/persistency/containeradapterfactory.h \
    graphics/persistency/containerpersistency.h \
    graphics/persistency/elements/ellipseitempersistency.h \
    graphics/persistency/elements/propertiespersistency.h \
    graphics/persistency/elements/enumpersistency.h \
    graphics/persistency/elements/numberpersistency.h \
    graphics/persistency/elements/outlinepersistency.h \
    graphics/persistency/elements/qbrushpersistency.h \
    graphics/persistency/elements/qcolorpersistency.h \
    graphics/persistency/elements/qdatepersistency.h \
    graphics/persistency/elements/qdatetimepersistency.h \
    graphics/persistency/elements/qfontpersistency.h \
    graphics/persistency/elements/qlinepersistency.h \
    graphics/persistency/elements/qpenpersistency.h \
    graphics/persistency/elements/qpointpersistency.h \
    graphics/persistency/elements/penitempersistency.h \
    graphics/persistency/elements/polylineitempersistency.h \
    graphics/persistency/elements/qpolygonpersistency.h \
    graphics/persistency/elements/qsizepersistency.h \
    graphics/persistency/elements/qstringpersistency.h \
    graphics/persistency/elements/textitempersistency.h \
    graphics/persistency/elements/stringpersistency.h \
    graphics/persistency/elements/qtpenstylepersistency.h \
    graphics/persistency/elements/qtpenstyle2charconvtable.h \
    graphics/persistency/elements/qtpenjoinstylepersistency.h \
    graphics/persistency/elements/qtbrushstyle2charconvtable.h \
    graphics/persistency/elements/qtbrushstylepersistency.h \
    graphics/persistency/elements/qtpencapstyle2charconvtable.h \
    graphics/persistency/elements/qtpencapstylepersistency.h \
    graphics/persistency/elements/qtpenjoinstyle2charconvtable.h \
    graphics/persistency/toolsceneitemspersistency.h \
    graphics/persistency/abstractcontentreader.h \
    graphics/persistency/abstractdocumentreader.h \
    graphics/persistency/abstractelementreader.h \
    graphics/persistency/abstractitempersistency.h \
    graphics/persistency/abstractprintvisitor.h \
    graphics/persistency/abstractreader.h \
    graphics/persistency/defaultcreateitemreadervisitor.h \
    graphics/persistency/defaultprintitemvisitor.h \
    graphics/persistency/documentprinter.h \
    graphics/persistency/documentreader.h \
    graphics/persistency/elementprinter.h \
    graphics/persistency/enumstringconversiontable.h \
    graphics/persistency/heapallocateditem.h \
    graphics/persistency/initialreader.h \
    graphics/persistency/pairassociativecontaineradapter.h \
    graphics/persistency/parseexception.h \
    graphics/persistency/serializeexception.h \
    graphics/persistency/simpleassociativecontaineradapter.h \
    graphics/persistency/stackhandler.h \
    graphics/persistency/standardcontaineradapter.h \
    graphics/persistency/toolscenepersistency.h \
    graphics/persistency/xercesinitguard.h \
    graphics/persistency/xercespanichandler.h \
    graphics/persistency/xercestranscodedstring.h \
    graphics/persistency/xmlchstring.h \
    graphics/persistency/abstractcreatereadervisitor.h \
    graphics/persistency/xmlprinter.h \
    graphics/throwexceptioncatchall.h \
    editdialog.h

unix {
 # LIBS += -lboost_system
LIBS += -lboost_filesystem
 LIBS += -lxerces-c-3.2

}

RESOURCES += \
    res.qrc

OTHER_FILES += \
    images/AreaCursor.png \
    images/AreaMeasureCursor.png \
    images/arrow.png \
    images/background.png \
    images/DistanceCursor.png \
    images/EllipseCursor.png \
    images/PenCursor.png \
    images/SelectCursor.png

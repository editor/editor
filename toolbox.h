/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _TOOLBOX_H_
#define _TOOLBOX_H_

#include "utils/log.h"
#include "graphics/items/abstractitem.h"

//TODO put classes in seperate files

namespace ui
{

class ToolView;

class BackgroundImageTool
{
  DECLARE_CLASS_LOGGER

  public:
    BackgroundImageTool( int imageIndex ) : m_imageIndex(imageIndex) {};

public:
  void Apply(graphics::ToolScene& scene, ui::ToolView& view) const;

protected:
  void AddPixmap(graphics::ToolScene& scene, const QString& fileName, ui::ToolView& view) const;

private:
  constexpr static qreal _defaultImageOpacity {0.5};
  const int m_imageIndex;
};

class DeleteTool
{
  DECLARE_CLASS_LOGGER

  public:
    void Apply(graphics::ToolScene& scene) const;
};


class LoadFileTool
{
  DECLARE_CLASS_LOGGER

  public:
    void Apply(graphics::ToolScene& scene /*, filepath??? <-- or put that in constructor*/) const;

private:
  void SetScene(graphics::ToolScene& scene) const;
};

class MultiPtTool
{
  DECLARE_CLASS_LOGGER

  public:
    void Apply(graphics::ToolScene& scene) const;
};

class NewAreaTool
{
  DECLARE_CLASS_LOGGER

  public:
    void Apply(graphics::ToolScene& scene) const;
};

class NewEllipseTool
{
  DECLARE_CLASS_LOGGER

  public:
    void Apply(graphics::ToolScene& scene) const;
};

class NewFileTool
{
  DECLARE_CLASS_LOGGER

  public:
    void Apply(graphics::ToolScene& scene, ui::ToolView& view) const;

};


class NewPenTool
{
  DECLARE_CLASS_LOGGER

  public:
    void Apply(graphics::ToolScene& scene) const;
};

class NewTextTool
{
  DECLARE_CLASS_LOGGER

  public:
    void Apply(graphics::ToolScene& scene) const;
};

class SaveFileTool
{
  DECLARE_CLASS_LOGGER

  public:
    void Apply(graphics::ToolScene& scene) const;

private:
  void SaveScene(graphics::ToolScene&, QByteArray&) const;
};

class SelectTool
{
  DECLARE_CLASS_LOGGER

  public:
    void Apply(graphics::ToolScene& scene) const;
};

class ZoomTool
{
  DECLARE_CLASS_LOGGER

  friend class ZoomInTool;
  friend class ZoomOutTool;

public:
  ZoomTool() : _scale(1.0) {}
  void ResetScale() {_scale=1.0;}

  qreal Scale() const { return _scale; }

private:
  qreal _scale;
  constexpr static  qreal _scaleMin{.5};
  constexpr static qreal _scaleFactor{1.5};
  constexpr static qreal _scaleMax{100.};
};

class ZoomInTool
{
  DECLARE_CLASS_LOGGER

  public:
    ZoomInTool(ZoomTool& zoomTool) : _zoomTool(zoomTool) {};

public:
  void Apply(ui::ToolView& view);

private:
  ZoomTool& _zoomTool;
};

class ZoomOutTool
{
  DECLARE_CLASS_LOGGER
  public:
    ZoomOutTool(ZoomTool& zoomTool) : _zoomTool(zoomTool) {};

public:
  void Apply(ui::ToolView& view);

private:
  ZoomTool& _zoomTool;
};
}
#endif

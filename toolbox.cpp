/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <iostream>
#include <fstream>
#include <cstdlib>

#include <QBrush>
#include <QFile>
#include <QPen>
#include <QSize>
#include <QString>
#include "utils/cast.h"
#include "utils/assert.h"
#include "utils/log.h"
#include "utils/macros.h"
#include "graphics/items/ellipseitem.h"
#include "graphics/items/penitem.h"
#include "graphics/items/textitem.h"
#include "graphics/toolscene.h"
#include "graphics/scenestates/abstractscenestate.h"
#include "graphics/scenestates/idlescenestate.h"
#include "graphics/scenestates/newareascenestate.h"
#include "graphics/scenestates/newellipsescenestate.h"
#include "graphics/scenestates/newpenscenestate.h"
#include "graphics/scenestates/newpolylinescenestate.h"
#include "graphics/scenestates/newtextscenestate.h"
#include "graphics/scenestates/selectscenestate.h"
#include "toolbox.h"
#include "toolview.h"
#include "utils/macros.h"
#include <QImage>

#include "utils/types.h"

#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>

#define LINE_WIDTH 4

namespace ui
{

QImage imageFile;

DEFINE_CLASS_LOGGER(BackgroundImageTool);

void BackgroundImageTool::Apply(graphics::ToolScene& scene, ui::ToolView& view) const
{
  try
  {
    if (1 == m_imageIndex)
    {
      AddPixmap(scene, ":/images/background.png", view);  // add to the scene
      imageFile.load(":/images/background.png");          // load the chunks file
    }
    else if (2 == m_imageIndex)
    {
      scene.SetBackgroundImage(QPixmap());
    }
    else
    {
      LOG_WARN("Background image id not valid");
    }
  }
  catch (const std::exception& ex)
  {
    LOG_ERROR("%s", ex.what());
  }
}

void BackgroundImageTool::AddPixmap(graphics::ToolScene& scene, const QString& fileName, ui::ToolView& view) const
{
  QPixmap pixmap;
  VERIFY(pixmap.load(fileName));
  scene.SetBackgroundImage(pixmap);
  view.centerOn(QPointF(pixmap.width() / 2.0, pixmap.height() / 2.0));
  //scene.CenterBackgroundImage();				// center for demo
}

DEFINE_CLASS_LOGGER(DeleteTool);

void DeleteTool::Apply(graphics::ToolScene& ts) const
{
  try
  {
    ts.RemoveSelectedItems();
  }
  catch (const std::exception& ex)
  {
    LOG_ERROR("%s", ex.what());
  }
}

DEFINE_CLASS_LOGGER(LoadFileTool);

void LoadFileTool:: Apply(graphics::ToolScene& scene) const
{
  UNUSED_VAR(scene);

}

void LoadFileTool::SetScene(graphics::ToolScene& ts) const
{
  UNUSED_VAR(ts);

  //TODO remove this
}

DEFINE_CLASS_LOGGER(MultiPtTool);

void MultiPtTool::Apply(graphics::ToolScene& scene) const
{
  try
  {
    scene.ChangeState(graphics::scenestate::AbstractSceneStateAP(new graphics::scenestate::NewPolylineSceneState()));
  }
  catch (const std::exception& ex)
  {
    LOG_ERROR("%s", ex.what());
  }
}

DEFINE_CLASS_LOGGER(NewAreaTool);

void NewAreaTool::Apply(graphics::ToolScene& scene) const
{
  try
  {
    scene.ChangeState(graphics::scenestate::AbstractSceneStateAP(new graphics::scenestate::NewAreaSceneState()));
  }
  catch (const std::exception& ex)
  {
    LOG_ERROR("%s", ex.what());
  }
}


DEFINE_CLASS_LOGGER(NewEllipseTool);

void NewEllipseTool::Apply(graphics::ToolScene& scene) const
{
  try
  {
    scene.ChangeState(graphics::scenestate::AbstractSceneStateAP(new graphics::scenestate::NewEllipseSceneState()));
  }
  catch (const std::exception& ex)
  {
    LOG_ERROR("%s", ex.what());
  }
}


DEFINE_CLASS_LOGGER(NewFileTool);

void NewFileTool::Apply(graphics::ToolScene& scene, ui::ToolView& view) const
{
  try
  {
    scene.RemoveAllItems();
    view.resetTransform();
    //imageFile.clear();
  }
  catch (const std::exception& ex)
  {
    LOG_ERROR("%s", ex.what());
  }
}


DEFINE_CLASS_LOGGER(NewPenTool);

void NewPenTool::Apply(graphics::ToolScene& scene) const
{
  try
  {
    scene.ChangeState(graphics::scenestate::AbstractSceneStateAP(new graphics::scenestate::NewPenSceneState(100)));
  }
  catch (const std::exception& ex)
  {
    LOG_ERROR("%s", ex.what());
  }
}


DEFINE_CLASS_LOGGER(NewTextTool);

void NewTextTool::Apply(graphics::ToolScene& scene) const
{
  try
  {
    scene.ChangeState(graphics::scenestate::AbstractSceneStateAP(new graphics::scenestate::NewTextSceneState()));
  }
  catch (const std::exception& ex)
  {
    LOG_ERROR("%s", ex.what());
  }
}


DEFINE_CLASS_LOGGER(SaveFileTool);

void SaveFileTool::Apply(graphics::ToolScene& scene) const
{
  try
  {

    scene.DeselectAll();
    QGraphicsScene& myScene = scene.GetGraphicsScene();
    QImage image(myScene.sceneRect().size().toSize(), QImage::Format_ARGB32);   // Create the image with the exact size of the shrunk scene
    image.fill(Qt::transparent);                                                // Start all pixels transparent
    QPainter painter(&image);
    myScene.render(&painter);
    image.save("/home/mry/file_name.png");
  }
  catch (const std::exception& ex)
  {
    LOG_ERROR("%s", ex.what());
  }
}

void SaveFileTool::SaveScene(graphics::ToolScene& scene, QByteArray& qba) const
{
  UNUSED_VAR(scene);
  UNUSED_VAR(qba);

    /*
  try
  {
    std::stringstream ss(std::stringstream::out);

    if (!ss.good())
      throw std::runtime_error("Unable to open scene output stream.");

    ss.exceptions (std::ios::failbit | std::ios::badbit | std::ios::eofbit);
    boost::archive::xml_oarchive oa(ss);
    oa << boost::serialization::make_nvp("scene", *scene);
    qba.append(ss.str().data(), (int)ss.str().size());
  //TODO
    QFile qf("sstream.xml");
    qf.open(QIODevice::WriteOnly);
    quint64 nOut = qf.write(qba);
    LOG_DEBUG("QByteArray file size is %lu", nOut);
  }
  catch (const std::exception& ex)
  {
    LOG_ERROR("%s", ex.what());
  }
  */
}



DEFINE_CLASS_LOGGER(SelectTool);

void SelectTool::Apply(graphics::ToolScene& scene) const
{
  try
  {
    scene.ChangeState(graphics::scenestate::AbstractSceneStateAP(new graphics::scenestate::SelectSceneState()));
  }
  catch (const std::exception& ex)
  {
    LOG_ERROR("%s", ex.what());
  }
}

DEFINE_CLASS_LOGGER(ZoomInTool);

void ZoomInTool::Apply(ui::ToolView& view)
{
  try
  {
    qreal newScale = _zoomTool._scale*  _zoomTool._scaleFactor;
    if (_zoomTool._scaleMax >= newScale)
    {
      qreal sc = newScale / _zoomTool._scale;
      view.scale(sc, sc);
      _zoomTool._scale = newScale;
      //view->centerOn(rect().center());
      LOG_DEBUG("scale: %.3f", _zoomTool._scale);
    }
  }
  catch (const std::exception& ex)
  {
    LOG_ERROR("%s", ex.what());
  }
}


DEFINE_CLASS_LOGGER(ZoomOutTool);

void ZoomOutTool::Apply(ui::ToolView& view)
{
  try
  {
    qreal newScale = _zoomTool._scale / _zoomTool._scaleFactor;
    if (_zoomTool._scaleMin <= newScale)
    {
      qreal sc = newScale / _zoomTool._scale;
      view.scale(sc, sc);
      _zoomTool._scale = newScale;
      //view->centerOn(rect().center());
      LOG_DEBUG("scale: %.3f", _zoomTool._scale);
    }
  }
  catch (const std::exception& ex)
  {
    LOG_ERROR("%s", ex.what());
  }
}


}

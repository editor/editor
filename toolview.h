/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _TOOLVIEW_H_
#define _TOOLVIEW_H_

#include <QGraphicsView>
#include <QResizeEvent>
#include <QWidget>
#include "utils/log.h"

namespace ui
{

class ToolView :
  public QGraphicsView
{

  DECLARE_CLASS_LOGGER

  public:
    ToolView(QWidget* parent = NULL) :
  QGraphicsView(parent)
  {}

private:
  virtual void resizeEvent(QResizeEvent* event);
};
}
#endif

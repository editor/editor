/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _UNKNOWNKEYEXCEPTION_H_
#define _UNKNOWNKEYEXCEPTION_H_

#include <stdexcept>
#include "utils/types.h"

namespace pattern
{

/** The UnknownKeyException is thrown by the Registry in case no factory has been found for a particular key.*/
class UnknownKeyException :
    public std::logic_error
{
public:
  UnknownKeyException(const STRING& msg) :
    std::logic_error(msg)
  {}
};

}

#endif


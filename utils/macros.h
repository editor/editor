/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _MACROS_H_
#define _MACROS_H_

#include <stdexcept>
#include <QString> // needed to to convert PATH -> CHAR
#include <QCoreApplication>


#ifdef SUPPORT_ASSERT


/** Interrupt current process.
  * Usually, this happs, because an assertion is broken.
  * @param file Filename of source code file.
  * @param function Name of function, which wants to interrupt the program.
  * @param condition The broken condition as string */
void AssertFailed(const char* file, const char* function, int line, const char* condition);


#define ASSERT(CONDITION)                                                       \
{                                                                               \
  try                                                                           \
{                                                                               \
  if (!(CONDITION))                                                             \
{                                                                               \
  ::util::AssertFailed(__FILE__, __FUNCTION__, __LINE__, #CONDITION);           \
  }                                                                             \
  }                                                                             \
  catch (const std::exception& ex)                                              \
{                                                                               \
  ::util::AssertFailed(__FILE__, __FUNCTION__, __LINE__, ex.what());            \
  }                                                                             \
  catch (...)                                                                   \
{                                                                               \
  ::util::AssertFailed(__FILE__, __FUNCTION__, __LINE__, "unknown exception");  \
  }                                                                             \
  }

#define VERIFY(CONDITION) ASSERT(CONDITION)

#else

#define ASSERT(CONDITION)

#define VERIFY(CONDITION)  \
{                          \
  if (CONDITION) {}        \
  }
#endif



#define CHAR_2_QSTR(STR)          (QString::fromUtf8((STR)))
#define PATH_2_QSTR(PATH)         (QString::fromLocal8Bit((PATH).string().c_str()))
#define QSTR_2_CHAR(STR)          ((STR).toLocal8Bit().data())
#define QSTR_2_PATH(STR)          (boost::filesystem::path((STR).toLocal8Bit().data()))
#define QSTR_2_STR(STR)           (STRING((STR).toUtf8().data()))
#define STR_2_QSTR(STR)           (QString::fromUtf8((STR).c_str()))

#define PATH_2_CHAR(PATH)         ((PATH).string().c_str())

#define QCONNECT(SENDER, SIGNL, ARG3, ...) (VERIFY(::QObject::connect(SENDER, SIGNL, ARG3, ##__VA_ARGS__)))

/** Concate two expressions */
#define CONCAT(x, y) __CONCAT(x, y)
#define __CONCAT(x, y) x ## y

#define UNUSED_VAR(x) {(void)x;}


#endif

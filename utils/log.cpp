/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "utils/log.h"
#if defined(LOGGING_FRAMEWORK_LOG4CPP)
#include <log4cpp/FileAppender.hh>
#include <log4cpp/PatternLayout.hh>
#endif
#if defined(LOGGING_FRAMEWORK_SYSLOG)
#include <string.h>
#include <base/util/Macros.h>
#endif

namespace logging
{

void InitLoggingFramework(
    #if defined(LOGGING_FRAMEWORK_LOG4CPP)
    const STRING* logfile
    #elif defined(LOGGING_FRAMEWORK_SYSLOG)
    const STRING& ident, bool printToStdErr
    #endif
    )
{
#if defined(LOGGING_FRAMEWORK_LOG4CPP)

  log4cpp::Category& root = log4cpp::Category::getRoot();
  root.setPriority(log4cpp::Priority::DEBUG);

  log4cpp::PatternLayout* layout = new log4cpp::PatternLayout();
  layout->setConversionPattern("%10t | %d{%H:%M:%S.%l} | %6p | %44.44c | %m%n");

  root.removeAllAppenders();

  //stdout
  {
    log4cpp::Appender* appender = new log4cpp::FileAppender("ConsoleAppender", ::dup(fileno(stdout)));
    appender->setLayout(layout);
    root.addAppender(appender);
  }

  //file
  if (logfile)
  {
    log4cpp::Appender* appender = new log4cpp::FileAppender("FileAppender", *logfile);
    appender->setLayout(layout);
    root.addAppender(appender);
  }

#elif defined(LOGGING_FRAMEWORK_PRINTF)

  //do nothing

#elif defined(LOGGING_FRAMEWORK_SYSLOG)

  static bool initialized = false;
  if (!initialized)
  {
    initialized = true;
    //strdup is needed because openlog() doesnt take ownership of ident, so it need to point to an valid string (address) even after calling openlog()
    openlog(strdup(STR_2_CHAR(ident)) , LOG_NDELAY | ((printToStdErr) ? LOG_PERROR : 0) | LOG_PID, LOG_USER);
  }

#elif defined(LOGGING_FRAMEWORK_NONE)

  //do nothing

#else

#error "unsupported logging framework"

#endif
}

#if defined(LOGGING_FRAMEWORK_LOG4CPP)
log4cpp::LayoutAP CreateDefaultLayout()
{
  log4cpp::LayoutAP layout(new log4cpp::PatternLayout());
  static_cast<log4cpp::PatternLayout*>(layout.get())->setConversionPattern("%10t | %d{%H:%M:%S.%l} | %6p | %44.44c | %m%n");
  return layout;
}
#endif

}

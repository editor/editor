/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _IFACTORY_H_
#define _IFACTORY_H_

#include <memory>

namespace pattern
{

/** The IFactory interface provides a Create() method to create instances of a particular type.*/
template <class PRODUCT>
struct IFactory
{
  typedef PRODUCT ProductType;
  typedef std::unique_ptr<PRODUCT> ProductAPType;

  virtual ~IFactory() {}
  virtual ProductAPType Create() = 0;
};

/** The IFactory1 interface provides a Create() method to create instances of a particular type which accept one constructor argument.*/
template <class PRODUCT, typename ARG1>
struct IFactory1
{
  typedef PRODUCT ProductType;
  typedef std::unique_ptr<PRODUCT> ProductAPType;
  typedef ARG1 Arg1Type;

  virtual ~IFactory1() {}
  virtual ProductAPType Create(ARG1 arg1) = 0;
};

}
#endif

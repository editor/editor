#ifndef BASEITEM_H
#define BASEITEM_H

#include "log.h"
#include "basevisitor.h"

class BaseItem
{
public:
    virtual ~BaseItem() = default;

    virtual void Accept(BaseVisitor& visitor) = 0;
};

#endif

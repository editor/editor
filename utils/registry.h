/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _REGISTRY_H_
#define _REGISTRY_H_

#include <map>
#include <stdexcept> //TODO
#include "utils/registry.h"
#include "utils/ifactory.h"
#include "utils/singleton.h"
#include "utils/unknownkeyexception.h"

namespace pattern
{

/** The Registry can create instances of type FACTORY::ProductAPType by providing a KEY.*/
template <typename KEY, typename FACTORY>
class Registry :
    public Singleton<Registry<KEY, FACTORY> >
{
  friend class Singleton<Registry<KEY, FACTORY> >;

public:
  typedef KEY KeyType;
  typedef FACTORY FactoryType;
  typedef typename FactoryType::ProductType ProductType;
  typedef typename FactoryType::ProductAPType ProductAPType;

private:
  Registry() //because it's a singleton
  {}

public:
  /** Register a factory.
    * @param key The key which is associated with the factory.
    * @param factory The factory to use to create instances associated with key 'key'.*/
  void Register(const KeyType& key, FactoryType* factory)
  {
    _map.insert(typename MapType::value_type(key, factory));
  }

  /** Factory pattern.
    * @param key The key which is associated with the derived type of Product.*/
  ProductAPType Create(const KeyType& key)
  {
    typename MapType::const_iterator it = _map.find(key);
    if (it == _map.end())
      throw UnknownKeyException("not found");
    return it->second->Create();
  }

private:
  typedef std::map<KeyType, FactoryType*> MapType;
  MapType _map; /** Map to store to all registers factory by their key.*/
};

/** The Registry1 can create instances of type FACTORY::ProductAPType by providing a KEY and the first constructor argument.*/
template <typename KEY, typename FACTORY>
class Registry1 :
    public Singleton<Registry1<KEY, FACTORY> >
{
  friend class Singleton<Registry1<KEY, FACTORY> >;

public:
  typedef KEY KeyType;
  typedef FACTORY FactoryType;
  typedef typename FactoryType::ProductType ProductType;
  typedef typename FactoryType::ProductAPType ProductAPType;
  typedef typename FactoryType::Arg1Type Arg1Type;

private:
  Registry1() //because it's a singleton
  {}

public:
  void Register(const KeyType& key, FactoryType* factory)
  {
    _map.insert(typename MapType::value_type(key, factory));
  }

  ProductAPType Create(const KeyType& key, Arg1Type arg1)
  {
    typename MapType::const_iterator it = _map.find(key);
    if (it == _map.end())
      throw UnknownKeyException("not found");
    return it->second->Create(arg1);
  }

private:
  typedef std::map<KeyType, FactoryType*> MapType;
  MapType _map;
};
}
#endif

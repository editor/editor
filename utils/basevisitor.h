/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _BASEVISITOR_H_
#define _BASEVISITOR_H_

namespace graphics
{
namespace items
{
    class AreaItem;
    class EllipseItem;
    class PenItem;
    class PolylineItem;
    class TextItem;
}
}

class BaseVisitor
{
public:
    virtual void Visit(graphics::items::AreaItem& item) = 0;
    virtual void Visit(graphics::items::EllipseItem& item) = 0;
    virtual void Visit(graphics::items::PenItem& item) = 0;
    virtual void Visit(graphics::items::PolylineItem& item) = 0;
    virtual void Visit(graphics::items::TextItem& item) = 0;
};

#endif


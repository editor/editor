/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _CAST_H_
#define _CAST_H_


#include <cstddef> //for NULL
#include <typeinfo> //for std::bad_cast
#include "utils/assert.h"

/** Equivalent to boost::polymorphic_cast() */
template <class TARGET, class SOURCE>
inline TARGET polymorphic_cast(SOURCE* x)
{
  TARGET tmp = dynamic_cast<TARGET>(x);
  if (tmp == NULL)
    throw std::bad_cast();
  return tmp;
}

/** Equivalent to boost::polymorphic_downcast() */
template <class TARGET, class SOURCE>
inline TARGET safe_static_cast(SOURCE* x)
{
  ASSERT(dynamic_cast<TARGET>(x) == x);  // detect logic error
  return static_cast<TARGET>(x);
}

#endif


/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _ASSERT_H_
#define _ASSERT_H_

#ifdef SUPPORT_ASSERT

#include <stdexcept>

namespace util
{

/** Interrupt current process.
    * Usually, this happs, because an assertion is broken.
    * @param file Filename of source code file.
    * @param function Name of function, which wants to interrupt the program.
    * @param condition The broken condition as string */
void AssertFailed(const char* file, const char* function, int line, const char* condition);

}

#define ASSERT(CONDITION)                                                       \
{                                                                               \
  try                                                                           \
{                                                                               \
  if (!(CONDITION))                                                             \
{                                                                               \
  ::util::AssertFailed(__FILE__, __FUNCTION__, __LINE__, #CONDITION);           \
  }                                                                             \
  }                                                                             \
  catch (const std::exception& ex)                                              \
{                                                                               \
  ::util::AssertFailed(__FILE__, __FUNCTION__, __LINE__, ex.what());            \
  }                                                                             \
  catch (...)                                                                   \
{                                                                               \
  ::util::AssertFailed(__FILE__, __FUNCTION__, __LINE__, "unknown exception");  \
}                                                                               \
}

#define VERIFY(CONDITION) ASSERT(CONDITION)

#else

#define ASSERT(CONDITION)

#define VERIFY(CONDITION)   \
{                           \
  if (CONDITION) {}         \
}

#endif

#endif

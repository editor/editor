/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _TYPES_H_
#define _TYPES_H_

#include <string>
#include <sstream>
#include <vector>
#include <cstdint>

typedef std::int8_t       INT8t;
typedef std::uint8_t      UINT8t;
typedef std::int16_t      INT16t;
typedef std::uint16_t     UINT16t;
typedef std::int32_t      INT32t;
typedef std::uint32_t     UINT32t;
typedef std::int64_t      INT64t;
typedef std::uint64_t     UINT64t;
typedef std::string         STRING;
typedef std::ostringstream  OSTRINGSTREAM;
typedef std::istringstream  ISTRINGSTREAM;
typedef std::stringstream   STRINGSTREAM;
typedef std::vector<UINT8t> BUFFER;

#endif

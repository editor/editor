/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _DEFAULTFACTORY_H_
#define _DEFAULTFACTORY_H_

#include "utils/ifactory.h"

namespace pattern
{

/** The DefaultFactory implements IFactory by calling the new-operator.*/
template <class PRODUCT, class DERIVED>
class DefaultFactory :
  public IFactory<PRODUCT>
{
  public:
  typedef typename IFactory<PRODUCT>::ProductAPType ProductAPType;

  public:
  virtual ProductAPType Create()
  {
    return ProductAPType(new DERIVED());
  }
};

/** The DefaultFactory1 implements IFactory1 by calling the new-operator.*/
template <class PRODUCT, class DERIVED, typename ARG1>
class DefaultFactory1 :
  public IFactory1<PRODUCT, ARG1>
{
  public:
  typedef typename IFactory1<PRODUCT, ARG1>::ProductAPType ProductAPType;
  typedef typename IFactory1<PRODUCT, ARG1>::Arg1Type Arg1Type;

  public:
  virtual ProductAPType Create(Arg1Type arg1)
  {
    return ProductAPType(new DERIVED(arg1));
  }
};

}

#endif


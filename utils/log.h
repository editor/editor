/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _LOG_H_
#define _LOG_H_

#define LOGGING_FRAMEWORK_PRINTF 1
#define SUPPORT_LOGGING_INFO 1

namespace logging
{

  void InitLoggingFramework(
    #if defined(LOGGING_FRAMEWORK_LOG4CPP)
      const STRING* logfile = NULL
    #elif defined(LOGGING_FRAMEWORK_SYSLOG)
      const STRING& ident = "app", bool printToStdErr = false
    #endif
      );

}

#if defined(LOGGING_FRAMEWORK_NONE) || defined(LOGGING_FRAMEWORK_SYSLOG) || defined(LOGGING_FRAMEWORK_PRINTF)

namespace logging
{
  namespace detail
  {
    struct DummyClassLogger //that's it
    {};
  }
}

#define DECLARE_CLASS_LOGGER                          \
  private:                                            \
  static ::logging::detail::DummyClassLogger _category;

#define DEFINE_CLASS_LOGGER(CLASS)                    \
  ::logging::detail::DummyClassLogger CLASS::_category;

#endif

#if defined(LOGGING_FRAMEWORK_LOG4CPP)

#include <memory>
#include <log4cpp/Category.hh>
#include <log4cpp/Layout.hh>
#include <base/logging/detail/Log4Cpp.h>
#include <base/logging/detail/Util.h>

#define DECLARE_CLASS_LOGGER                                    \
  private:                                                      \
  static log4cpp::Category& _category;                          \
  public:                                                       \
  static log4cpp::Category& GetCategory() {return _category;}   \
  private:

#define DEFINE_CLASS_LOGGER(CLASS) \
  log4cpp::Category& CLASS::_category(log4cpp::Category::getInstance(logging::detail::Filename2Namespace(__FILE__)));

#define LOG_FATAL_IMPL(fmt, ...)        try { _category.fatal(fmt, ##__VA_ARGS__); } catch (...) {}
#define LOG_ERROR_IMPL(fmt, ...)        try { _category.error(fmt, ##__VA_ARGS__); } catch (...) {}
#define LOG_WARN_IMPL(fmt, ...)         try { _category.warn(fmt, ##__VA_ARGS__); } catch (...)  {}
#define LOG_INFO_IMPL(fmt, ...)         try { _category.notice(fmt, ##__VA_ARGS__); } catch (...){}
#define LOG_DEBUG_IMPL(fmt, ...)        try { _category.debug(fmt, ##__VA_ARGS__); } catch (...) {}

#define LOG_FUNC_FATAL_IMPL(fmt, ...)   try { logging::detail::GetCategory(__FILE__).fatal(fmt, ##__VA_ARGS__); } catch (...)  {}
#define LOG_FUNC_ERROR_IMPL(fmt, ...)   try { logging::detail::GetCategory(__FILE__).error(fmt, ##__VA_ARGS__); } catch (...)  {}
#define LOG_FUNC_WARN_IMPL(fmt, ...)    try { logging::detail::GetCategory(__FILE__).warn(fmt, ##__VA_ARGS__); } catch (...)   {}
#define LOG_FUNC_INFO_IMPL(fmt, ...)    try { logging::detail::GetCategory(__FILE__).notice(fmt, ##__VA_ARGS__); } catch (...) {}
#define LOG_FUNC_DEBUG_IMPL(fmt, ...)   try { logging::detail::GetCategory(__FILE__).debug(fmt, ##__VA_ARGS__); } catch (...)  {}

namespace log4cpp
{
  typedef std::unique_ptr<Layout> LayoutAP;
}
namespace logging
{
  log4cpp::LayoutAP CreateDefaultLayout();
}

#elif defined(LOGGING_FRAMEWORK_PRINTF)

#include <stdio.h>

#define LOG_FATAL_IMPL(fmt, ...)        try { fprintf(stderr, fmt "\n", ##__VA_ARGS__); } catch (...) {}
#define LOG_ERROR_IMPL(fmt, ...)        try { fprintf(stderr, fmt "\n", ##__VA_ARGS__); } catch (...) {}
#define LOG_WARN_IMPL(fmt, ...)         try { fprintf(stderr, fmt "\n", ##__VA_ARGS__); } catch (...) {}
#define LOG_INFO_IMPL(fmt, ...)         try { fprintf(stderr, fmt "\n", ##__VA_ARGS__); } catch (...) {}
#define LOG_DEBUG_IMPL(fmt, ...)        try { fprintf(stderr, fmt "\n", ##__VA_ARGS__); } catch (...) {}

#define LOG_FUNC_FATAL_IMPL(fmt, ...)   try { fprintf(stderr, fmt "\n", ##__VA_ARGS__); } catch (...) {}
#define LOG_FUNC_ERROR_IMPL(fmt, ...)   try { fprintf(stderr, fmt "\n", ##__VA_ARGS__); } catch (...) {}
#define LOG_FUNC_WARN_IMPL(fmt, ...)    try { fprintf(stderr, fmt "\n", ##__VA_ARGS__); } catch (...) {}
#define LOG_FUNC_INFO_IMPL(fmt, ...)    try { fprintf(stderr, fmt "\n", ##__VA_ARGS__); } catch (...) {}
#define LOG_FUNC_DEBUG_IMPL(fmt, ...)   try { fprintf(stderr, fmt "\n", ##__VA_ARGS__); } catch (...) {}

#elif defined(LOGGING_FRAMEWORK_SYSLOG)

#include <base/logging/detail/Syslog.h>

#define LOG_FATAL_IMPL(fmt, ...)        try { ::logging::detail::Log2Syslog(::logging::detail::SL_EMERG,   __FILE__, __LINE__, fmt, ##__VA_ARGS__); } catch (...) {}
#define LOG_ERROR_IMPL(fmt, ...)        try { ::logging::detail::Log2Syslog(::logging::detail::SL_CRIT,    __FILE__, __LINE__, fmt, ##__VA_ARGS__); } catch (...) {}
#define LOG_WARN_IMPL(fmt, ...)         try { ::logging::detail::Log2Syslog(::logging::detail::SL_WARNING, __FILE__, __LINE__, fmt, ##__VA_ARGS__); } catch (...) {}
#define LOG_INFO_IMPL(fmt, ...)         try { ::logging::detail::Log2Syslog(::logging::detail::SL_INFO,    __FILE__, __LINE__, fmt, ##__VA_ARGS__); } catch (...) {}
#define LOG_DEBUG_IMPL(fmt, ...)        try { ::logging::detail::Log2Syslog(::logging::detail::SL_DEBUG,   __FILE__, __LINE__, fmt, ##__VA_ARGS__); } catch (...) {}

#define LOG_FUNC_FATAL_IMPL(fmt, ...)   try { ::logging::detail::Log2Syslog(::logging::detail::SL_EMERG,   __FILE__, __LINE__, fmt, ##__VA_ARGS__); } catch (...) {}
#define LOG_FUNC_ERROR_IMPL(fmt, ...)   try { ::logging::detail::Log2Syslog(::logging::detail::SL_CRIT,    __FILE__, __LINE__, fmt, ##__VA_ARGS__); } catch (...) {}
#define LOG_FUNC_WARN_IMPL(fmt, ...)    try { ::logging::detail::Log2Syslog(::logging::detail::SL_WARNING, __FILE__, __LINE__, fmt, ##__VA_ARGS__); } catch (...) {}
#define LOG_FUNC_INFO_IMPL(fmt, ...)    try { ::logging::detail::Log2Syslog(::logging::detail::SL_INFO,    __FILE__, __LINE__, fmt, ##__VA_ARGS__); } catch (...) {}
#define LOG_FUNC_DEBUG_IMPL(fmt, ...)   try { ::logging::detail::Log2Syslog(::logging::detail::SL_DEBUG,   __FILE__, __LINE__, fmt, ##__VA_ARGS__); } catch (...) {}

#elif defined(LOGGING_FRAMEWORK_NONE)

#define LOG_FATAL_IMPL(fmt, ...)
#define LOG_ERROR_IMPL(fmt, ...)
#define LOG_WARN_IMPL(fmt, ...)
#define LOG_INFO_IMPL(fmt, ...)
#define LOG_DEBUG_IMPL(fmt, ...)

#define LOG_FUNC_FATAL_IMPL(fmt, ...)
#define LOG_FUNC_ERROR_IMPL(fmt, ...)
#define LOG_FUNC_WARN_IMPL(fmt, ...)
#define LOG_FUNC_INFO_IMPL(fmt, ...)
#define LOG_FUNC_DEBUG_IMPL(fmt, ...)

#else

#error "unsupported logging framework"

#endif

#ifdef SUPPORT_LOGGING_FATAL
#define LOG_FATAL(fmt, ...)             LOG_FATAL_IMPL(fmt, ##__VA_ARGS__)
#define LOG_FUNC_FATAL(fmt, ...)        LOG_FUNC_FATAL_IMPL(fmt, ##__VA_ARGS__)
#else
#define LOG_FATAL(fmt, ...)
#define LOG_FUNC_FATAL(fmt, ...)
#endif

#ifdef SUPPORT_LOGGING_ERROR
#define LOG_ERROR(fmt, ...)             LOG_ERROR_IMPL(fmt, ##__VA_ARGS__)
#define LOG_FUNC_ERROR(fmt, ...)        LOG_FUNC_ERROR_IMPL(fmt, ##__VA_ARGS__)
#else
#define LOG_ERROR(fmt, ...)
#define LOG_FUNC_ERROR(fmt, ...)
#endif

#ifdef SUPPORT_LOGGING_WARN
#define LOG_WARN(fmt, ...)              LOG_WARN_IMPL(fmt, ##__VA_ARGS__)
#define LOG_FUNC_WARN(fmt, ...)         LOG_FUNC_WARN_IMPL(fmt, ##__VA_ARGS__)
#else
#define LOG_WARN(fmt, ...)
#define LOG_FUNC_WARN(fmt, ...)
#endif

#ifdef SUPPORT_LOGGING_INFO
#define LOG_INFO(fmt, ...)              LOG_INFO_IMPL(fmt, ##__VA_ARGS__)
#define LOG_FUNC_INFO(fmt, ...)         LOG_FUNC_INFO_IMPL(fmt, ##__VA_ARGS__)
#else
#define LOG_INFO(fmt, ...)
#define LOG_FUNC_INFO(fmt, ...)
#endif

#ifdef SUPPORT_LOGGING_DEBUG
#define LOG_DEBUG(fmt, ...)             LOG_DEBUG_IMPL(fmt, ##__VA_ARGS__)
#define LOG_FUNC_DEBUG(fmt, ...)        LOG_FUNC_DEBUG_IMPL(fmt, ##__VA_ARGS__)
#else
#define LOG_DEBUG(fmt, ...)
#define LOG_FUNC_DEBUG(fmt, ...)
#endif

#endif

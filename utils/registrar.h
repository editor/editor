/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _REGISTRAR_H_
#define _REGISTRAR_H_

#include "utils/defaultfactory.h"
#include "utils/registry.h"

namespace pattern
{

/** The Registrar class is responsible to register a factory to a registry.*/
template <class REGISTRY, class DERIVED>
class Registrar
{
  public:
  typedef REGISTRY RegistryType;
  typedef typename RegistryType::KeyType KeyType;
  typedef typename RegistryType::ProductType ProductType;
  typedef DERIVED DerivedType;

  public:
  Registrar(const KeyType& key)
  {
    RegistryType::Instance().Register(key, &_factory);
  }

  private:
  DefaultFactory<ProductType, DerivedType> _factory;
};

/** The Registrar class is responsible to register a factory which accepts one contructor argument to a registry.*/
template <class REGISTRY, class DERIVED>
class Registrar1
{
  public:
  typedef REGISTRY RegistryType;
  typedef typename RegistryType::KeyType KeyType;
  typedef typename RegistryType::ProductType ProductType;
  typedef DERIVED DerivedType;
  typedef typename RegistryType::Arg1Type Arg1Type;

  public:
  Registrar1(const KeyType& key)
  {
    RegistryType::Instance().Register(key, &_factory);
  }

  private:
  DefaultFactory1<ProductType, DerivedType, Arg1Type> _factory;
};
}
#endif

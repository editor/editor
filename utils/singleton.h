/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _SINGLETON_H_
#define _SINGLETON_H_

#include <stdlib.h>
#include <boost/noncopyable.hpp>
#include <boost/thread/locks.hpp>
#include "utils/nullmutex.h"
#include "utils/assert.h"

namespace pattern
{

/** Singleton pattern.
  * @tparam T Type, which you want to have available as singleton
  * @tparam MUTEX Type of the mutex which is used to protect ctor and dtor
  * @tparam RELEASE_AT_EXIT If true, the dtor will be executed when the program is terminated normally
  * @code
  * class MyClass :
  * 	public pattern::Singleton<MyClass>
  * {
  *		//Singleton<MyClass> has to be friend, so that ctor and dtor can be called by Singleton<>
  *		friend class pattern::Singleton<MyClass>;
  *		private:
  *		//ctor and dtor should be private
  *		MyClass() {}
  *		~MyClass() {}
  *		public:
  *		void Hello() {printf("Hello\n");}
  * }
  *
  * MyClass::Instance().Hello()
  * @endcode */
template
<
    class T,
    class MUTEX = ::boost::NullMutex,
    bool RELEASE_AT_EXIT = true
    >
class Singleton :
    private boost::noncopyable
{
private:
  typedef MUTEX Mutex;

protected:
  Singleton()
  {}

  ~Singleton()
  {}

public:
  /** Returns the wrapped singleton instance
    * If called the very first time, a new instance of T will be created */
  static T& Instance()
  {
    //TODO optimize this -> http://www.cs.umd.edu/~pugh/java/memoryModel/DoubleCheckedLocking.html
    boost::lock_guard<Mutex> guard(_mutex);
    if (!_instance)
    {
      _instance = new T();
      if (RELEASE_AT_EXIT)
      {
        VERIFY(0 == atexit(Release));
      }
    }
    return *_instance;
  }

  /** Releases the wrapped singleton instance */
  static void Release()
  {
    boost::lock_guard<Mutex> guard(_mutex);
    if (_instance)
    {
      T* i = _instance;
      _instance = NULL; //set to NULL before calling the dtor, because the dtor may throws an exception
      delete i;
    }
  }

private:
  static T* _instance;
  static Mutex _mutex;
};

template<class T, class MUTEX, bool RELEASE_AT_EXIT>
T* Singleton<T, MUTEX, RELEASE_AT_EXIT>::_instance = NULL;

template<class T, class MUTEX, bool RELEASE_AT_EXIT>
MUTEX Singleton<T, MUTEX, RELEASE_AT_EXIT>::_mutex;

/** The SingletonGuard class is just a util class which can be created on the stack, to ensure, that a singleton is release when the guard goes out of scope.*/
template<class T>
struct SingletonGuard :
    private boost::noncopyable
{
  SingletonGuard(bool create = false)
  {
    if (create)
      T::Instance();
  }

  ~SingletonGuard()
  {
    T::Release();
  }
};
}
#endif

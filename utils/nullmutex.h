/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _NULLMUTEX_H_
#define _NULLMUTEX_H_

#include <boost/utility.hpp>

namespace boost
{
/**
 * Simple implementation of the boost::thread::mutex interface.
 * Allows to create generic code with a dummy lock mechanism.
 * e,g, used by the pattern::Singleton class as default mutex type.
 */
class NullMutex :
    private boost::noncopyable
{
public:
  inline void lock() {}
  inline bool try_lock() {return true;}
  inline void unlock() {}
  inline void lock_shared() {}
  inline bool try_lock_shared() {return true;}
  inline void unlock_shared() {}
  inline void lock_upgrade() {}
  inline void unlock_upgrade() {}
  inline void unlock_upgrade_and_lock() {}
  inline void unlock_upgrade_and_lock_shared() {}
  inline void unlock_and_lock_upgrade() {}
};
}
#endif

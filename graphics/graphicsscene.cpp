/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "utils/assert.h"
#include "graphics/graphicsscene.h"
#include "graphics/toolscene.h"

namespace graphics
{

GraphicsScene::GraphicsScene(ToolScene* ts) :
  QGraphicsScene(ts),
  m_ts(ts)
{
  ASSERT(m_ts);
}

void GraphicsScene::drawBackground(QPainter* painter, const QRectF& rect)
{
  m_ts->DrawBackground(*painter, rect);
}

void GraphicsScene::mousePressEvent(QGraphicsSceneMouseEvent* mouseEvent)
{
  if (!m_ts->MousePressEvent(*mouseEvent))
  {
    QGraphicsScene::mousePressEvent(mouseEvent);
  }
}

void GraphicsScene::mouseMoveEvent(QGraphicsSceneMouseEvent* mouseEvent)
{
  if (!m_ts->MouseMoveEvent(*mouseEvent))
  {
    QGraphicsScene::mouseMoveEvent(mouseEvent);
  }
}

void GraphicsScene::mouseReleaseEvent(QGraphicsSceneMouseEvent* mouseEvent)
{
  if (!m_ts->MouseReleaseEvent(*mouseEvent))
  {
    QGraphicsScene::mouseReleaseEvent(mouseEvent);
  }
}

}

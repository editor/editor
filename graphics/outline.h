/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _OUTLINE_H_
#define _OUTLINE_H_

#include <QColor>
#include <QMetaType>

namespace graphics
{
/**
 * The Outline struct
 * contains the data for outlining.
 */
struct Outline
{
  Outline():
    Width(0)
  {}

  Outline(const QColor& color, qreal width):
    Color(color),
    Width(width)
  {}

  Outline(const Outline& rhs)
  {
    Color = rhs.Color;
    Width = rhs.Width;
  }

  QColor Color;
  qreal Width;
};

}

Q_DECLARE_METATYPE(graphics::Outline)
#endif

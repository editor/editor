/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "toolscene.h"

#include <QApplication>
#include <QPainter>
#include "utils/cast.h"
#include "utils/log.h"
#include "utils/macros.h"
#include "graphics/persistency/utilpersistency.h"
#include "toolscene.h"
#include "graphics/graphicsscene.h"
#include "graphics/qitemlist.h"
#include "graphics/outline.h"
#include "graphics/scenestates/idlescenestate.h"
#include "graphics/persistency/defaultcreateitemreadervisitor.h"
#include "graphics/persistency/defaultprintitemvisitor.h"
#include "graphics/persistency/toolscenepersistency.h"


using namespace graphics;

ToolScene::ToolScene(QObject* parent) :
  QObject(parent),
  m_state(new graphics::scenestate::IdleSceneState()),
  m_dirty(false),
  m_createItemReaderVisitor(new persistency::DefaultCreateItemReaderVisitor()),
  m_printItemVisitor(new persistency::DefaultPrintItemVisitor()),
  m_textDialog(NULL)
{
  m_graphicsScene = new graphics::GraphicsScene(this);

  SetPropertyValue(PROP_BRUSH,     QColor(255, 0, 0, 128));
  SetPropertyValue(PROP_PEN,       QPen(QBrush(QColor(0, 0, 0, 256 * 3 / 4)), 3));
  SetPropertyValue(PROP_FONT,      QApplication::font());
  SetPropertyValue(PROP_POSITION,  QPointF());

  QVariant var;
  var.setValue(Outline(QColor(255,0,0,0),5));
  SetPropertyValue(PROP_OUTLINE, var);

}

void ToolScene::SetBackgroundImage(const QPixmap& img)
{
  m_bkgndImage = img;
  SetSceneRect(m_bkgndImage.rect());
  m_graphicsScene->update();
}

void ToolScene::ClearBackgroundImage()
{
  SetBackgroundImage(QPixmap());
}

void ToolScene::RemoveAllItems()
{
  m_graphicsScene->clear();
}

const QGraphicsScene& ToolScene::GetGraphicsScene() const
{
  return *m_graphicsScene;
}

QGraphicsScene& ToolScene::GetGraphicsScene()
{
  return *m_graphicsScene;
}

QRectF ToolScene::GetSceneRect() const
{
  return m_graphicsScene->sceneRect();
}

void ToolScene::SetSceneRect(const QRectF& rect)
{
  m_graphicsScene->setSceneRect(rect);
}

items::ItemList ToolScene::Items() const
{
  QItemList items = m_graphicsScene->items();
  items::ItemList list;
  for (QItemList::iterator iter = items.begin(); iter != items.end(); ++iter)
  {
    //qobject_cast does not work: QGraphicsItem is NOT a QObject
    items::AbstractItem* abstractItem = dynamic_cast<items::AbstractItem*>(*iter);
    if (abstractItem)
    {
      list.push_back(abstractItem);
    }
  }
  return list;
}

void ToolScene::ChangeState(scenestate::AbstractSceneStateAP newState)
{
  LOG_DEBUG("change state %s -> %s", m_state->GetStateName(), newState->GetStateName());
  m_state->ExitState(*this);
  m_state = std::move(newState);
  QCONNECT(m_state.get(), SIGNAL(SigStateFinalized()), SLOT(StateFinalized()));
  m_state->EnterState(*this);
  emit SigStateChanged();
}

const scenestate::AbstractSceneState& ToolScene::GetState() const
{
  return *m_state;
}

void ToolScene::FinalizeState()
{
  m_state->FinalizeState(*this);
}

void ToolScene::RemoveSelectedItems()
{
  items::ItemList selects = SelectedItems();
  for (graphics::items::ItemList::iterator it = selects.begin();
       it != selects.end();
       ++it)
  {
    delete *it;
  }
}

void ToolScene::DeselectAll()
{
  items::ItemList items = SelectedItems();
  for (items::ItemList::Iterator it=items.begin(); it != items.end(); ++it)
  {
    (*it)->GotoFirstNotSelectedState();
  }
}

Properties ToolScene::GetSupportedProperties() const
{
  Properties props;

  typedef QList<Property> Keys;
  Keys keys = m_defaultProperties.keys();
  for (Keys::const_iterator it = keys.begin();
       it != keys.end();
       ++it)
  {
    props |= *it;
  }

  return props;
}

QVariant ToolScene::GetPropertyValue(Property p) const
{
  if (!m_defaultProperties.contains(p))
  {
    throw std::logic_error("property not found");
  }
  return m_defaultProperties[p];
}

void ToolScene::SetPropertyValue(Property p, const QVariant& value)
{
  m_defaultProperties[p] = value;
  emit SigPropertyValueChanged(p, value);
}

void ToolScene::RegisterToSigPropertyValueChanged(QObject* receiver, const char* slot)
{
  QCONNECT(this, SIGNAL(SigPropertyValueChanged(graphics::Property, QVariant)), receiver, slot);
}

void ToolScene::SetSelectedItemsPropertyValue(Property prop, const QVariant& value)
{
  items::ItemList selected = SelectedItems();
  for (graphics::items::ItemList::iterator it = selected.begin();
       it != selected.end();
       ++it)
  {
    if ((*it)->GetSupportedProperties().testFlag(prop))
    {
      (*it)->SetPropertyValue(prop, value);
    }
  }
}
/*
void ToolScene::SetDefaultItemsPropertyValue(items::AbstractItem::Property prop, const QVariant& value)
{
        items::AbstractItem::SetDefaultPropertyValue(prop, value);
}
*/

void ToolScene::Load(const boost::filesystem::path& filename)
{
  persistency::Load<persistency::ToolScenePersistency>(*this, "scene", filename);
  SetDirty(false);
}

void ToolScene::Save(const boost::filesystem::path& filename) const
{
  persistency::Save<persistency::ToolScenePersistency>(*this, "scene", filename);
  const_cast<ToolScene*>(this)->SetDirty(false);
}

void ToolScene::SetIGetText(IGetText* gt)
{
  m_textDialog = gt;
}

IGetText* ToolScene::GetIGetText()
{
  return m_textDialog;
}

#ifdef graphics_USE_MEASURE
void ToolScene::SetMultiPointRefPointList(ms::MultiPointRefPointListAP rp)
{
  //delete all items, which use the current ref point list
  if (_multiPointRefPointList.get())
  {
    items::ItemList absItems = Items();
    for (items::ItemList::iterator it = absItems.begin(); it != absItems.end(); ++it)
    {
      if (_multiPointRefPointList.get() == (*it)->GetMultiPointRefPointList())
      {
        delete *it;
      }
    }
  }

  //delete all multipoint measure models (items will be deleted indirectly)
  for (ms::MeasureModelList::iterator it = _measureModels.begin();
       it != _measureModels.end();)
  {
    if (dynamic_cast<const ms::MultiPointMeasureModel*>(&(*it)))
    {
      it = _measureModels.erase(it);
    }
    else
    {
      ++it;
    }
  }

  //now, assign the new ref point list
  _multiPointRefPointList = rp;
}

const ms::MultiPointRefPointList* ToolScene::GetMultiPointRefPointList() const
{
  return _multiPointRefPointList.get();
}
#endif

graphics::items::AbstractItem* ToolScene::GetTopItem()
{
  items::ItemList absItems = Items();
  items::AbstractItem* topItem = NULL;
  for (graphics::items::ItemList::iterator it = absItems.begin(); it < absItems.end(); ++it)
  {
    if (NULL == topItem || (*it)->zValue() > topItem->zValue())
      topItem = *it;
  }
  return topItem;
}

bool ToolScene::MoveSelectedBy(const QPointF& delta)
{
  items::ItemList selects = SelectedItems();

  // the center of all movable items must remain on the scene
  for (graphics::items::ItemList::const_iterator it = selects.begin(); it != selects.end(); ++it)
  {
    if (!(*it)->IsMovable())
      continue;
    QPointF center = (*it)->sceneBoundingRect().center();
    if (!m_graphicsScene->sceneRect().contains(center + delta))
      return false; // item(s) cannot be not moved
  }

  for (graphics::items::ItemList::iterator it=selects.begin(); it != selects.end(); ++it)
  {
    if (!(*it)->IsMovable())
      continue;
    (*it)->moveBy(delta.x(), delta.y());
  }

  return true; // some item(s) moved
}

items::ItemList ToolScene::SelectedItems() const
{
  graphics::items::ItemList items = Items();
  graphics::items::ItemList selects;
  for (graphics::items::ItemList::iterator it=items.begin(); it<items.end(); ++it)
  {
    if ((*it)->IsSelected())
    {
      selects.push_back(*it);
    }
  }
  return selects;
}

graphics::items::AbstractItem* ToolScene::AddItem(graphics::items::AbstractItem* item, bool applyDefaults)
{
  m_graphicsScene->addItem(item);
  if (applyDefaults)
  {
    const Properties props = GetSupportedProperties();
    for (unsigned int i = 1; i < _MAX_PROPERTY; i = i << 1)
    {
      const Property prop = static_cast<Property>(i);
      if (!props.testFlag(prop))
        continue;
      if (!item->GetSupportedProperties().testFlag(prop))
        continue;
      item->SetPropertyValue(prop, GetPropertyValue(prop));
    }
  }
  QCONNECT(item, SIGNAL(SigItemChanged()), SLOT(ItemChanged()));
  QCONNECT(item, SIGNAL(SigStateChanged()), SLOT(StateChanged()));
  SetDirty();
  return item;
}

bool ToolScene::IsDirty() const
{
  return m_dirty;
}

void ToolScene::SetDirty(bool dirty)
{
  if (m_dirty != dirty)
  {
    m_dirty = dirty;
    emit SigDirtyChanged(m_dirty);
  }
}

void ToolScene::ItemChanged()
{
  SetDirty();
}

void ToolScene::StateChanged()
{
  emit SigStateChanged();
}

void ToolScene::DrawBackground(QPainter& painter, const QRectF& rect)
{
  UNUSED_VAR(rect);
  painter.drawPixmap(QPoint(0, 0), m_bkgndImage);
}

bool ToolScene::MousePressEvent(QGraphicsSceneMouseEvent& mouseEvent)
{
  if (!m_graphicsScene->sceneRect().contains(mouseEvent.scenePos()))
  {
    LOG_DEBUG("Mouse press out of bounds");
    return false;
  }

  // Allow Qt to handle the event when we do not
  return m_state->MousePressEvent(*this, mouseEvent);
}

bool ToolScene::MouseMoveEvent(QGraphicsSceneMouseEvent& mouseEvent)
{
  return m_state->MouseMoveEvent(*this, mouseEvent);
}

bool ToolScene::MouseReleaseEvent(QGraphicsSceneMouseEvent& mouseEvent)
{
  return m_state->MouseReleaseEvent(*this, mouseEvent);
}

void ToolScene::StateFinalized()
{
  emit SigStateFinalized();
}

void ToolScene::ConfigurePersistency(persistency::CreateReaderVisitorAP rv, persistency::PrintVisitorAP pv)
{
  m_createItemReaderVisitor = std::move(rv);
  m_printItemVisitor = std::move(pv);
}

persistency::AbstractCreateReaderVisitor& ToolScene::GetCreateItemReaderVisitor()
{
  ASSERT(m_createItemReaderVisitor.get());
  return *m_createItemReaderVisitor;
}

persistency::AbstractPrintVisitor& ToolScene::GetPrintItemVisitor()
{
  ASSERT(m_printItemVisitor.get());
  return *m_printItemVisitor;
}



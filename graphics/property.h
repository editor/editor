/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _PROPERTY_H
#define _PROPERTY_H

namespace graphics
{
enum Property
{
  PROP_NONE             = 0, //nothing
  PROP_POSITION         = 1 << 1, //QPointF
  PROP_FONT             = 1 << 2, //QFont
  PROP_BRUSH            = 1 << 3, //QBrush
  PROP_PEN              = 1 << 4, //QPen
  //PROP_ZVALUE         = 1 << 5, //qreal
  PROP_OUTLINE          = 1 << 5, //QPen
  PROP_TEXT             = 1 << 6, //QString
  _MAX_PROPERTY         = 1 << 7
};

Q_DECLARE_FLAGS(Properties, Property);
Q_DECLARE_OPERATORS_FOR_FLAGS(Properties);
}

#endif

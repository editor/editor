/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _IPROPERTIES_H_
#define _IPROPERTIES_H_

#include <QObject>
#include <QVariant>
#include "graphics/property.h"

namespace graphics
{

struct IProperties
{
  virtual ~IProperties() {}

  /**
   * GetSupportedProperties
   * return all properties for the item
   * @return Properties
   */
  virtual Properties GetSupportedProperties() const = 0;
  /**
   * GetPropertyValue
   * get the value for a given property p.
   * @return QVariant
   * @throw std::logic_error if property isn't supported
   */
  virtual QVariant GetPropertyValue(Property p) const = 0;
  /**
   * SetPropertyValue
   * set the value (value) for a given property p.
   * @param value
   * @throw std::logic_error if property isn't supported
   */
  virtual void SetPropertyValue(Property p, const QVariant& value) = 0;

  virtual void RegisterToSigPropertyValueChanged(QObject* receiver, const char* slot) = 0;
  //void SigPropertyValueChanged(graphics::Property property, QVariant value);
};
}
#endif

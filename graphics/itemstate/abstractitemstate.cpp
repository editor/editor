/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <QPainter>
#include "utils/assert.h"
#include "graphics/items/abstractitem.h"
#include "graphics/itemstate/abstractitemstate.h"

namespace graphics
{
namespace itemstate
{

AbstractItemState::AbstractItemState(QObject* parent) :
  QObject(parent)
{}

AbstractItemState::~AbstractItemState()
{}

void AbstractItemState::EnterState(items::AbstractItem& item)
{
  UNUSED_VAR(item);
}

void AbstractItemState::ExitState(items::AbstractItem& item)
{
  UNUSED_VAR(item);
}

void AbstractItemState::Paint(QPainter& painter, items::AbstractItem& item) const
{
  UNUSED_VAR(painter);
  UNUSED_VAR(item);
}

bool AbstractItemState::IsSelectedState() const
{
  return false;
}

void AbstractItemState::PaintSelectionRect(QPainter& painter, items::AbstractItem& item) const
{
  painter.save();

  // draw the item's bounding rect
  const QRectF rect = item.GetSelectedRect();

  QPen pen;
  pen.setCosmetic(true);
  pen.setColor(Qt::black);
  painter.setPen(pen);
  painter.drawRect(rect);

  pen.setColor(Qt::white);
  pen.setStyle(Qt::DashLine);
  painter.setPen(pen);
  painter.drawRect(rect);

  painter.restore();
}

}
}

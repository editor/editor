/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <QVector>
#include "utils/macros.h"
#include "utils/cast.h"
#include "graphics/qitemlist.h"
#include "graphics/items/abstractitem.h"
#include "graphics/nodeitem/editnodeitem.h"
#include "graphics/itemstate/nodeselectitemstate.h"

namespace graphics
{
namespace itemstate
{

NodeSelectItemState::NodeSelectItemState()
{}

NodeSelectItemState::~NodeSelectItemState()
{}

void NodeSelectItemState::Paint(QPainter& painter, items::AbstractItem& item) const
{
  PaintSelectionRect(painter, item);
}

void NodeSelectItemState::EnterState(items::AbstractItem& item)
{
  SyncEditNodes(item);
  QCONNECT(&item, SIGNAL(SigEditNodesChanged()), SLOT(EditNodesChanged()));
}

void NodeSelectItemState::ExitState(items::AbstractItem& item)
{
  VERIFY(item.disconnect(this));

  QItemList childs = item.childItems();
  for (int i=0; i < childs.size(); ++i)
  {
    if (nodeitem::EditNodeItem::Type == childs[i]->type())
    {
      delete childs[i];
    }
  }
}

bool NodeSelectItemState::IsSelectedState() const
{
  return true;
}

void NodeSelectItemState::EditNodesChanged()
{
  SyncEditNodes(*(safe_static_cast<items::AbstractItem*>(sender())));
}

void NodeSelectItemState::SyncEditNodes(items::AbstractItem& item)
{
  //get all NodeItem's
  typedef QVector<nodeitem::EditNodeItem*> NodeItemList;
  NodeItemList nodeItems;
  {
    QItemList childs = item.childItems();
    for (QItemList::iterator it = childs.begin(); it != childs.end(); ++it)
    {
      if (nodeitem::CI_EDIT_NODE_ITEM_TYPE ==(*it)->type())
      {
        nodeitem::EditNodeItem* ni = safe_static_cast<nodeitem::EditNodeItem*>(*it);
        if (ni)
        {
          nodeItems.push_back(ni);
        }
      }
    }
  }

  //erase NodeItem which we dont need anymore
  const QPointFList positions = item.GetEditNodePositions();
  if (nodeItems.size() > positions.size())
  {
    for (int i = positions.size(); i < nodeItems.size(); ++i)
    {
      delete nodeItems[i];
    }
    nodeItems.resize(positions.size());
  }

  //add missing node items
  for (int i = nodeItems.size(); i < positions.size(); ++i)
  {
    nodeitem::EditNodeItem* node = new nodeitem::EditNodeItem(i, &item);
    nodeItems.push_back(node);
  }

  //sync positions for all node items
  for (int i = 0; i < nodeItems.size(); ++i)
  {
    QPointF pos = positions[i];
    const QSizeF size = nodeItems[i]->boundingRect().size();
    pos.setX(pos.x() - size.width() / 2);
    pos.setY(pos.y() - size.height() / 2);
    nodeItems[i]->setPos(pos);
  }
}

}
}

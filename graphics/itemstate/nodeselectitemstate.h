/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _NODESELECTITEMSTATE_H_
#define _NODESELECTITEMSTATE_H_

#include "utils/singleton.h"
#include "graphics/itemstate/abstractitemstate.h"

namespace graphics
{
namespace itemstate
{

/**
 * The NodeSelectItemState class
 * item state class for node editing
 */
class NodeSelectItemState :
    public AbstractItemState,
    public pattern::Singleton<NodeSelectItemState>
{
  Q_OBJECT
  friend class pattern::Singleton<NodeSelectItemState>;

private:
  NodeSelectItemState();
  virtual ~NodeSelectItemState();

public:
  /*! @copydoc AbstractItemState::EnterState() */
  virtual void EnterState(items::AbstractItem& item);
  /*! @copydoc AbstractItemState::ExitState() */
  virtual void ExitState(items::AbstractItem& item);
  /*! @copydoc AbstractItemState::Paint() */
  virtual void Paint(QPainter& painter, items::AbstractItem& item) const;
  /*! @copydoc AbstractItemState::IsSelectedState() */
  virtual bool IsSelectedState() const;

private slots:
  /**
   * EditNodesChanged
   * Qt slot is called, when a position of a node has changed.
   * Assumption: the sender is an AbstractItem
   */
  void EditNodesChanged();

private:
  /**
   * SyncEditNodes
   * Update the positions of all edit nodes of the given item
   * @param item
   */
  void SyncEditNodes(items::AbstractItem& item);
};

#define NODE_SELECT_ITEM_STATE (itemstate::NodeSelectItemState::Instance())

}
}
#endif


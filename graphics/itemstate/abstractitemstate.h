/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _ABSTRACTITEMSTATE_H_
#define _ABSTRACTITEMSTATE_H_

#include <QObject>

class QPainter;

namespace graphics
{

namespace items
{
class AbstractItem;
}

namespace itemstate
{
  /**
* The AbstractItemState class
* base class for item state
*/
class AbstractItemState :
  public QObject
{
  Q_OBJECT

protected:
  /**
   * AbstractItemState constructor
   * @param parent
   */
  AbstractItemState(QObject* parent = nullptr);

public:
  virtual ~AbstractItemState();

public:
  /**
   * EnterState
   * called when entering the state
   * @param item item belonging to state
   */
  virtual void EnterState(items::AbstractItem& item);

  /**
   * ExitState
   * called when exiting the state
   * @param item  item belonging to state
   */
  virtual void ExitState(items::AbstractItem& item);

  /**
   * Paint
   * decorate the item based on the given AbstractItemState
   * @param painter the painter
   * @param item item to be decorated
   */
  virtual void Paint(QPainter& painter, items::AbstractItem& item) const;

  /**
   * IsSelectedState
   * @return true if state represents a selected state
   */
  virtual bool IsSelectedState() const;

protected:
  /**
   * PaintSelectionRect paint the selection rectangle around the item
   * @param painter the painter from item
   * @param item the item itself
   */
  void PaintSelectionRect(QPainter& painter, items::AbstractItem& item) const;
};
}
}

#endif

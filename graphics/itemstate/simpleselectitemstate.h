/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _SIMPLESELECTEDITEMSTATE_H_
#define _SIMPLESELECTEDITEMSTATE_H_

#include "utils/singleton.h"
#include "graphics/itemstate/abstractitemstate.h"

namespace graphics
{
namespace itemstate
{
/**
* The SimpleSelectItemState class
* item state class for simple item selection
*/
class SimpleSelectItemState :
    public AbstractItemState,
    public pattern::Singleton<SimpleSelectItemState>
{
  Q_OBJECT
  friend class pattern::Singleton<SimpleSelectItemState>;

private:
  SimpleSelectItemState(){}
  virtual ~SimpleSelectItemState(){}

public:
  /*! @copydoc AbstractItemState::Paint() */
  virtual void Paint(QPainter& painter, graphics::items::AbstractItem& item) const {PaintSelectionRect(painter, item);}
  /*! @copydoc AbstractItemState::IsSelectedState() */
  virtual bool IsSelectedState() const {return true;}
};

#define SIMPLE_SELECT_ITEM_STATE (::graphics::itemstate::SimpleSelectItemState::Instance())

}
}
#endif


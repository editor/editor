/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _NOTSELECTEDITEMSTATE_H_
#define _NOTSELECTEDITEMSTATE_H_

#include "utils/singleton.h"
#include "graphics/itemstate/abstractitemstate.h"

namespace graphics
{
namespace itemstate
{
  /**
* The NotSelectedItemState class
* item state class for no selection state
*/
class NotSelectedItemState :
  public AbstractItemState,
  public pattern::Singleton<NotSelectedItemState>
{
  Q_OBJECT
  friend class pattern::Singleton<NotSelectedItemState>;

private:
  NotSelectedItemState(){}
  virtual ~NotSelectedItemState(){}
};

#define NOT_SELECTED_ITEM_STATE (::graphics::itemstate::NotSelectedItemState::Instance())

}
}
#endif

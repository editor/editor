/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _QPOINTLIST_H_
#define _QPOINTLIST_H_

#include <QPoint>
#include <QPointF>
#include <QVector>

namespace graphics
{
typedef QVector<QPoint> QPointList;
typedef QVector<QPointF> QPointFList;
}

#endif

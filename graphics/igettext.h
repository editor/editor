/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _IGETTEXT_H_
#define _IGETTEXT_H_

#include <QString>

namespace graphics
{

/**
 * The IGetText struct
 * Interface for getting a text string from user
 */
struct IGetText
{
  virtual ~IGetText() {};
  /**
   * GetText
   * get new or edited text.
   * If data exists, it is passed in as parameter
   * At the end of the call, the new data is in text
   * @param text
   * @return true: success, take over new text, false abort operation
   */
  virtual bool GetText(QString& text) = 0;
};

}
#endif

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <QPainter>
#include <QPointF>
#include "utils/cast.h"
#include "utils/assert.h"
#include "graphics/items/areaitem.h"
#include "graphics/itemstate/notselecteditemstate.h"
#include "graphics/itemstate/nodeselectitemstate.h"

namespace graphics
{

namespace items
{

DEFINE_CLASS_LOGGER(AreaItem);

const char* AreaItem::ItemName = "area";

const qreal AreaItem::m_selectionWidth = 20;

AreaItem::AreaItem() :
  AbstractPolygonItem(ItemName),
  m_closed(false)
{}

bool AreaItem::IsClosed() const
{
  return m_closed;
}

void AreaItem::SetClosed(bool closed)
{
  m_closed = closed;
  emit SigItemChanged();
  CalculateItem();
  update();
}

int AreaItem::type() const
{
  return Type;
}

void AreaItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
  AbstractPolygonItem::paint(painter, option, widget);

  painter->save();

  QPainterPath path;
  path.addPolygon(m_polygon);

  QPen outlinePen;
  if (m_outline.Width > 0)
  {
    outlinePen.setColor(m_outline.Color);
    outlinePen.setWidth(m_outline.Width);

    outlinePen.setJoinStyle(m_pen.joinStyle());
    outlinePen.setCapStyle(m_pen.capStyle());
    outlinePen.setStyle(Qt::SolidLine);

    painter->setPen(outlinePen);
    if (m_closed)
    {
      // draw a closed polygon (start point and end point are equals)
      QPolygonF temp = m_polygon;
      temp << temp[0];
      painter->drawPolyline(temp);
    }
    else
    {
      painter->drawPolyline(m_polygon);
    }
  }

  painter->setPen(m_pen);
  if (m_closed)
  {
    painter->setBrush(m_brush);
    painter->drawPolygon(m_polygon);
  }
  else
  {
    painter->drawPolyline(m_polygon);
  }

  painter->restore();
}

QPainterPath AreaItem::shape() const
{
  if (m_closed)
  {
    QPainterPath path;
    path.addPolygon(m_polygon);
    return path;
  }
  else
  {
    QPainterPath path;
    path.addPolygon(m_polygon);
    QPainterPathStroker stroker;
    qreal width = std::max(m_pen.widthF(), m_outline.Width);
    width = std::max(m_selectionWidth, width);
    stroker.setWidth(width);
    return stroker.createStroke(path);
  }
}

QRectF AreaItem::boundingRect() const
{
  QRectF br = m_polygon.boundingRect();
  const qreal d = std::max(m_pen.widthF(), m_outline.Width);
  br.adjust(-d, -d, d, d);
  return br;
}

Properties AreaItem::GetSupportedProperties() const
{
  return AbstractPolygonItem::GetSupportedProperties() | PROP_BRUSH;
}

QVariant AreaItem::GetPropertyValue(Property p) const
{
  switch (p)
  {
  case PROP_BRUSH:
  return m_brush;
  default:
  return AbstractPolygonItem::GetPropertyValue(p);
  }
}

void AreaItem::SetPropertyValueImpl(Property p, const QVariant& value)
{
  switch (p)
  {
  case PROP_BRUSH:
    m_brush = value.value<QBrush>();
  break;
  default:
    AbstractPolygonItem::SetPropertyValueImpl(p, value);
  break;
  }
}

void AreaItem::CalculateItem()
{
// nop
}

QPainterPath AreaItem::CreateTextPath(const QString& text)
{
  QPainterPath path;
  QRectF textRect = GetTextRect(text);

  QPointF textPos = QPointF(m_polygon.boundingRect().center().x() - textRect.width()/2,
                            m_polygon.boundingRect().center().y() - textRect.height()/2);

  path.addText(textPos, m_font, text);
  return path;
}

QRectF AreaItem::GetTextRect(const QString& text) const
{
  // BUG IN QT: https://bugreports.qt-project.org/browse/QTBUG-15974
  // the size does not cover all the characters to be displayed.
  QFontMetrics fm(m_font);
  QRectF r = fm.boundingRect(text);
  return QRectF(QPointF(0, 0), QSizeF(r.width()*1.3, r.height()));
}

}
}

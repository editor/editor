/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _POLYLINEITEM_H_
#define _POLYLINEITEM_H_

#include <QPen>
#include <QPolygonF>
#include "graphics/items/abstractpolygonitem.h"
#include "graphics/items/customitemtype.h"
#include "graphics/scenestates/autoqpointer.h"

namespace graphics
{
namespace items
{

/**
* The PolylineItem class
* Class to handle two point lines and multi point lines.
*/
class PolylineItem :
  public AbstractPolygonItem
{
  Q_OBJECT
  DECLARE_CLASS_LOGGER

public:
    void Accept(BaseVisitor& visitor) override
    {
        LOG_INFO("PolylineItem::Accept");
        visitor.Visit(*this);
    };

public:
  PolylineItem();

public:
  /**
   * type return the type of the class
   * @return type
   */
  virtual int type() const;
  /**
   * boundingRect
   * This virtual function defines the outer bounds of the item as a rectangle;
   * all painting must be restricted to inside an item's bounding rect.
   * @return QRectF
   */
  virtual QRectF boundingRect() const;
  /**
   * shape
   * Returns the shape of this item as a QPainterPath in local coordinates.
   * The shape is used for many things, including collision detection, hit tests, and for the QGraphicsScene::items() functions.
   * @return QPainterPath
   */
  virtual QPainterPath shape() const;

protected:
  /**
   * paint
   * This function paints the contents of an item in local coordinates.
   * @param painter
   * @param option
   * @param widget
   */
  virtual void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget = nullptr);

public:
  static const char* ItemName;
  static const int Type = CI_POLYLINE_ITEM_TYPE;

protected:
  /*! @copydoc AbstractItem::CalculateItem() */
  virtual void CalculateItem();

private:
  /**
   * CreateWingPath
   * create a rectangular wing at the start- or endpoint of the line
   * @param start true: start point, false: end point
   * @return QPainterPath
   */
  QPainterPath CreateWingPath(bool start) const;

  /**
   * CreateTextPath
   * Convert the given text to a QPainterPath.
   * The text will be aligned btw first and second node
   * @param text
   * @return QPainterPath
   */
  QPainterPath CreateTextPath(const QString& text);

private:
  QPainterPath m_textPath;
  static const qreal m_wingLen;
  static const qreal m_selectionWidth;
};

typedef graphics::scenestate::AutoQPointer<items::PolylineItem> PolylineItemAP;

}
}

#endif

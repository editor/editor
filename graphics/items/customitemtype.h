/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _CUSTOMITEMTYPE_H_
#define _CUSTOMITEMTYPE_H_

#include "graphics/nodeitem/customitemtypeprivate.h"
#include <QGraphicsItem>
#include <QString>

namespace graphics
{
namespace items
{

typedef int CustomItemType;

enum CustomItemTypePublic
{
  CI_AREA_ITEM_TYPE = nodeitem::_CI_LAST_PRIVATE,
  CI_ELLIPSE_ITEM_TYPE,
  CI_PEN_ITEM_TYPE,
  CI_POLYLINE_ITEM_TYPE,
  CI_TEXT_ITEM_TYPE,
  _CI_LAST_PUBLIC
};

const char* CustomItemType2Char(CustomItemType it);
}
}
#endif

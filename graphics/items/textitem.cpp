/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <QPainter>
#include "utils/cast.h"
#include "utils/assert.h"
#include "graphics/items/textitem.h"
#include "graphics/graphicsscene.h"
#include "graphics/itemstate/notselecteditemstate.h"
#include "graphics/itemstate/simpleselectitemstate.h"

namespace graphics
{
namespace items
{

DEFINE_CLASS_LOGGER(TextItem);

const char* TextItem::ItemName = "text";

AbstractItem::StateList TextItem::m_stateList;

TextItem::TextItem() :
  AbstractItem(ItemName)
{}

void TextItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
  AbstractItem::paint(painter, option, widget);

  painter->save();

  // Black pen with an off-white, 50% opacity background
  QRectF textRect = GetTextRect();

  painter->setOpacity(0.3);
  painter->fillRect(textRect, QBrush("whitesmoke"));

  painter->setOpacity(1.0);
  painter->setPen(m_pen);
  painter->setFont(m_font);
  LOG_INFO("TextItem::paint Font: %s, %f", painter->fontInfo().family().toLocal8Bit().data(), painter->fontInfo().pointSizeF());

  painter->setBrush(m_brush);
  painter->drawText(textRect, Qt::TextDontClip, m_text);		// demo: rectangle is too small for text

  painter->restore();
}

QRectF TextItem::boundingRect() const
{
  return GetTextRect().adjusted(-2,-2,2,2);
}

QRectF TextItem::GetTextRect() const
{
  // BUG IN QT: https://bugreports.qt-project.org/browse/QTBUG-15974
  // the size does not cover all the characters to be displayed.
  QFontMetrics fm(m_font);
  QRectF r = fm.boundingRect(m_text);
  return QRectF(QPointF(0, 0), QSizeF(r.width()*1.3, r.height())); // TODO: magic number
}

int TextItem::type() const
{
  return Type;
}

AbstractItem::Options TextItem::GetOptions() const
{
  return OPT_SIMPLE_SELECTABLE | OPT_MOVABLE | OPT_DELETABLE;
}

Properties TextItem::GetSupportedProperties() const
{
  return AbstractItem::GetSupportedProperties() | PROP_PEN | PROP_BRUSH | PROP_FONT;
}

QVariant TextItem::GetPropertyValue(Property p) const
{
  switch (p)
  {
  case PROP_PEN:
  return m_pen;
  case PROP_BRUSH:
  return m_brush;
  case PROP_FONT:
  return m_font;
  case PROP_TEXT:
  return m_text;
  default:
  return AbstractItem::GetPropertyValue(p);
  }
}

void TextItem::SetPropertyValueImpl(Property p, const QVariant& value)
{
  switch (p)
  {
  case PROP_PEN:
    m_pen = value.value<QPen>();
    update();
  break;
  case PROP_BRUSH:
    m_brush = value.value<QBrush>();
    update();
  break;
  case PROP_FONT:
    m_font = value.value<QFont>();
    update();
  break;
  case PROP_TEXT:
    m_text = value.value<QString>();
    update();
  break;
  default:
    AbstractItem::SetPropertyValueImpl(p, value);
  break;
  }
}

QString TextItem::GetText() const
{
  return m_text;
}

void TextItem::SetText(const QString& text)
{
  prepareGeometryChange();
  m_text = text;
  emit SigItemChanged();
}

const AbstractItem::StateList& TextItem::GetStateList() const
{
  // static list, do this once
  if (m_stateList.empty())
  {
    m_stateList.push_back(&NOT_SELECTED_ITEM_STATE);
    m_stateList.push_back(&SIMPLE_SELECT_ITEM_STATE);
  }
  return m_stateList;
}

}
}

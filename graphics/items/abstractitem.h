/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _ABSTRACTITEM_H_
#define _ABSTRACTITEM_H_

#include <memory>
#include "utils/basevisitor.h"
#include "utils/baseitem.h"
#include <QGraphicsObject>
#include "utils/log.h"
#include "utils/macros.h"
#include "graphics/qpointlist.h"
#include "graphics/nodeitem/editnodeitem.h"
#include "graphics/itemstate/abstractitemstate.h"
#include "graphics/items/itemlist.h"
#include "graphics/iproperties.h"
#include "graphics/throwexceptioncatchall.h"

namespace graphics
{
  namespace items
  {
    /**
 * The AbstractItem class
 * abstract class for various graphical items.
 */
    class AbstractItem :
        public QGraphicsObject,
        public BaseItem,
        public IProperties
    {
      Q_OBJECT
      DECLARE_CLASS_LOGGER

      protected:
        AbstractItem(const QString& itemTypeName);

    public:
      virtual ~AbstractItem();

      // Qt methods //////////////////////////////////////////////////////////////////////////////////////////
    protected:
      /**
         * itemChange
         * This virtual function is called by QGraphicsItem to notify custom items that some part of the item's state changes.
         * @param change
         * @param value
         * @return QVariant
         */
      virtual QVariant itemChange(GraphicsItemChange change, const QVariant& value);
    public:
      /**
         * GetSelectedRect
         * return the rectangle which should be used to render the selection rect.
         * @return QRectF
         */
      virtual QRectF GetSelectedRect() const;
      /**
         * paint
         * This function paints the contents of an item in local coordinates.
         * @param painter
         * @param option
         * @param widget
         */
      virtual void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget);
      // /////////////////////////////////////////////////////////////////////////////////////////////////////

      // overwrite the following two methods to support edit nodes ///////////////////////////////////////////
    public: // Editable nodes
      /**
         * GetEditNodePositions
         * @return a list of edit node positions.
         */
      virtual QPointFList GetEditNodePositions() const;
      /**
         * MoveEditNodeTo
         * move the edit node with given id to the given position.
         * @param id
         * @param position
         */
      virtual void MoveEditNodeTo(nodeitem::NodeId id, const QPointF& position);

    signals:
      /**
         * SigEditNodesChanged
         * Signal which is emitted, when a node has changed.
         */
      void SigEditNodesChanged();
      // /////////////////////////////////////////////////////////////////////////////////////////////////////

      // state handling /////////////////////////////////////////////////////////////////////////////////////
    protected:
      //NOTE auto_ptr not implemented because states are not deleted
      typedef QVector<itemstate::AbstractItemState*> StateList;
    public:
      /**
         * GotoFirstSelectedState
         * switch to the first selected state.
         */
      void GotoFirstSelectedState();
      /**
         * GotoFirstNotSelectedState
         * switch to the first not selected state.
         */
      void GotoFirstNotSelectedState();
      /**
         * GotoNextState
         * switch to next state
         */
      void GotoNextState();
      /**
         * GotoState
         * switch to the given state
         * @param next state to switch to
         */
      void GotoState(StateList::size_type next);
    protected:
      /**
         * GetStateList
         * get the list of the states for the item.
         * @return StateList
         */
      virtual const StateList& GetStateList() const = 0;
    private:
      /**
         * GetCurrentState get get current stae
         * @return AbstractItemState
         */
      const itemstate::AbstractItemState& GetCurrentState() const;
      itemstate::AbstractItemState& GetCurrentState();
    private:
      StateList::size_type _currentStateIdx;
      // /////////////////////////////////////////////////////////////////////////////////////////////////////

      // measurement ////////////////////////////////////////////////////////////////////////////////////////
    public:
      /**
         * GetMeasureModel$
         * get the measure model of the item
         * @return AbstractMeasureModel
         */
      //virtual const ms::AbstractMeasureModel* GetMeasureModel() const;
#ifdef graphics_USE_MEASURE
    public:
      /**
         * GetMultiPointRefPointList
         * get the multi point reference list
         * @return MultiPointRefPointList
         */
      //virtual const ms::MultiPointRefPointList* GetMultiPointRefPointList() const;
#endif
      // /////////////////////////////////////////////////////////////////////////////////////////////////////

      // options ////////////////////////////////////////////////////////////////////////////////////////////
    public:
      enum Option
      {
        OPT_SIMPLE_SELECTABLE   = 1 << 0,
        OPT_NODE_SELECTABLE     = 1 << 1,
        OPT_MOVABLE             = 1 << 2,
        OPT_DELETABLE           = 1 << 3
      };
      Q_DECLARE_FLAGS(Options, Option);

    public:
      /**
         * GetOptions
         * get the options for the item.
         * This is pure virtual and needs to be implemented by the derived classes.
         * @return Options
         */
      virtual Options GetOptions() const = 0;
      // convenience: Flag queries for GetOptions
      bool IsSimpleSelectable() const;
      bool IsNodeSelectable() const;
      bool IsSelectable() const;
      bool IsMovable() const;
      bool IsDeletable() const;
      bool IsSelected() const;
      // /////////////////////////////////////////////////////////////////////////////////////////////////////

      // properties ////////////////////////////////////////////////////////////////////////////////////////////
    public:
      /**
         * GetSupportedProperties
         * return all properties for the item
         * @return Properties
         */
      virtual Properties GetSupportedProperties() const;
      /**
         * GetPropertyValue
         * get the value for a given property p.
         * @param p
         * @return QVariant
         */
      virtual QVariant GetPropertyValue(Property p) const;
      /**
         * SetPropertyValue
         * set the value (value) for a given property p.
         * @param p
         * @param value
         */
      virtual void SetPropertyValue(Property p, const QVariant& value);

      virtual void RegisterToSigPropertyValueChanged(QObject* receiver, const char* slot);

    protected:
      /**
         * SetPropertyValueImpl
         * Set the property value.
         * This operation is virtual and can be overwritten in a derived class.
         * @param p
         * @param value
         */
      virtual void SetPropertyValueImpl(Property p, const QVariant& value);

      // signal item has changed /////////////////////////////////////////////////////////////////////////////
    signals:
      /**
         * SigItemChanged
         * Qt signal which is emitted when a item has changed.
         */
      void SigItemChanged();
      void SigPropertyValueChanged(graphics::Property p, QVariant value);
      void SigStateChanged();
      // /////////////////////////////////////////////////////////////////////////////////////////////////////
    };

    Q_DECLARE_OPERATORS_FOR_FLAGS(AbstractItem::Options);

    typedef std::unique_ptr<AbstractItem> AbstractItemAP;

  }
}

#endif


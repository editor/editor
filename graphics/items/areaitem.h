/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _AREAITEM_H_
#define _AREAITEM_H_

#include <QBrush>
#include <QColor>
#include <QPainter>
#include <QPen>
#include "utils/log.h"
#include "graphics/items/abstractpolygonitem.h"
#include "graphics/items/customitemtype.h"
#include "graphics/scenestates/autoqpointer.h"

namespace graphics
{
namespace items
{

/**
 * The AreaItem class
 * Class for handling open or closed polygons
 */
  class AreaItem :
      public AbstractPolygonItem
  {
    Q_OBJECT
  public:
    DECLARE_CLASS_LOGGER

      void Accept(BaseVisitor& visitor) override
      {
          LOG_INFO("AreaItem::Accept");
          visitor.Visit(*this);
      };

    public:
      AreaItem();

  public:
    /**
       * boundingRect
       * This virtual function defines the outer bounds of the item as a rectangle;
       * all painting must be restricted to inside an item's bounding rect.
       * @return QRectF
       */
    virtual QRectF boundingRect() const;

    /**
       * shape
       * Returns the shape of this item as a QPainterPath in local coordinates.
       * The shape is used for many things, including collision detection, hit tests, and for the QGraphicsScene::items() functions.
       * @return QPainterPath
       */
    virtual QPainterPath shape() const;
    /**
       * paint
       * This function paints the contents of an item in local coordinates.
       * @param painter
       * @param option
       * @param widget
       */
    virtual void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget = nullptr);

  public:
    /**
       * type return the type of the class
       * @return type
       */
    virtual int type() const;
    /*! @copydoc AbstractItem::GetSupportedProperties() */
    virtual Properties GetSupportedProperties() const;
    /*! @copydoc AbstractItem::GetPropertyValue() */
    virtual QVariant GetPropertyValue(Property p) const;

    // area item specific
    /**
       * IsClosed
       * returns true, if the area of the area item is closed
       * @return bool
       */
    bool IsClosed() const;
    /**
       * SetClosed
       * set the area item to the closed state
       * @param closed (true: set closed, false: set open)
       */
    void SetClosed(bool closed = true);

  protected:
    /*! @copydoc AbstractItem::SetPropertyValueImpl() */
    virtual void SetPropertyValueImpl(Property p, const QVariant& value);

  public:
    /*! @copydoc AbstractItem::SetMeasureModel() */
    //virtual void SetMeasureModel(const ms::AbstractMeasureModel* model);
    /*! @copydoc AbstractItem::GetMeasureModel() */
    //virtual const ms::AbstractMeasureModel* GetMeasureModel() const;

  public:
    static const char* ItemName;
    static const int Type = CI_AREA_ITEM_TYPE;

  protected:
    /*! @copydoc AbstractItem::CalculateItem() */
    virtual void CalculateItem();

  private:
    QRectF GetTextRect(const QString& text) const;
    QPainterPath CreateTextPath(const QString& text);

  private:

    static const qreal m_selectionWidth;
    QPainterPath m_textPath;
    QString _measuredArea;
    QBrush m_brush;
    bool m_closed;
  };

  typedef graphics::scenestate::AutoQPointer<items::AreaItem> AreaItemAP;

}
}

#endif


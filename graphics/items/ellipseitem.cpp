/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "utils/assert.h"
#include "graphics/items/ellipseitem.h"
#include "graphics/itemstate/notselecteditemstate.h"
#include "graphics/itemstate/nodeselectitemstate.h"

namespace graphics
{
namespace items
{

DEFINE_CLASS_LOGGER(EllipseItem);

const char* EllipseItem::ItemName = "ellipse";

AbstractItem::StateList EllipseItem::m_stateList;

EllipseItem::EllipseItem() :
  AbstractItem(ItemName)
{}

QPointFList EllipseItem::GetEditNodePositions() const
{
  const qreal x = 0;
  const qreal y = 0;
  const qreal x2 = m_size.width() / 2;
  const qreal y2 = m_size.height() / 2;
  const qreal x3 = m_size.width();
  const qreal y3 = m_size.height();

  QPointFList positions(4);
  positions[TOP] = QPointF(x2, y);
  positions[LEFT] = QPointF(x, y2);
  positions[RIGHT] = QPointF(x3, y2);
  positions[BOTTOM] = QPointF(x2, y3);
  return positions;
}

void EllipseItem::MoveEditNodeTo(nodeitem::NodeId id, const QPointF& position)
{
  QSizeF size;
  switch (id)
  {
  case TOP:
  {
    const qreal dy = mapToScene(0, position.y()).y() - pos().y();
    setPos(pos().x(), pos().y() + dy);
    size = QSizeF(GetSize().width(), GetSize().height() - dy);
  }
  break;

  case LEFT:
  {
    const qreal dx = mapToScene(position.x(), 0).x() - pos().x();
    setPos(pos().x() + dx, pos().y());
    size = QSizeF(GetSize().width() - dx, GetSize().height());
  }
  break;

  case RIGHT:
    size = QSizeF(position.x(), GetSize().height());
  break;

  case BOTTOM:
    size = QSizeF(GetSize().width(), position.y());
  break;

  default:
    ASSERT(false);
  break;
  }
  size.setWidth(std::max<qreal>(0, size.width()));
  size.setHeight(std::max<qreal>(0, size.height()));
  SetSize(size);
}

void EllipseItem::SetSize(const QSizeF& size)
{
  prepareGeometryChange();
  m_size = size;
  emit SigItemChanged();
  emit SigEditNodesChanged();
}

const QSizeF& EllipseItem::GetSize() const
{
  return m_size;
}

void EllipseItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
  AbstractItem::paint(painter, option, widget);
  painter->save();
  painter->setPen(m_pen);
  painter->setBrush(m_brush);
  const QRectF rect(QPoint(0, 0), m_size);
  painter->drawEllipse(rect);
  painter->restore();
}

QPainterPath EllipseItem::shape() const
{
  QPainterPath path;
  const QRectF rect(QPointF(0, 0), m_size);
  path.addEllipse(rect);
  return path;
}

const AbstractItem::StateList& EllipseItem::GetStateList() const
{
  // static list, do this once
  if (m_stateList.empty())
  {
    m_stateList.push_back(&NOT_SELECTED_ITEM_STATE);
    m_stateList.push_back(&NODE_SELECT_ITEM_STATE);
  }
  return m_stateList;
}

Properties EllipseItem::GetSupportedProperties() const
{
  return AbstractItem::GetSupportedProperties() | PROP_PEN | PROP_BRUSH;
}

QVariant EllipseItem::GetPropertyValue(Property p) const
{
  switch (p)
  {
  case PROP_PEN:
  return m_pen;
  case PROP_BRUSH:
  return m_brush;
  default:
  return AbstractItem::GetPropertyValue(p);
  }
}

void EllipseItem::SetPropertyValueImpl(Property p, const QVariant& value)
{
  switch (p)
  {
  case PROP_PEN:
    m_pen = value.value<QPen>();
  break;
  case PROP_BRUSH:
    m_brush = value.value<QBrush>();
  break;
  default:
    AbstractItem::SetPropertyValueImpl(p, value);
  break;
  }
}

QRectF EllipseItem::boundingRect() const
{
  QRectF rect(QPointF(0, 0), m_size);
  const qreal w = m_pen.widthF();
  rect.adjust(-w, -w, w, w);
  return rect;
}

int EllipseItem::type() const
{
  return Type;
}

items::AbstractItem::Options EllipseItem::GetOptions() const
{
  return OPT_NODE_SELECTABLE | OPT_MOVABLE | OPT_DELETABLE;
}

}
}


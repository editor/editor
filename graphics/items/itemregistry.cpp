/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "utils/registrar.h"
#include "utils/macros.h"
#include "graphics/items/itemregistry.h"
#include "graphics/items/areaitem.h"
#include "graphics/items/ellipseitem.h"
#include "graphics/items/penitem.h"
#include "graphics/items/polylineitem.h"
#include "graphics/items/textitem.h"

namespace graphics
{
namespace items
{

#define REGISTER_ITEM(ITEM) \
  static pattern::Registrar<ItemRegistry, ITEM> CONCAT(dummy, __LINE__)(ITEM::ItemName);

REGISTER_ITEM(AreaItem)
REGISTER_ITEM(EllipseItem)
REGISTER_ITEM(PenItem)
REGISTER_ITEM(PolylineItem)
REGISTER_ITEM(TextItem)

}
}

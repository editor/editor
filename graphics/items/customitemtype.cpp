/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "graphics/items/customitemtype.h"
#include "graphics/nodeitem/customitemtypeprivate.h"


namespace graphics
{
namespace items
{

#define STRINGIFY(ENUM) case ENUM: return #ENUM;

const char* CustomItemType2Char(CustomItemType it)
{
  switch (it)
  {
    STRINGIFY(CI_AREA_ITEM_TYPE)
    STRINGIFY(CI_ELLIPSE_ITEM_TYPE)
    STRINGIFY(CI_TEXT_ITEM_TYPE)
    STRINGIFY(CI_PEN_ITEM_TYPE)
    default: return nodeitem::CustomItemType2CharPrivate(it);
  }
}

}
}

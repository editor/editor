/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _ABSTRACTPOLYGONITEM_H_
#define _ABSTRACTPOLYGONITEM_H_

#include <QFont>
#include <QPen>
#include <QPolygonF>
#include "utils/log.h"
#include "graphics/outline.h"
#include "graphics/items/abstractitem.h"

namespace graphics
{
  namespace items
  {

/**
 * The AbstractPolygonItem class
 * abstract class to handle polygon items.
 * The specialization in derived classes.
 */
    class AbstractPolygonItem :
        public AbstractItem
    {
      Q_OBJECT

    protected:
      AbstractPolygonItem(const QString& itemTypeName);

    public:
      /*! @copydoc AbstractItem::GetSelectedRect() */
      virtual QRectF GetSelectedRect() const;
      /*! @copydoc AbstractItem::GetOptions() */
      virtual Options GetOptions() const;
      /*! @copydoc AbstractItem::GetSupportedProperties() */
      virtual Properties GetSupportedProperties() const;
      /*! @copydoc AbstractItem::GetPropertyValue() */
      virtual QVariant GetPropertyValue(Property p) const;
      /*! @copydoc AbstractItem::GetEditNodePositions() */
      virtual QPointFList GetEditNodePositions() const;
      /*! @copydoc AbstractItem::MoveEditNodeTo() */
      virtual void MoveEditNodeTo(nodeitem::NodeId id, const QPointF& position);
    protected:
      /*! @copydoc AbstractItem::GetStateList() */
      virtual const StateList& GetStateList() const;
      /*! @copydoc AbstractItem::SetPropertyValueImpl() */
      virtual void SetPropertyValueImpl(Property p, const QVariant& value);

      //Polygone API
    public:
      /**
         * GetPolygon
         * @return get the polygon of the item
         */
      const QPolygonF& GetPolygon() const;
      /**
         * SetPolygon
         * set the polygon for the item
         * @param poly
         */
      void SetPolygon(const QPolygonF& poly);

      //convenience
      /**
         * SetNodePosition
         * set the given node idx to the new position pos
         * @param idx
         * @param pos
         */
      void SetNodePosition(QPolygonF::size_type idx, const QPointF& pos);
      /**
         * GetNodePosition
         * get the position of the node idx
         * @param idx
         * @return QPointF
         */
      const QPointF& GetNodePosition(QPolygonF::size_type idx) const;
      /**
         * AddNode
         * add a new node at position pos
         * @param pos
         */
      void AddNode(const QPointF& pos);
      /**
         * RemoveNode
         * remove a node indexed by idx
         * @param idx
         */
      void RemoveNode(QPolygonF::size_type idx);

    protected:
      /**
         * CalculateItem
         * calculate the area or distance
         */
      virtual void CalculateItem() = 0;

    protected:
      QPolygonF m_polygon; //rendered polygon
      QFont m_font;        // font for measurment text
      QPen m_pen;
      Outline m_outline;

    private:
      static StateList m_stateList;
    };

  }
}

#endif

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "graphics/items/abstractpolygonitem.h"
#include "graphics/itemstate/notselecteditemstate.h"
#include "graphics/itemstate/nodeselectitemstate.h"

namespace graphics
{
namespace items
{

AbstractItem::StateList AbstractPolygonItem::m_stateList;

AbstractPolygonItem::AbstractPolygonItem(const QString& itemTypeName):
  AbstractItem(itemTypeName)
{}

QRectF AbstractPolygonItem::GetSelectedRect() const
{
  return m_polygon.boundingRect();
}

const AbstractItem::StateList& AbstractPolygonItem::GetStateList() const
{
  // static list, do this once
  if (m_stateList.empty())
  {
    m_stateList.push_back(&NOT_SELECTED_ITEM_STATE);
    m_stateList.push_back(&NODE_SELECT_ITEM_STATE);
  }
  return m_stateList;
}

const QPolygonF& AbstractPolygonItem::GetPolygon() const
{
  return m_polygon;
}

void AbstractPolygonItem::SetPolygon(const QPolygonF& poly)
{
  prepareGeometryChange();
  m_polygon = poly;
  CalculateItem();
  emit SigItemChanged();
  emit SigEditNodesChanged();
}

void AbstractPolygonItem::AddNode(const QPointF& pos)
{
  prepareGeometryChange();
  m_polygon << pos;
  CalculateItem();
  emit SigItemChanged();
  emit SigEditNodesChanged();
}

void AbstractPolygonItem::SetNodePosition(QPolygonF::size_type idx, const QPointF& pos)
{
  ASSERT(idx < m_polygon.size());
  prepareGeometryChange();
  m_polygon[idx] = pos;
  CalculateItem();
  emit SigItemChanged();
  emit SigEditNodesChanged();
}

void AbstractPolygonItem::RemoveNode(QPolygonF::size_type idx)
{
  ASSERT(idx < m_polygon.size());
  prepareGeometryChange();
  m_polygon.remove(idx);
  CalculateItem();
  emit SigItemChanged();
  emit SigEditNodesChanged();
}

items::AbstractItem::Options AbstractPolygonItem::GetOptions() const
{
  return OPT_NODE_SELECTABLE | OPT_MOVABLE | OPT_DELETABLE;
}

Properties AbstractPolygonItem::GetSupportedProperties() const
{
  return AbstractItem::GetSupportedProperties() | PROP_PEN | PROP_OUTLINE | PROP_FONT;
}

QVariant AbstractPolygonItem::GetPropertyValue(Property p) const
{
  switch (p)
  {
  case PROP_PEN:
  return m_pen;
  case PROP_FONT:
  return m_font;
  case PROP_OUTLINE:
  {
    QVariant var;
    var.setValue(m_outline);
    return var;
  }
  default:
  return AbstractItem::GetPropertyValue(p);
  }
}

void AbstractPolygonItem::SetPropertyValueImpl(Property p, const QVariant& value)
{
  switch (p)
  {
  case PROP_PEN:
    m_pen = value.value<QPen>();
  break;
  case PROP_FONT:
    m_font = value.value<QFont>();
  case PROP_OUTLINE:
    m_outline = value.value<graphics::Outline>();
  break;
  default:
    AbstractItem::SetPropertyValueImpl(p, value);
  break;
  }
}

QPointFList AbstractPolygonItem::GetEditNodePositions() const
{
  return m_polygon;
}

void AbstractPolygonItem::MoveEditNodeTo(nodeitem::NodeId id, const QPointF& position)
{
  SetNodePosition(id, position);
  CalculateItem();
}

const QPointF& AbstractPolygonItem::GetNodePosition(QPolygonF::size_type idx) const
{
  ASSERT(idx < m_polygon.size());
  return m_polygon[idx];
}

}
}

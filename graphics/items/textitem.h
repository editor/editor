/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _TEXTITEM_H_
#define _TEXTITEM_H_

#include <QColor>
#include <QFont>
#include <QPen>
#include "utils/log.h"
#include "graphics/items/abstractitem.h"
#include "graphics/items/customitemtype.h"
#include "graphics/scenestates/autoqpointer.h"

namespace graphics
{
namespace items
{

/**
* The TextItem class
* Class to handle text items
*/
class TextItem :
    public AbstractItem
{
  Q_OBJECT
  Q_PROPERTY(QString m_text READ GetText WRITE SetText)	// expose text string for test purposes
  DECLARE_CLASS_LOGGER

public:
void Accept(BaseVisitor& visitor) override
{
    LOG_INFO("TextItem::Accept");
    visitor.Visit(*this);
};


public:
  TextItem();

public:
  virtual int type() const;
  /*! @copydoc AbstractItem::GetOptions() */
  virtual Options GetOptions() const;
  /*! @copydoc AbstractItem::GetSupportedProperties() */
  virtual Properties GetSupportedProperties() const;
  /*! @copydoc AbstractItem::GetPropertyValue() */
  virtual QVariant GetPropertyValue(Property p) const;

  // S11n
  /**
     * GetText
     * get the text of the TextItem
     * @return QString
     */
  QString GetText() const;
  /**
     * SetText
     * set the text of the TextItem
     * @param text
     */
  void SetText(const QString& text);
protected:
  /*! @copydoc AbstractItem::SetPropertyValueImpl() */
  virtual void SetPropertyValueImpl(Property p, const QVariant& value);
  /**
     * boundingRect
     * This virtual function defines the outer bounds of the item as a rectangle;
     * all painting must be restricted to inside an item's bounding rect.
     * @return QRectF
     */
  virtual QRectF boundingRect() const;
  /**
     * GetTextRect
     * get the reactangle for the text
     * @return QRectF
     */
  QRectF GetTextRect() const;
  /**
     * paint
     * This function paints the contents of an item in local coordinates.
     * @param painter
     * @param option
     * @param widget
     */
  virtual void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget = nullptr);
  /*! @copydoc AbstractItem::GetStateList() */
  virtual const StateList& GetStateList() const;

public:
  static const char* ItemName;
  static const int Type = CI_TEXT_ITEM_TYPE;

private:
  QString m_text;
  QPen    m_pen;
  QBrush  m_brush;
  QFont   m_font;
  static StateList m_stateList;
};

typedef graphics::scenestate::AutoQPointer<graphics::items::TextItem> TextItemAP;

}
}

#endif

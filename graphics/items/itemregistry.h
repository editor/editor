/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _ITEMREGISTRY_H_
#define _ITEMREGISTRY_H_

#include "utils/ifactory.h"
#include "utils/registry.h"
#include "graphics/items/abstractitem.h"

namespace graphics
{
namespace items
{
typedef pattern::Registry<STRING, pattern::IFactory<graphics::items::AbstractItem> > ItemRegistry;
#define GRAPHICS_ITEM_REGISTRY (graphics::items::ItemRegistry::Instance())
}
}
#endif


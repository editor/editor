/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <QPainter>
#include "graphics/items/penitem.h"
#include "graphics/itemstate/notselecteditemstate.h"
#include "graphics/itemstate/simpleselectitemstate.h"

namespace graphics
{
namespace items
{

const qreal PenItem::m_selectionWidth = 30;

AbstractItem::StateList PenItem::m_stateList;

const char* PenItem::ItemName = "pen";

PenItem::PenItem() :
  AbstractItem(ItemName)
{}

int PenItem::type() const
{
  return Type;
}

items::AbstractItem::Options PenItem::GetOptions() const
{
  return (OPT_MOVABLE | OPT_DELETABLE);
}

Properties PenItem::GetSupportedProperties() const
{
  return AbstractItem::GetSupportedProperties() | PROP_PEN | PROP_OUTLINE;
}

QVariant PenItem::GetPropertyValue(Property p) const
{
  switch (p)
  {
  case PROP_PEN:
  return m_pen;
  case PROP_OUTLINE:
  {
    QVariant var;
    var.setValue(m_outline);
    return var;
  }
  default:
  return AbstractItem::GetPropertyValue(p);
  }
}

void PenItem::AddPoint(const QPointF& pt)
{
  prepareGeometryChange();
  m_points.append(pt);
  emit SigItemChanged();
}

void PenItem::RemovePoint(const QPolygonF::size_type idx)
{
  ASSERT(idx < m_points.size());
  prepareGeometryChange();
  m_points.remove(idx);
  emit SigItemChanged();
}

void PenItem::SetPropertyValueImpl(Property p, const QVariant& value)
{
  switch (p)
  {
  case PROP_PEN:
    prepareGeometryChange();
    m_pen = value.value<QPen>();
  break;
  case PROP_OUTLINE:
    prepareGeometryChange();
    m_outline = value.value<graphics::Outline>();
  break;
  default:
    AbstractItem::SetPropertyValueImpl(p, value);
  break;
  }
}

void PenItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
  AbstractItem::paint(painter, option, widget);
  painter->save();
  const QPainterPath path = CreatePath();
  if (m_outline.Width > 0)
  {
    QPen outlinePen;
    outlinePen.setColor(m_outline.Color);
    outlinePen.setWidth(m_outline.Width);
    outlinePen.setJoinStyle(m_pen.joinStyle());
    outlinePen.setCapStyle(m_pen.capStyle());
    outlinePen.setStyle(Qt::SolidLine);
    painter->setPen(outlinePen);
    painter->drawPath(path);
  }
  painter->setPen(m_pen);
  painter->drawPath(path);
  painter->restore();
}

QPainterPath PenItem::shape() const
{
  QPainterPathStroker stroker;
  qreal width = std::max(m_pen.widthF(), m_outline.Width);
  width = std::max(m_selectionWidth, width);
  stroker.setWidth(width);
  return stroker.createStroke(CreatePath());
}

QPainterPath PenItem::CreatePath() const
{
  QPainterPath path;
  path.addPolygon(m_points);
  return path;
}

const AbstractItem::StateList& PenItem::GetStateList() const
{
  // static list, do this once
  if (m_stateList.empty())
  {
    m_stateList.push_back(&NOT_SELECTED_ITEM_STATE);
    m_stateList.push_back(&SIMPLE_SELECT_ITEM_STATE);
  }
  return m_stateList;
}

QRectF PenItem::boundingRect() const
{
  QRectF rect = m_points.boundingRect();
  qreal offset = std::max(m_pen.widthF(), m_outline.Width);
  rect.adjust(-offset, -offset, offset, offset);
  return rect;
}

const QPolygonF& PenItem::GetPolyline() const
{
  return m_points;
}

void PenItem::SetPolyline(const QPolygonF& poly)
{
  prepareGeometryChange();
  m_points = poly;
  emit SigItemChanged();
}

}
}

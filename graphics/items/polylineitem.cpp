/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <QFont>
#include <QFontMetrics>
#include <QPainter>
#include <QRectF>
#include <QTransform>
#include "utils/assert.h"
#include "graphics/items/polylineitem.h"
#include "graphics/itemstate/notselecteditemstate.h"
#include "graphics/itemstate/nodeselectitemstate.h"

namespace graphics
{
namespace items
{

DEFINE_CLASS_LOGGER(PolylineItem);

const char* PolylineItem::ItemName = "polyline";
const qreal PolylineItem::m_wingLen = 20;
const qreal PolylineItem::m_selectionWidth = 20;

PolylineItem::PolylineItem() :
  AbstractPolygonItem(ItemName)
{}

int PolylineItem::type() const
{
  return Type;
}

void PolylineItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
  AbstractPolygonItem::paint(painter, option, widget);

  painter->save();

  QPainterPath path;
  path.addPolygon(m_polygon);
  path.addPath(CreateWingPath(true));
  path.addPath(CreateWingPath(false));

  QPen outlinePen;
  if (m_outline.Width > 0)
  {
    outlinePen.setColor(m_outline.Color);
    outlinePen.setWidthF(m_outline.Width);

    outlinePen.setJoinStyle(m_pen.joinStyle());
    outlinePen.setCapStyle(m_pen.capStyle());
    outlinePen.setStyle(Qt::SolidLine);

    painter->setPen(outlinePen);
    painter->drawPath(path);
  }

  painter->setPen(m_pen);
  painter->drawPath(path);

  painter->restore();
}

QRectF PolylineItem::boundingRect() const
{
  QRectF br = m_polygon.boundingRect();
  qreal d = m_wingLen / 2 + std::max(m_pen.widthF(), m_outline.Width);
  br.adjust(-d, -d, d, d);

  if (m_textPath.isEmpty())
  {
    return br;
  }
  else
  {
    QRectF textRect = m_textPath.boundingRect();
    textRect.adjust(-d, -d, d, d);

    const QRectF united = br.united(textRect);
    return united;
  }
}

QPainterPath PolylineItem::shape() const
{
  QPainterPath path;
  path.addPolygon(m_polygon);
  QPainterPathStroker stroker;
  qreal width = std::max(m_pen.widthF(), m_outline.Width);
  width = std::max(m_selectionWidth, width);
  stroker.setWidth(width);
  return stroker.createStroke(path);
}

QPainterPath PolylineItem::CreateWingPath(bool start) const
{
  QPainterPath linePath;
  if (m_polygon.size() >= 2)
  {
    if (start)
    {
      QLineF line(m_polygon[0], m_polygon[1]);
      QLineF wing1 = line.normalVector();
      wing1.setLength(m_wingLen);
      wing1.translate(wing1.p1() - wing1.pointAt(.5));

      QPainterPath path;
      path.moveTo(wing1.p1());
      path.lineTo(wing1.p2());
      linePath = path;
    }
    else
    {
      QLineF line(m_polygon[m_polygon.size() - 1], m_polygon[m_polygon.size() - 2]);
      QLineF wing2 = line.normalVector();
      wing2.setLength(m_wingLen);
      wing2.translate(wing2.p1() - wing2.pointAt(.5) );

      QPainterPath path;
      path.moveTo(wing2.p1());
      path.lineTo(wing2.p2());
      linePath = path;
    }
  }
  return linePath;
}

void PolylineItem::CalculateItem()
{
  // nop
}

QPainterPath PolylineItem::CreateTextPath(const QString& text)
{
  QPainterPath path;
  if (m_polygon.size() >= 2)
  {
    // Set up transform to drawn text above the line
    QTransform textTrans;
    QLineF line(m_polygon[0],m_polygon[1]);

    if (90.0 < line.angle()  &&  line.angle() < 270.0)
    {
      textTrans.translate(line.p2().x(), line.p2().y());
      textTrans.rotate(180 -line.angle());
    }
    else
    {
      textTrans.translate(line.p1().x(), line.p1().y());
      textTrans.rotate(-line.angle());
    }

    // Center text above and along the line
    QFontMetrics fm(m_font);
    QRectF textRectf = fm.boundingRect(text);
    QPointF baseline((line.length() - textRectf.width())/2,
                     - fm.descent() - std::max(m_pen.widthF(), m_outline.Width));

    // Create the path using the transform
    QPainterPath tpath;
    QPainterPath mappedPath;
    tpath.addText(baseline, m_font, text);
    mappedPath.addPath(textTrans.map(tpath));
    path = mappedPath;
  }
  return path;
}

}
}

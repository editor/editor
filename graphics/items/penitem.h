/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _PENITEM_H_
#define _PENITEM_H_

#include <QPainter>
#include <QPen>
#include "graphics/outline.h"
#include "graphics/items/abstractitem.h"
#include "graphics/items/customitemtype.h"
#include "graphics/scenestates/autoqpointer.h"

namespace graphics
{
namespace items
{

  /**
* The PenItem class
* Class to handle freehand drawing.
*/
class PenItem :
  public AbstractItem
{
  Q_OBJECT

public:
    void Accept(BaseVisitor& visitor) override
    {
        LOG_INFO("PenItem::Accept");
        visitor.Visit(*this);
    };

public:
  PenItem();

public:
  /**
     * type return the type of the class
     * @return type
     */
  virtual int type() const;
  /**
     * boundingRect
     * This virtual function defines the outer bounds of the item as a rectangle;
     * all painting must be restricted to inside an item's bounding rect.
     * @return QRectF
     */
  virtual QRectF boundingRect() const; // for Qt (mouse hits)

  /**
     * paint
     * This function paints the contents of an item in local coordinates.
     * @param painter
     * @param option
     * @param widget
     */
  virtual void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget = nullptr);
  /**
     * shape
     * Returns the shape of this item as a QPainterPath in local coordinates.
     * The shape is used for many things, including collision detection, hit tests, and for the QGraphicsScene::items() functions.
     * @return QPainterPath
     */
  virtual QPainterPath shape() const;

public:
  /*! @copydoc AbstractItem::GetOptions() */
  virtual Options GetOptions() const;
  /*! @copydoc AbstractItem::GetSupportedProperties() */
  virtual Properties GetSupportedProperties() const;
  /*! @copydoc AbstractItem::GetPropertyValue() */
  virtual QVariant GetPropertyValue(Property p) const;

  /**
     * AddPoint
     * add a point to the pen
     * @param pt
     */
  void AddPoint(const QPointF& pt);
  void RemovePoint(const QPolygonF::size_type idx);

  /**
     * GetPolyline
     * get the polyline of the pen
     * @return QPolygonF
     */
  const QPolygonF& GetPolyline() const;

  /**
     * SetPolyline
     * set the polyline for the pen
     * @param poly
     */
  void SetPolyline(const QPolygonF &poly);

protected:
  /*! @copydoc AbstractItem::SetPropertyValueImpl() */
  virtual void SetPropertyValueImpl(Property p, const QVariant& value);


  /*! @copydoc AbstractItem::GetStateList() */
  virtual const StateList& GetStateList() const;

private:
  /**
     * CreatePath
     * create a QPainterPath along the pen line
     * @return QPainterPath
     */
  QPainterPath CreatePath() const;

public:
  static const char* ItemName;
  static const int Type = CI_PEN_ITEM_TYPE;

private:
  QPolygonF m_points;
  QPen m_pen;
  Outline m_outline;
  static StateList m_stateList;
  static const qreal m_selectionWidth;
};

typedef graphics::scenestate::AutoQPointer<PenItem>PenItemAP;

}
}

#endif

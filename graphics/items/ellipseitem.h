/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _ELLIPSEITEM_H_
#define _ELLIPSEITEM_H_

#include <QPainter>
#include <QSizeF>
#include "utils/log.h"
#include "graphics/items/abstractitem.h"
#include "graphics/items/customitemtype.h"

namespace graphics
{
namespace items
{

/**
* The EllipseItem class
* Class to handle ellipse items.
*/
class EllipseItem :
  public AbstractItem
{
  Q_OBJECT
  DECLARE_CLASS_LOGGER

public:
    void Accept(BaseVisitor& visitor) override
    {
        LOG_INFO("EllipseItem::Accept");
        visitor.Visit(*this);
    };

public:
  EllipseItem();

public:
  /**
     * type return the type of the class
     * @return type
     */
  virtual int type() const;
  /**
     * boundingRect
     * This virtual function defines the outer bounds of the item as a rectangle;
     * all painting must be restricted to inside an item's bounding rect.
     * @return QRectF
     */
  virtual QRectF boundingRect() const; // for Qt mouse hits

  /**
     * paint
     * This function paints the contents of an item in local coordinates.
     * @param painter
     * @param option
     * @param widget
     */
  virtual void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget = nullptr);

  /**
     * shape
     * Returns the shape of this item as a QPainterPath in local coordinates.
     * The shape is used for many things, including collision detection, hit tests, and for the QGraphicsScene::items() functions.
     * @return QPainterPath
     */
  virtual QPainterPath shape() const;

public:
  /*! @copydoc AbstractItem::GetOptions() */
  virtual Options GetOptions() const;
  /*! @copydoc AbstractItem::GetSupportedProperties() */
  virtual Properties GetSupportedProperties() const;
  /*! @copydoc AbstractItem::GetPropertyValue() */
  virtual QVariant GetPropertyValue(Property p) const;
  /*! @copydoc AbstractItem::GetEditNodePositions() */
  virtual QPointFList GetEditNodePositions() const;
  /*! @copydoc AbstractItem::MoveEditNodeTo() */
  virtual void MoveEditNodeTo(nodeitem::NodeId id, const QPointF& position);
protected:
  /*! @copydoc AbstractItem::SetPropertyValueImpl() */
  virtual void SetPropertyValueImpl(Property p, const QVariant& value);
  /*! @copydoc AbstractItem::GetStateList() */
  virtual const StateList& GetStateList() const;

public:
  /**
     * SetRect set ellipse rectangle
     * @param rect
     */
  void SetSize(const QSizeF& size);
  /**
     * GetRect get the surrounding rectangle for the ellipse
     * @return QRectF
     */
  const QSizeF& GetSize() const;

public:
  static const char* ItemName;
  static const int Type = CI_ELLIPSE_ITEM_TYPE;

private:
  enum EditNodeIndex
  {
    TOP,
    LEFT,
    RIGHT,
    BOTTOM
  };

  static StateList m_stateList;
  QSizeF m_size;
  QPen m_pen;
  QBrush m_brush;
};

}
}

#endif


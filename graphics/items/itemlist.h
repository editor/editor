/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _ITEMLIST_H_
#define _ITEMLIST_H_

#include <QVector>

namespace graphics
{
namespace items
{

class AbstractItem;
typedef QVector<AbstractItem*> ItemList;

}
}

#endif

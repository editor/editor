/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <stdexcept>
#include <QGraphicsSceneMouseEvent>
#include <QBrush>
#include <QFont>
#include <QPen>
#include "utils/assert.h"
#include "utils/macros.h"
#include "utils/cast.h"
#include "utils/macros.h"
#include "graphics/items/abstractitem.h"

namespace graphics
{

namespace items
{

DEFINE_CLASS_LOGGER(AbstractItem);

AbstractItem::AbstractItem(const QString& itemTypeName) :
  QGraphicsObject(nullptr),
  _currentStateIdx(0)
{
  UNUSED_VAR(itemTypeName);
  ASSERT(!itemTypeName.isEmpty());
}

AbstractItem::~AbstractItem()
{
  emit SigItemChanged();
}

QPointFList AbstractItem::GetEditNodePositions() const
{
  throw std::logic_error("not implemented");
}

void AbstractItem::MoveEditNodeTo(nodeitem::NodeId id, const QPointF& position)
{
  UNUSED_VAR(id);
  UNUSED_VAR(position);
  throw std::logic_error("not implemented");
}

bool AbstractItem::IsSimpleSelectable() const
{
  return GetOptions() & OPT_SIMPLE_SELECTABLE;
}

bool AbstractItem::IsNodeSelectable() const
{
  return GetOptions() & OPT_NODE_SELECTABLE;
}

bool AbstractItem::IsSelectable() const
{
  return GetOptions() & (OPT_SIMPLE_SELECTABLE | OPT_NODE_SELECTABLE);
}

bool AbstractItem::IsMovable() const
{
  return GetOptions() & OPT_MOVABLE;
}

bool AbstractItem::IsDeletable() const
{
  return GetOptions() & OPT_DELETABLE;
}

bool AbstractItem::IsSelected() const
{
  return GetCurrentState().IsSelectedState();
}

Properties AbstractItem::GetSupportedProperties() const
{
  return PROP_POSITION/* | PROP_ZVALUE*/;
}

QVariant AbstractItem::GetPropertyValue(Property p) const
{
  switch (p)
  {
  case PROP_POSITION:
  return pos();
  /*case PROP_ZVALUE:
                        return zValue();*/
  case PROP_TEXT:
  return QString();
  default:
    throw std::logic_error("unsupported property");
  }
}

void AbstractItem::SetPropertyValue(Property p, const QVariant& value)
{
  SetPropertyValueImpl(p, value);
  update();
  emit SigItemChanged();
  emit SigPropertyValueChanged(p, value);
}

void AbstractItem::SetPropertyValueImpl(Property p, const QVariant& value)
{
  switch (p)
  {
  case PROP_POSITION:
    setPos(value.value<QPointF>());
  break;
/*case PROP_ZVALUE:
    setZValue(value.toReal());
  break;
*/
  default:
    throw std::logic_error("unsupported property");
  }
}

void AbstractItem::RegisterToSigPropertyValueChanged(QObject* receiver, const char* slot)
{
  QCONNECT(this, SIGNAL(SigPropertyValueChanged(graphics::Property, QVariant)), receiver, slot);
}

QRectF AbstractItem::GetSelectedRect() const
{
  return boundingRect();
}

void AbstractItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
  UNUSED_VAR(option);
  UNUSED_VAR(widget);

  GetCurrentState().Paint(*painter, *this);
}

void AbstractItem::GotoNextState()
{
  GotoState((_currentStateIdx + 1) % GetStateList().size());
}

void AbstractItem::GotoFirstSelectedState()
{
  const StateList& states = GetStateList();
  for (StateList::size_type i = 0; i < states.size(); ++i)
  {
    if (states[i]->IsSelectedState())
    {
      GotoState(i);
      return;
    }
  }
  //TODO throw exception here
}

void AbstractItem::GotoState(StateList::size_type next)
{
  GetCurrentState().ExitState(*this);
  _currentStateIdx = next;
  GetCurrentState().EnterState(*this);
  update(); //needed, becasue we need to render the frame around the bounding rect (in case the item is now selected)
  emit SigStateChanged();
}

void AbstractItem::GotoFirstNotSelectedState()
{
  const StateList& states = GetStateList();
  for (StateList::size_type i = 0; i < states.size(); ++i)
  {
    if (!states[i]->IsSelectedState())
    {
      GotoState(i);
      return;
    }
  }
  //TODO throw exception here
}

QVariant AbstractItem::itemChange(GraphicsItemChange change, const QVariant& value)
{
  const QVariant ret = QGraphicsObject::itemChange(change, value);
  if (change == ItemVisibleHasChanged && !value.toBool())
  {
    GotoFirstNotSelectedState();
  }
  emit SigItemChanged();
  return ret;
}

const itemstate::AbstractItemState& AbstractItem::GetCurrentState() const
{
  const StateList& states = GetStateList();
  ASSERT(_currentStateIdx < states.size());
  return *states[_currentStateIdx];
}

itemstate::AbstractItemState& AbstractItem::GetCurrentState()
{
  const StateList& states = GetStateList();
  ASSERT(_currentStateIdx < states.size());
  return *states[_currentStateIdx];
}

#ifdef graphics_USE_MEASURE
const ms::MultiPointRefPointList* AbstractItem::GetMultiPointRefPointList() const
{
  return NULL;
}
#endif

}
}

// we do not add RegisterAllItems.cpp to the CMake-Script.
// reason: if we do so, the linker will remove all registrar's because those symbols are never used
#include "itemregistry.cpp"

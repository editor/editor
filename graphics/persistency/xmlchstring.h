/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _XMLCHSTRING_H_
#define _XMLCHSTRING_H_

#include <string>
#include <xercesc/util/XMLString.hpp> /* to get definition for XMLCh */

namespace persistency
{
typedef std::basic_string<XMLCh> XMLChString;
}
#endif

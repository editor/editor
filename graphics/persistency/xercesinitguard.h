/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _XERCESINITGUARD_H_
#define _XERCESINITGUARD_H_

#include <boost/thread/locks.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/noncopyable.hpp>
#include "utils/log.h"
#include "graphics/persistency/xercespanichandler.h"

namespace persistency
{

/** Helper class, which can be instantiated on the call stack to ensure, that the Xerces-framework is initiated correctly before using it. */
class XercesInitGuard :
    private boost::noncopyable
{
  DECLARE_CLASS_LOGGER

  public:
    XercesInitGuard();
  ~XercesInitGuard();

public:
  /** When neverRelease is enabled, Xerces will never be deinitialized.*/
  static void SetNeverRelease(bool nr);

private:
  /** calls XMLPlatformUtils::Initialize() */
  static void Init();

  /** calls XMLPlatformUtils::Terminate() */
  static void Release();

private:
  typedef boost::mutex Mutex;
  typedef boost::lock_guard<Mutex> MutexGuard;
  static boost::mutex m_mutex;
  static bool m_initialized;
  static unsigned m_instanceCounter;
  static bool m_neverRelease;
  static XercesPanicHandler m_panicHandler;
};

}

#endif

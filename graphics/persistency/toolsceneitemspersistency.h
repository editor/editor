/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _TOOLSCENEITEMSPERSISTENCY_H_
#define _TOOLSCENEITEMSPERSISTENCY_H_

#include "utils/log.h"
#include "graphics/toolscene.h"
#include "graphics/persistency/abstractcontentreader.h"
#include "graphics/persistency/abstractelementreader.h"
#include "graphics/persistency/elementprinter.h"

namespace persistency
{

class ToolSceneItemsPersistency :
  public AbstractElementReader
{
  DECLARE_CLASS_LOGGER

public:
  ToolSceneItemsPersistency(graphics::ToolScene& scene);

public:
  static void Print(const graphics::ToolScene& scene, ElementPrinter& printer);

protected:
  virtual AbstractElementReaderSP CreateElementReader(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const XNQ Attributes& attrs);

private:
  graphics::ToolScene& m_scene;
};
}
#endif

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _CONTAINERADAPTERFACTORY_H_
#define _CONTAINERADAPTERFACTORY_H_

#include <map>
#include <set>
#include "graphics/persistency/pairassociativecontaineradapter.h"
#include "graphics/persistency/simpleassociativecontaineradapter.h"
#include "graphics/persistency/standardcontaineradapter.h"

namespace persistency
{

template<class ITEM_PERSISTENCY, class CONTAINER>
struct ContainerAdapterFactory
{
  typedef StandardContainerAdapter<ITEM_PERSISTENCY, CONTAINER> Adapter;
};

template<class ITEM_PERSISTENCY, class SET_VALUE, class SET_COMPARE, class SET_ALLOC>
struct ContainerAdapterFactory
    <
    ITEM_PERSISTENCY,
    std::set<SET_VALUE, SET_COMPARE, SET_ALLOC>
    >
{
  typedef SimpleAssociativeContainerAdapter<ITEM_PERSISTENCY, std::set<SET_VALUE, SET_COMPARE, SET_ALLOC> > Adapter;
};

template<class ITEM_PERSISTENCY, class MAP_KEY, class MAP_DATA, class MAP_COMPARE, class MAP_ALLOC>
struct ContainerAdapterFactory
    <
    ITEM_PERSISTENCY,
    std::map<MAP_KEY, MAP_DATA, MAP_COMPARE, MAP_ALLOC>
    >
{
  typedef PairAssociativeContainerAdapter<ITEM_PERSISTENCY, std::map<MAP_KEY, MAP_DATA, MAP_COMPARE, MAP_ALLOC> > Adapter;
};

}

#endif


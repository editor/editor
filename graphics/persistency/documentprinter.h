/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _DOCUMENTPRINTER_H_
#define _DOCUMENTPRINTER_H_

#include <string>
#include <boost/filesystem.hpp>
#include <boost/utility.hpp>
#include <xercesc/framework/XMLFormatter.hpp>
#include <xercesc/framework/LocalFileFormatTarget.hpp>
#include <xercesc/util/XMLString.hpp>
#include "graphics/persistency/xmlprinter.h"
#include "graphics/persistency/documentprinter.h"
#include "graphics/persistency/elementprinter.h"
#include "graphics/persistency/xercesinitguard.h"

namespace persistency
{
  class DocumentPrinter :
      private boost::noncopyable
  {
  public:
    enum Encoding
    {
      EncodingUTF8,   ///< writes an UTF-8 byte order mark and <?xml ... encoding="UTF-8" ... ?>
      EncodingUTF16LE ///< writes an UTF-16LE byte order mark and <?xml ... encoding="UTF-16LE" ... ?>
    };

    DocumentPrinter(const boost::filesystem::path& filePath,
                    Encoding encoding = EncodingUTF8,
                    bool standalone = true,
                    bool prettyPrint = false);

  public:

    ElementPrinter& CreateRootElement(const STRING& elementTag);
    ElementPrinter& RootElement();

  private:
    static const XMLCh* GetEncodingString(Encoding encoding);

  private:
    const XercesInitGuard m_initGuard; ///< 1. the init guard initializes the xerces platform utilities
    XERCES_CPP_NAMESPACE_QUALIFIER LocalFileFormatTarget m_formatTarget; ///< 2. the output file target
    XERCES_CPP_NAMESPACE_QUALIFIER XMLFormatter m_formatter; ///< 3. the formatter
    XMLPrinter m_printer;  ///< 4. the XML Printer
    ElementPrinterAP m_rootElement; ///< needs this position because it should be deleted first
  };

}
#endif

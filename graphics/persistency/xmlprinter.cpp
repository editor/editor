/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <algorithm>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/XMLUniDefs.hpp>
#include "utils/assert.h"
#include "graphics/persistency/xmlprinter.h"

namespace persistency
{

namespace XCS = XERCES_CPP_NAMESPACE;

const XMLCh XMLPrinter::Left[1] =        { XCS::chOpenAngle };
const XMLCh XMLPrinter::EqualsQuote[2] = { XCS::chEqual, XCS::chDoubleQuote };
const XMLCh XMLPrinter::Quote[1] =       { XCS::chDoubleQuote };
const XMLCh XMLPrinter::SlashRight[2] =  { XCS::chForwardSlash, XCS::chCloseAngle };
const XMLCh XMLPrinter::LeftSlash[2] =   { XCS::chOpenAngle, XCS::chForwardSlash };
const XMLCh XMLPrinter::Right[1] =       { XCS::chCloseAngle };
const XMLCh XMLPrinter::Space[1] =       { XCS::chSpace };
const XMLCh XMLPrinter::NewLine[1] =     { XCS::chLF };
const XMLChString XMLPrinter::m_indentStr(MAX_NESTING_LEVEL * SPACES_PER_INDENT, XCS::chSpace);

XMLPrinter::XMLPrinter(XCS::XMLFormatter& formatter, bool prettyPrint) :
  m_startTagClosed(true),
  m_formatter(formatter),
  m_prettyPrint(prettyPrint),
  m_nestingLevel(0),
  m_charactersWritten()
{
}


XMLPrinter::~XMLPrinter()
{
  ASSERT(m_nestingLevel == 0);
}


void XMLPrinter::Attribute(const XMLCh* attributeName, const XMLCh* attributeValue)
{
  ASSERT(m_startTagClosed == false);
  m_formatter.formatBuf(Space, 1, XCS::XMLFormatter::NoEscapes);
  m_formatter.formatBuf(attributeName, XCS::XMLString::stringLen(attributeName), XCS::XMLFormatter::NoEscapes);
  m_formatter.formatBuf(EqualsQuote, 2, XCS::XMLFormatter::NoEscapes);
  m_formatter.formatBuf(attributeValue, XCS::XMLString::stringLen(attributeValue), XCS::XMLFormatter::AttrEscapes);
  m_formatter.formatBuf(Quote, 1, XCS::XMLFormatter::NoEscapes);
}


void XMLPrinter::Characters(const XMLCh* st)
{
  if (XCS::XMLString::stringLen(st) > 0)
  {
    m_charactersWritten.set(ClipNestingLevel(m_nestingLevel));

    if (m_startTagClosed == false)
    {
      m_startTagClosed = true;
      m_formatter.formatBuf(Right, 1, XCS::XMLFormatter::NoEscapes);
    }
    m_formatter.formatBuf(st, XCS::XMLString::stringLen(st), XCS::XMLFormatter::CharEscapes);
  }
}


void XMLPrinter::StartElement(const XMLCh* tagName)
{
  if (m_startTagClosed == false)
  {
    m_formatter.formatBuf(Right, 1, XCS::XMLFormatter::NoEscapes);
  }

  if (m_prettyPrint)
  {
    Indent();
  }

  m_formatter.formatBuf(Left, 1, XCS::XMLFormatter::NoEscapes);
  m_startTagClosed = false;
  m_formatter.formatBuf(tagName, XCS::XMLString::stringLen(tagName), XCS::XMLFormatter::StdEscapes);

  ++m_nestingLevel;

  m_charactersWritten.reset(ClipNestingLevel(m_nestingLevel));

}


void XMLPrinter::EndElement(const XMLCh* tagName)
{
  ASSERT(m_nestingLevel > 0);
  --m_nestingLevel;

  if (m_startTagClosed == false)
  {
    m_formatter.formatBuf(SlashRight, 2, XCS::XMLFormatter::NoEscapes);
    m_startTagClosed = true;
  }
  else
  {
    if (   m_prettyPrint // stop if we do not prettyprint
           && !m_charactersWritten.test(ClipNestingLevel(m_nestingLevel+1U)))
    {
      Indent();
    }
    m_formatter.formatBuf(LeftSlash, 2, XCS::XMLFormatter::NoEscapes);
    m_formatter.formatBuf(tagName, XCS::XMLString::stringLen(tagName), XCS::XMLFormatter::StdEscapes);
    m_formatter.formatBuf(Right, 1, XCS::XMLFormatter::NoEscapes);
  }
}


size_t XMLPrinter::ClipNestingLevel(const size_t level) const
{
  return std::min<size_t>(level, MAX_NESTING_LEVEL - 1);
}


void XMLPrinter::Indent()
{
  m_formatter.formatBuf(NewLine, 1, XCS::XMLFormatter::NoEscapes);

  ASSERT((ClipNestingLevel(m_nestingLevel) * SPACES_PER_INDENT) <= m_indentStr.size());
  m_formatter.formatBuf(m_indentStr.data(), ClipNestingLevel(m_nestingLevel) * SPACES_PER_INDENT, XCS::XMLFormatter::NoEscapes);
}

}


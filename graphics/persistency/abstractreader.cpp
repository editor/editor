/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "graphics/persistency/abstractreader.h"
#include "graphics/persistency/abstractdocumentreader.h"
#include "graphics/persistency/abstractelementreader.h"
#include "utils/assert.h"

namespace persistency
{

AbstractReader::AbstractReader() :
  m_stackHandler(nullptr)
{}

AbstractReader::~AbstractReader()
{}

void AbstractReader::SetStackHandler(StackHandler* sh)
{
  m_stackHandler = sh;
}

StackHandler* AbstractReader::GetStackHandler()
{
  return m_stackHandler;
}

void AbstractReader::PushDocumentReader(AbstractDocumentReaderSP reader)
{
  Push(reader);
  reader->startedDocument();
}

void AbstractReader::PushElementReader(AbstractElementReaderSP reader, const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const XNQ Attributes& attrs)
{
  Push(reader);
  reader->startedElement(uri, localname, qname, attrs);
}

void AbstractReader::Push(AbstractReaderSP reader)
{
  reader->SetStackHandler(m_stackHandler);
  ASSERT(m_stackHandler);
  m_stackHandler->Push(reader);
}

void AbstractReader::Pop()
{
  ASSERT(m_stackHandler);
  m_stackHandler->Pop();
  m_stackHandler = nullptr;
}

}

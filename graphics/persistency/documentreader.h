/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _DOCUMENTREADER_H_
#define _DOCUMENTREADER_H_


#include "graphics/persistency/abstractdocumentreader.h"
#include "graphics/persistency/xercestranscodedstring.h"
#include "graphics/persistency/parseexception.h"
#include "utils/types.h"

namespace persistency
{
template
<
    class ELEMREADER,
    typename T
    >
class DocumentReader :
    public AbstractDocumentReader
{
public:
  DocumentReader(const STRING& elemName, T& obj) :
    m_elemName(elemName),
    m_obj(obj)
  {}

public:
  virtual void startElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const XNQ Attributes& attrs)
  {
    const LocalString lname(localname);

    if (!lname.equals(m_elemName))
    {
      throw ParseException("invalid root tag");
    }

    AbstractElementReaderSP er(new ELEMREADER(m_obj));
    PushElementReader(er, uri, localname, qname, attrs);
  }

private:
  const STRING m_elemName;
  T& m_obj;
};

}
#endif

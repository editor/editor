/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "utils/assert.h"
#include "graphics/persistency/defaultprintitemvisitor.h"
#include "graphics/persistency/elements/areaitempersistency.h"
#include "graphics/persistency/elements/ellipseitempersistency.h"
#include "graphics/persistency/elements/penitempersistency.h"
#include "graphics/persistency/elements/polylineitempersistency.h"
#include "graphics/persistency/elements/textitempersistency.h"

namespace persistency
{

DefaultPrintItemVisitor::DefaultPrintItemVisitor()
{}

#define IMPL_VISITOR(CLASS, PERSISTENCY)                        \
  void DefaultPrintItemVisitor::Visit(CLASS& item)              \
{                                                               \
  ElementPrinter* printer = GetPrinter();                       \
  ASSERT(printer);                                              \
  ElementPrinter childPrinter(*printer, PERSISTENCY::TAGNAME);  \
  PERSISTENCY::Print(item, childPrinter);                       \
}

IMPL_VISITOR(graphics::items::AreaItem, AreaItemPersistency)
IMPL_VISITOR(graphics::items::EllipseItem, EllipseItemPersistency)
IMPL_VISITOR(graphics::items::PenItem, PenItemPersistency)
IMPL_VISITOR(graphics::items::PolylineItem, PolylineItemPersistency)
IMPL_VISITOR(graphics::items::TextItem, TextItemPersistency)

}


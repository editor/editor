/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "graphics/persistency/abstractcontentreader.h"
#include "graphics/persistency/xercestranscodedstring.h"

namespace persistency
{

void AbstractContentReader::characters(const XMLCh* const chars, const XMLSize_t length)
{
  m_chars.append(chars, length);
}

void AbstractContentReader::endedElement(const XMLCh* const /*uri*/, const XMLCh* const /*localname*/, const XMLCh* const /*qname*/)
{
  ParseContent(m_chars);
}

}

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _ABSTRACTPPRINTVISITOR_H_
#define _ABSTRACTPPRINTVISITOR_H_

#include <memory>
#include "utils/basevisitor.h"
#include "utils/assert.h"
#include "graphics/persistency/elementprinter.h"

namespace persistency
{

class AbstractPrintVisitor :
    public BaseVisitor
{
protected:
  AbstractPrintVisitor() :
    m_printer(nullptr)
  {}

public:
  void SetPrinter(ElementPrinter* printer)
  {
    m_printer = printer;
  }

protected:
  ElementPrinter* GetPrinter()
  {
    return m_printer;
  }

private:
  ElementPrinter* m_printer;
};

typedef std::unique_ptr<AbstractPrintVisitor> PrintVisitorAP;

}

#endif


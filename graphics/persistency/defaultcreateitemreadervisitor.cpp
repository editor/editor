/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "graphics/persistency/defaultcreateitemreadervisitor.h"
#include "graphics/persistency/elements/areaitempersistency.h"
#include "graphics/persistency/elements/ellipseitempersistency.h"
#include "graphics/persistency/elements/penitempersistency.h"
#include "graphics/persistency/elements/polylineitempersistency.h"
#include "graphics/persistency/elements/textitempersistency.h"

namespace persistency
{

DefaultCreateItemReaderVisitor::DefaultCreateItemReaderVisitor()
{}

#define IMPL_VISITOR(CLASS, S11N)                         \
void DefaultCreateItemReaderVisitor::Visit(CLASS& item)   \
{                                                         \
  SetReader(AbstractElementReaderSP(new S11N(item)));     \
}

IMPL_VISITOR(graphics::items::AreaItem, AreaItemPersistency)
IMPL_VISITOR(graphics::items::EllipseItem, EllipseItemPersistency)
IMPL_VISITOR(graphics::items::PenItem, PenItemPersistency)
IMPL_VISITOR(graphics::items::PolylineItem, PolylineItemPersistency)
IMPL_VISITOR(graphics::items::TextItem, TextItemPersistency)
}


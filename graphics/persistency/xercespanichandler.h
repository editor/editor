/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _XERCESPANICHANDLER_H_
#define _XERCESPANICHANDLER_H_

#include <xercesc/util/PanicHandler.hpp>

namespace persistency
{

class XercesPanicHandler :
    public XERCES_CPP_NAMESPACE_QUALIFIER PanicHandler
{
public:
  virtual void panic(const PanicReasons reason);
};

}
#endif


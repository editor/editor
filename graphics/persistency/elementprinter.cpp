/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <boost/filesystem.hpp>
#include <boost/utility.hpp>

#include "utils/types.h"

#include "graphics/persistency/xercestranscodedstring.h"
#include "graphics/persistency/xmlprinter.h"
#include "graphics/persistency/elementprinter.h"

namespace persistency
{

ElementPrinter::~ElementPrinter()
{
  m_xmlPrinter.EndElement(m_elementTag);
}

ElementPrinter::ElementPrinter(ElementPrinter& elementPrinter, const STRING& elementTag) :
  m_xmlPrinter(elementPrinter.m_xmlPrinter),
  m_elementTag(elementTag)
{
  m_xmlPrinter.StartElement(m_elementTag);
}

ElementPrinter::ElementPrinter(persistency::XMLPrinter& rootPrinter, const STRING& elementTag) :
  m_xmlPrinter(rootPrinter),
  m_elementTag(elementTag)
{
  m_xmlPrinter.StartElement(m_elementTag);
}

void ElementPrinter::PrintText(const STRING& text)
{
  m_xmlPrinter.Characters(XercesString(text));
}

void ElementPrinter::PrintText(const XMLCh* text)
{
  m_xmlPrinter.Characters(text);
}

void ElementPrinter::PrintAttribute(const STRING& attributeName, const STRING& attributeValue)
{
  m_xmlPrinter.Attribute(XercesString(attributeName), XercesString(attributeValue));
}

}

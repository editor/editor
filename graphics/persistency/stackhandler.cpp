/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "graphics/persistency/stackhandler.h"

namespace persistency
{

DEFINE_CLASS_LOGGER(StackHandler);

StackHandler::StackHandler()
{}

StackHandler::~StackHandler()
{}

void StackHandler::Push(DefaultHandlerSP handler)
{
  //LOG_DEBUG("[%d] %spush %s",_stack.size() + 1, IndentStr(_stack.size() + 1).c_str(), typeid(*(handler.get())).name());
  m_stack.push(handler);
}

void StackHandler::Pop()
{
  //LOG_DEBUG("[%d] %spop  %s",_stack.size(), IndentStr(_stack.size()).c_str(), typeid(*(TopHandler().get())).name());
  m_stack.pop();
}

void StackHandler::RegisterToParser(XNQ SAX2XMLReader& parser)
{
  parser.setContentHandler(this);
  parser.setDTDHandler(this);
  parser.setEntityResolver(this);
  parser.setErrorHandler(this);
  parser.setDeclarationHandler(this);
  parser.setLexicalHandler(this);
}

#define XERCES_WRAPPER_FUNC0(RET, FUNCTION)   \
  RET StackHandler::FUNCTION()                \
{                                             \
  return TopHandler()->FUNCTION();            \
}

#define XERCES_WRAPPER_FUNC1(RET, FUNCTION, ARG1TYPE, ARG1NAME)   \
  RET StackHandler::FUNCTION(ARG1TYPE ARG1NAME)                   \
{                                                                 \
  return TopHandler()->FUNCTION(ARG1NAME);                        \
}

#define XERCES_WRAPPER_FUNC2(RET, FUNCTION, ARG1TYPE, ARG1NAME, ARG2TYPE, ARG2NAME) \
  RET StackHandler::FUNCTION(ARG1TYPE ARG1NAME, ARG2TYPE ARG2NAME)                  \
{                                                                                   \
  return TopHandler()->FUNCTION(ARG1NAME, ARG2NAME);                                \
}

#define XERCES_WRAPPER_FUNC3(RET, FUNCTION, ARG1TYPE, ARG1NAME, ARG2TYPE, ARG2NAME, ARG3TYPE, ARG3NAME) \
  RET StackHandler::FUNCTION(ARG1TYPE ARG1NAME, ARG2TYPE ARG2NAME, ARG3TYPE ARG3NAME)                   \
{                                                                                                       \
  return TopHandler()->FUNCTION(ARG1NAME, ARG2NAME, ARG3NAME);                                          \
}

#define XERCES_WRAPPER_FUNC4(RET, FUNCTION, ARG1TYPE, ARG1NAME, ARG2TYPE, ARG2NAME, ARG3TYPE, ARG3NAME, ARG4TYPE, ARG4NAME) \
  RET StackHandler::FUNCTION(ARG1TYPE ARG1NAME, ARG2TYPE ARG2NAME, ARG3TYPE ARG3NAME, ARG4TYPE ARG4NAME)                    \
{                                                                                                                           \
  return TopHandler()->FUNCTION(ARG1NAME, ARG2NAME, ARG3NAME, ARG4NAME);                                                    \
}

#define XERCES_WRAPPER_FUNC5(RET, FUNCTION, ARG1TYPE, ARG1NAME, ARG2TYPE, ARG2NAME, ARG3TYPE, ARG3NAME, ARG4TYPE, ARG4NAME, ARG5TYPE, ARG5NAME) \
  RET StackHandler::FUNCTION(ARG1TYPE ARG1NAME, ARG2TYPE ARG2NAME, ARG3TYPE ARG3NAME, ARG4TYPE ARG4NAME, ARG5TYPE ARG5NAME)                     \
{                                                                                                                                               \
  return TopHandler()->FUNCTION(ARG1NAME, ARG2NAME, ARG3NAME, ARG4NAME, ARG5NAME);                                                              \
}

XERCES_WRAPPER_FUNC2(void, characters, const XMLCh* const, chars, const XMLSize_t, length)
XERCES_WRAPPER_FUNC0(void, endDocument)
XERCES_WRAPPER_FUNC3(void, endElement, const XMLCh* const, uri, const XMLCh* const, localname, const XMLCh* const, qname)
XERCES_WRAPPER_FUNC2(void, ignorableWhitespace, const XMLCh* const, chars, const XMLSize_t, length)
XERCES_WRAPPER_FUNC2(void, processingInstruction, const XMLCh* const, target, const XMLCh* const, data)
XERCES_WRAPPER_FUNC0(void, resetDocument)
XERCES_WRAPPER_FUNC1(void, setDocumentLocator, const XNQ Locator* const, locator)
XERCES_WRAPPER_FUNC0(void, startDocument)
XERCES_WRAPPER_FUNC4(void, startElement, const XMLCh* const, uri, const XMLCh* const, localname, const XMLCh* const, qname, const XNQ Attributes&, attrs)
XERCES_WRAPPER_FUNC2(void, startPrefixMapping, const XMLCh* const, prefix, const XMLCh* const, uri)
XERCES_WRAPPER_FUNC1(void, endPrefixMapping, const XMLCh* const, prefix)
XERCES_WRAPPER_FUNC1(void, skippedEntity, const XMLCh* const, name)
XERCES_WRAPPER_FUNC2(XNQ InputSource*, resolveEntity, const XMLCh* const, publicId, const XMLCh* const, systemId)
XERCES_WRAPPER_FUNC1(void, error, const XNQ SAXParseException&, exc)
XERCES_WRAPPER_FUNC1(void, fatalError, const XNQ SAXParseException&, exc)
XERCES_WRAPPER_FUNC1(void, warning, const XNQ SAXParseException&, exc)
XERCES_WRAPPER_FUNC0(void, resetErrors)
XERCES_WRAPPER_FUNC3(void, notationDecl, const XMLCh* const, name, const XMLCh* const, publicId, const XMLCh* const, systemId)
XERCES_WRAPPER_FUNC0(void, resetDocType)
XERCES_WRAPPER_FUNC4(void, unparsedEntityDecl, const XMLCh* const, name, const XMLCh* const, publicId, const XMLCh* const, systemId, const XMLCh* const, notationName)
XERCES_WRAPPER_FUNC2(void, comment, const XMLCh* const, chars, const XMLSize_t, length)
XERCES_WRAPPER_FUNC0(void, endCDATA)
XERCES_WRAPPER_FUNC0(void, endDTD)
XERCES_WRAPPER_FUNC1(void, endEntity, const XMLCh* const, name)
XERCES_WRAPPER_FUNC0(void, startCDATA)
XERCES_WRAPPER_FUNC3(void, startDTD, const XMLCh* const, name, const XMLCh* const, publicId, const XMLCh* const, systemId)
XERCES_WRAPPER_FUNC1(void, startEntity, const XMLCh* const, name)
XERCES_WRAPPER_FUNC2(void, elementDecl, const XMLCh* const, name, const XMLCh* const, model)
XERCES_WRAPPER_FUNC5(void, attributeDecl, const XMLCh* const, eName, const XMLCh* const, aName, const XMLCh* const, type, const XMLCh* const, mode, const XMLCh* const, value)
XERCES_WRAPPER_FUNC2(void, internalEntityDecl, const XMLCh* const, name, const XMLCh* const, value)
XERCES_WRAPPER_FUNC3(void, externalEntityDecl, const XMLCh* const, name, const XMLCh* const, publicId, const XMLCh* const, systemId)

DefaultHandlerSP StackHandler::TopHandler() const
{
  ASSERT(!m_stack.empty());
  return m_stack.top();
}

#ifdef SUPPORT_LOGGING_DEBUG
STRING StackHandler::IndentStr(UINT32t depth)
{
  STRING str;
  str.append(depth * 4, ' ');
  return str;
}
#endif

}

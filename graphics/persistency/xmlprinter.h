/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _XMLPRINTER_H_
#define _XMLPRINTER_H_

#include <bitset>
#include <boost/utility.hpp>
#include <xercesc/framework/XMLFormatter.hpp>
#include "graphics/persistency/xercestranscodedstring.h"
#include "graphics/persistency/xmlchstring.h"

namespace persistency
{

/**
 * The XML Printer effectively outputs the XML to the formatter
 */
class XMLPrinter :
    private boost::noncopyable
{
public:

  /**
   * Constructor
   * @param formatter the xercesc write backend
   * @param prettyPrint prints some whitespace for better readability (by human beings) default is false
   */
  XMLPrinter(XERCES_CPP_NAMESPACE_QUALIFIER XMLFormatter& formatter, bool prettyPrint=false);

  /**
   * Destructor
   * Checks if the nesting level has reached 0 (all elements closed)
   * @note DO NOT DERIVE FROM THIS CLASS, NO VIRTUAL DESTRUCTOR
   */
  ~XMLPrinter();


  /**
   * Outputs an attribute, replaces reserved characters in attribute value with escape sequences
   * @param attributeName the attribute name - written as given, take care not to use reserved characters in names
   * @param attributeValue the attribute value
   */
  void Attribute(const XMLCh* attributeName, const XMLCh* attributeValue);


  /**
   * Outputs text, replaces reserved characters with escape sequences
   * @param st the text to write
   */
  void Characters(const XMLCh* st);

  /**
   * Outputs a start element string with the tag name tagName
   * @param tagName the tag name of the element
   */
  void StartElement(const XMLCh* tagName);

  /**
   * Outputs an end element string for the tag name tagName
   * @param tagName the element tag name
   */
  void EndElement(const XMLCh* tagName);


private:
  /**
   * Outputs whitespace, a newline followed by _nestingLevel space characters
   */
  void Indent();

  /**
   * returns the clipped nesting level to use as index in _charactersWritten bitset
   * @param level the nesting level to clip
   * @return cliped nesting level, clippedLevel < _charactersWritten.size()
   */
  size_t ClipNestingLevel(size_t level) const;

private:
  /**
   * Importatnt characters and string fragments in XML
   */
  static const XMLCh Left[1];        ///< the character: <
  static const XMLCh EqualsQuote[2]; ///< the string: ="
  static const XMLCh Quote[1];       ///< the character: "
  static const XMLCh SlashRight[2];  ///< the string: />
  static const XMLCh LeftSlash[2];   ///< the string: </
  static const XMLCh Right[1];       ///< the character: >
  static const XMLCh Space[1];       ///< the character: space
  static const XMLCh NewLine[1];     ///< the character: newline

  bool m_startTagClosed; ///< state for writing the start tag closing > or />
  XERCES_CPP_NAMESPACE_QUALIFIER XMLFormatter& m_formatter; ///< the xercesc write backend

  enum {MAX_NESTING_LEVEL = 32, SPACES_PER_INDENT = 4};
  const bool m_prettyPrint;            ///< flag to enable / disable pretty printing
  size_t m_nestingLevel;               ///< the nesting level counter, incremented with StartElement, decrement with EndElement
  std::bitset<MAX_NESTING_LEVEL> m_charactersWritten; ///< flags used for pretty printing, we support only 32 nesting levels
  static const XMLChString m_indentStr;
};

}

#endif

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <xercesc/sax2/Attributes.hpp>

#include "graphics/persistency/abstractelementreader.h"
#include "graphics/persistency/xercestranscodedstring.h"
#include "graphics/persistency/parseexception.h"
#include "utils/assert.h"
#include "utils/macros.h"

namespace persistency
{

DEFINE_CLASS_LOGGER(AbstractElementReader);

void AbstractElementReader::startElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const XNQ Attributes& attrs)
{
	//create a new reader ...
	AbstractElementReaderSP reader(CreateElementReader(uri, localname, qname, attrs));
	if (!reader)
	{
		throw ParseException(STRING("can't read tag ") + LocalString(localname).Get());
	}
	//... and push it on the stack
	PushElementReader(reader, uri, localname, qname, attrs);
}

void AbstractElementReader::endElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname) //call in derived class
{
	//LOG_DEBUG("endElement '%s'", LocalString(localname).Get());
	//increase refcounter to this, because Pop() reduces refcounter, which may will call the dtor
  DefaultHandlerSP THIS = GetStackHandler()->TopHandler();
	ASSERT(THIS.get() == this);

	Pop();

	//this may throws an exception
	endedElement(uri, localname, qname);
}

STRING AbstractElementReader::GetAttributeValue(const XNQ Attributes& attrs, const char* const attrName)
{
	STRING value;
	if (!GetOptionalAttributeValue(attrs, attrName, value))
	{
		throw ParseException(STRING("attribute '") + attrName + "' not found");
	}
	return value;
}

bool AbstractElementReader::GetOptionalAttributeValue(const XNQ Attributes& attrs, const char* const attrName, STRING& value)
{
	value.clear();

	const XercesString xaName(attrName);
	for (XMLSize_t i = 0; i < attrs.getLength(); ++i)
	{
		if (xaName.equals(attrs.getLocalName(i)))
		{
			value = LocalString(attrs.getValue(i)).Get();
			return true;
		}
	}
	return false;
}

}

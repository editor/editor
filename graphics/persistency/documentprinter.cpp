/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <string>
#include <stdexcept>

#include <boost/filesystem.hpp>
#include <boost/utility.hpp>

#include "utils/assert.h"
#include "utils/macros.h"
#include "utils/types.h"

#include <xercesc/util/XMLUniDefs.hpp>

#include "graphics/persistency/xercestranscodedstring.h"
#include "graphics/persistency/documentprinter.h"
#include "graphics/persistency/elementprinter.h"

namespace persistency
{
  namespace XCS = XERCES_CPP_NAMESPACE;

  const XMLCh* DocumentPrinter::GetEncodingString(Encoding encoding)
  {
    switch(encoding)
      {
      default:
        ASSERT(false);
        // no break, make UTF8 default

      case EncodingUTF8:
        {
          static const XMLCh UTF8[] = { XCS::chLatin_U, XCS::chLatin_T, XCS::chLatin_F,
                                        XCS::chDash,
                                        XCS::chDigit_8, 0};
          return &UTF8[0];
        }
        break;

      case EncodingUTF16LE:
        {
          static const XMLCh UTF16LE[] = { XCS::chLatin_U, XCS::chLatin_T, XCS::chLatin_F,
                                           XCS::chDash,
                                           XCS::chDigit_1, XCS::chDigit_6, XCS::chLatin_L, XCS::chLatin_E, 0};
          return &UTF16LE[0];
        }
        break;
      }
  }



  DocumentPrinter::DocumentPrinter(
      const boost::filesystem::path& filePath,
      const Encoding encoding,
      const bool standalone,
      const bool prettyPrint
      ) :
    m_initGuard(),                                               // initialize first, prepare xercesc
    m_formatTarget(PATH_2_CHAR(filePath)),                       // create second, the object is passed to the formatter
    m_formatter(GetEncodingString(encoding), 0, &m_formatTarget,  // create third, the printer needs a reference
               XCS::XMLFormatter::StdEscapes),
    m_printer(m_formatter, prettyPrint),                          // create fourth
    m_rootElement()                                              // create last as it must be deleted first
  {
    switch(encoding)
      {
      case EncodingUTF8:
        {
          static const XMLByte UTF8_BOM[3] = {0xEF, 0xBB, 0xBF};
          m_formatter.writeBOM(UTF8_BOM, sizeof(UTF8_BOM));
        }
        break;

      case EncodingUTF16LE:
        {
          static const XMLByte UTF16LE_BOM[2] = {0xFF, 0xFE};
          m_formatter.writeBOM(UTF16LE_BOM, sizeof(UTF16LE_BOM));
        }
        break;

      default:
        // by default, do not write a BOM
        ASSERT(false);
        break;
      }

    {
      const XercesString prolog("<?xml version=\"1.0\" encoding=\"");
      m_formatter.formatBuf(prolog, prolog.Length(),
                           XCS::XMLFormatter::NoEscapes);
    }

    {
      m_formatter.formatBuf(GetEncodingString(encoding),
                           XCS::XMLString::stringLen(GetEncodingString(encoding)),
                           XCS::XMLFormatter::NoEscapes);
    }

    {
      const XercesString prolog(standalone ? "\" standalone=\"yes\"?>" : "\" standalone=\"no\"?>");
      m_formatter.formatBuf(prolog, prolog.Length(),
                           XCS::XMLFormatter::NoEscapes);
    }

  }



  ElementPrinter& DocumentPrinter::CreateRootElement(const STRING& elementTag)
  {
    if (m_rootElement.get() != 0) // a well formed XML document has exactly one root element
      {
        throw std::logic_error("root element already created");
      }
    m_rootElement.reset(new ElementPrinter(m_printer, elementTag));
    return *m_rootElement;
  }



  ElementPrinter& DocumentPrinter::RootElement()
  {
    if (m_rootElement.get() == 0) // a well formed XML document has exactly one root element
      {
        throw std::logic_error("no root element");
      }
    return *m_rootElement;
  }




}


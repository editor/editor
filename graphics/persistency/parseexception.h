/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _PARSEEXCEPTION_H_
#define _PARSEEXCEPTION_H_

#include <stdexcept>
#include "utils/types.h"

namespace persistency
{

class ParseException :
    public std::runtime_error
{
public:
  ParseException(const STRING& msg) :
    std::runtime_error(msg)
  {}
};

}

#endif

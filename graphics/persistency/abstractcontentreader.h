/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _ABSTRACTOCONTENTREADER_H_
#define _ABSTRACTOCONTENTREADER_H_

#include "graphics/persistency/abstractelementreader.h"
#include "graphics/persistency/xmlchstring.h"

namespace persistency
{
class AbstractContentReader :
    public AbstractElementReader
{
public:
  virtual void characters(const XMLCh* const chars, const XMLSize_t length);
  virtual void endedElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname);

protected:
  virtual AbstractElementReaderSP CreateElementReader(const XMLCh* const, const XMLCh* const, const XMLCh* const, const XNQ Attributes&)
  {
    return AbstractElementReaderSP();
  }
  virtual void ParseContent(const XMLChString& content) = 0;
private:
  XMLChString m_chars;
};
}
#endif

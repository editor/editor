/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _TOOLSCENEPERSISTENCY_H_
#define _TOOLSCENEPERSISTENCY_H_

#include "graphics/toolscene.h"
#include "graphics/persistency/abstractelementreader.h"
#include "graphics/persistency/elementprinter.h"


namespace graphics
{
class GraphicsScene;
}

namespace persistency
{

class ToolScenePersistency :
    public AbstractElementReader
{
public:
  ToolScenePersistency(graphics::ToolScene& scene);

public:
  static void Print(const graphics::ToolScene& scene, ElementPrinter& printer);

protected:
  virtual AbstractElementReaderSP CreateElementReader(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const XNQ Attributes& attrs);

private:
  static const char* m_propertiesTagName;
  static const char* m_itemsTagName;
  graphics::ToolScene& m_scene;
};

}

#endif


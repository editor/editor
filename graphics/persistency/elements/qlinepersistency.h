/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _QLINEPERSISTENCY_H_
#define _QLINEPERSISTENCY_H_

#include <QLine>
#include <QLineF>
#include <QPoint>
#include <QPointF>
#include "graphics/persistency/elements/QPointS11n.h"
#include "graphics/persistency/AbstractElementReader.h"
#include "graphics/persistency/ElementPrinter.h"

namespace persistency
{

template <class LINE, class POINT, class POINTS11N>
class QLinePersistencyT :
    public AbstractElementReader
{
public:
  QLinePersistencyT(LINE& line) :
    m_line(line),
    m_readP1(0),
    m_readP2(0)
  {}

public:
  static void Print(const LINE& line, ElementPrinter& printer);

protected:
  virtual AbstractElementReaderSP CreateElementReader(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const XNQ Attributes& attrs);
  virtual void endedElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname);

private:
  LINE& m_line;
  POINT m_p1;
  POINT m_p2;
  unsigned m_readP1;
  unsigned m_readP2;
  static const char* m_p1TagName;
  static const char* m_p2TagName;
};

typedef QLinePersistencyT<QLine, QPoint, QPointPersistency> QLinePersistency;
typedef QLinePersistencyT<QLineF, QPointF, QPointFPersistency> QLineFPersistency;

template <class LINE, class POINT, class POINTPERSISTENCY>
AbstractElementReaderSP QLinePersistencyT<LINE, POINT, POINTS11N>::CreateElementReader(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const XNQ Attributes& attrs)
{
  UNUSED_VAR(uri);
  UNUSED_VAR(qname);
  UNUSED_VAR(attrs);

  const LocalString lname(localname);

  if (lname.equals("p1"))
  {
    ++m_readP1;
    return AbstractElementReaderSP(new POINTPERSISTENCY(m_p1));
  }
  else if (lname.equals("p2"))
  {
    ++m_readP2;
    return AbstractElementReaderSP(new POINTPERSISTENCY(m_p2));
  }
  return AbstractElementReaderSP();
}

template <class LINE, class POINT, class POINTPERSISTENCY>
void QLinePersistencyT<LINE, POINT, POINTPERSISTENCY>::endedElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname)
{
  UNUSED_VAR(uri);
  UNUSED_VAR(localname);
  UNUSED_VAR(qname);

  if (m_readP1 != 1 || m_readP2 != 1)
    throw ParseException("invalid line");

  m_line.setP1(m_p1);
  m_line.setP2(m_p2);
}

template <class LINE, class POINT, class POINTPERSISTENCY>
void QLinePersistencyT<LINE, POINT, POINTPERSISTENCY>::Print(const LINE& line, ElementPrinter& printer)
{
  {
    ElementPrinter p(printer, "p1");
    POINTPERSISTENCY::Print(line.p1(), p);
  }
  {
    ElementPrinter p(printer, "p2");
    POINTPERSISTENCY::Print(line.p2(), p);
  }
}

}

#endif


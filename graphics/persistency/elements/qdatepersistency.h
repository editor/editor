/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _QDATEPERSISTENCY_H_
#define _QDATEPERSISTENCY_H_

#include <QDate>
#include "graphics/persistency/abstractcontentreader.h"
#include "graphics/persistency/elementprinter.h"

namespace persistency
{
class QDatePersistency :
    public AbstractContentReader
{
public:
  QDatePersistency(QDate& dt);

public:
  static void Print(const QDate& dt, ElementPrinter& printer);

protected:
  virtual void startedElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const XNQ Attributes& attrs);
  virtual void ParseContent(const XMLChString& content);

private:
  QDate& m_dt;
  bool m_isValid;
  static const char* m_isValidAttrName;
  static const char* m_isValidAttrNameTrue;
  static const char* m_isValidAttrNameFalse;
};

}

#endif

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _QSTRINGPERSISTENCY_H_
#define _QSTRINGPERSISTENCY_H_

#include <QString>
#include "graphics/persistency/abstractcontentreader.h"
#include "graphics/persistency/elementprinter.h"

namespace persistency
{

class QStringPersistency :
    public AbstractContentReader
{
public:
  QStringPersistency(QString& str);

public:
  static void Print(const QString& str, ElementPrinter& printer);

protected:
  virtual void startedElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const XNQ Attributes& attrs);
  virtual void ParseContent(const XMLChString& content);

private:
  QString& m_str;
  bool m_isNull;
  static const char* m_isNullAttrName;
  static const char* m_isNullAttrNameTrue;
  static const char* m_isNullAttrNameFalse;
};

}

#endif


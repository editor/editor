/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#include "utils/assert.h"
#include "graphics/persistency/elements/propertiespersistency.h"
#include "graphics/persistency/elements/qbrushpersistency.h"
#include "graphics/persistency/elements/qfontpersistency.h"
#include "graphics/persistency/elements/qpenpersistency.h"
#include "graphics/persistency/elements/qpointpersistency.h"
#include "graphics/persistency/elements/qpolygonpersistency.h"
#include "graphics/persistency/elements/qstringpersistency.h"
#include "graphics/persistency/parseexception.h"
#include "graphics/persistency/elements/outlinepersistency.h"

namespace persistency
{

const char* PropertiesPersistency::m_brushTagName = "brush";
const char* PropertiesPersistency::m_fontTagName = "font";
const char* PropertiesPersistency::m_penTagName = "pen";
const char* PropertiesPersistency::m_outlinePenTagName = "outline";
const char* PropertiesPersistency::m_posnTagName = "position";
//const char* PropertiesS11n::_zTagName = "z";

PropertiesPersistency::PropertiesPersistency(graphics::IProperties& props) :
  m_props(props)
{}

AbstractElementReaderSP PropertiesPersistency::CreateElementReader(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const XNQ Attributes& attrs)
{
  UNUSED_VAR(uri);
  UNUSED_VAR(qname);
  UNUSED_VAR(attrs);

  const LocalString lname(localname);
  if (lname.equals(m_brushTagName))
  {
    MarkPropertyRead(graphics::PROP_BRUSH);
    return AbstractElementReaderSP(new QBrushPersistency(m_brush));
  }
  else if (lname.equals(m_fontTagName))
  {
    //MarkPropertyRead(::PROP_FONT);
    return AbstractElementReaderSP(new QFontPersistency(m_font));
  }
  else if (lname.equals(m_penTagName))
  {
    MarkPropertyRead(graphics::PROP_PEN);
    return AbstractElementReaderSP(new QPenPersistency(m_pen));
  }

  else if (lname.equals(m_outlinePenTagName))
  {
    MarkPropertyRead(graphics::PROP_OUTLINE);
    return AbstractElementReaderSP(new OutlinePersistency(m_outlinePen));
  }

  else if (lname.equals(m_posnTagName))
  {
    MarkPropertyRead(graphics::PROP_POSITION);
    return AbstractElementReaderSP(new QPointFPersistency(m_posn));
  }
  /*	else if (lname.equals(_zTagName))
  {
    MarkPropertyRead(graphics::PROP_ZVALUE);
    return AbstractElementReaderSP(new NumberS11n<qreal>(_zValue));
  }*/
  return AbstractElementReaderSP();
}

void PropertiesPersistency::endedElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname)
{
  UNUSED_VAR(uri);
  UNUSED_VAR(localname);
  UNUSED_VAR(qname);

  graphics::Properties unsupported = m_props.GetSupportedProperties();
  unsupported ^= unsupported;
  const graphics::Properties tooMuch = m_readProperties & unsupported;
  if (tooMuch != 0)
    throw ParseException("read some unsupported properties");

  if (m_readProperties.testFlag(graphics::PROP_BRUSH))
    m_props.SetPropertyValue(graphics::PROP_BRUSH, m_brush);

  if (m_readProperties.testFlag(graphics::PROP_FONT))
    m_props.SetPropertyValue(graphics::PROP_FONT, m_font);

  if (m_readProperties.testFlag(graphics::PROP_PEN))
    m_props.SetPropertyValue(graphics::PROP_PEN, m_pen);

  if (m_readProperties.testFlag(graphics::PROP_OUTLINE))
  {
    QVariant var;
    var.setValue(m_outlinePen);
    m_props.SetPropertyValue(graphics::PROP_OUTLINE, var);
  }
  if (m_readProperties.testFlag(graphics::PROP_POSITION))
    m_props.SetPropertyValue(graphics::PROP_POSITION, m_posn);
}

void PropertiesPersistency::Print(const graphics::IProperties& props, ElementPrinter& printer)
{
  const graphics::Properties supported = props.GetSupportedProperties();
  for (unsigned int i = 1; i < graphics::_MAX_PROPERTY; i = i << 1)
  {
    const graphics::Property prop =
        static_cast<graphics::Property>(i);

    if (!supported.testFlag(prop))
      continue;

    switch (prop)
    {
    case graphics::PROP_POSITION:
    {
      ElementPrinter p(printer, m_posnTagName);
      QPointFPersistency::Print(props.GetPropertyValue(prop).value<QPointF>(), p);
    }
    break;
    case graphics::PROP_PEN:
    {
      ElementPrinter p(printer, m_penTagName);
      QPenPersistency::Print(props.GetPropertyValue(prop).value<QPen>(), p);
    }
    break;
    case graphics::PROP_OUTLINE:
    {
      ElementPrinter p(printer, m_outlinePenTagName);
      OutlinePersistency::Print(props.GetPropertyValue(prop).value<graphics::Outline>(), p);
    }
    break;
    case graphics::PROP_BRUSH:
    {
      ElementPrinter p(printer, m_brushTagName);
      QBrushPersistency::Print(props.GetPropertyValue(prop).value<QBrush>(), p);
    }
    break;
    case graphics::PROP_FONT:
    {
      ElementPrinter p(printer, m_fontTagName);
      QFontPersistency::Print(props.GetPropertyValue(prop).value<QFont>(), p);
    }
    break;
    default:
      ASSERT(false);
    break;
    }
  }
}

void PropertiesPersistency::MarkPropertyRead(graphics::Property prop)
{
  if (m_readProperties.testFlag(prop))
    throw ParseException("property already read");
  m_readProperties |= prop;
}

}


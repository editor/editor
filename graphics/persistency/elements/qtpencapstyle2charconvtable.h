/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _QTPENCAPSTYLE2CHARCONVTABLE_H_
#define _QTPENCAPSTYLE2CHARCONVTABLE_H_

#include <Qt>
#include "graphics/persistency/enumstringconversiontable.h"

namespace persistency
{
DECLARE_ENUM_STRING_CONVERSION_TABLE(QtPenCapStyle2CharConvTable, Qt::PenCapStyle)
}

#endif


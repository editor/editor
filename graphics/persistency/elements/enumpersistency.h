/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _ENUMPERSISTENCY_H_
#define _ENUMPERSISTENCY_H_

#include "graphics/persistency/enumstringconversiontable.h"
#include "graphics/persistency/elementprinter.h"
#include "graphics/persistency/abstractcontentreader.h"
#include "graphics/persistency/xercestranscodedstring.h"
#include "graphics/persistency/parseexception.h"

namespace persistency
{

template<typename ENUM_STRING_CONVERSION_TABLE>
class EnumPersistency :
    public AbstractContentReader
{
public:
  EnumPersistency(typename ENUM_STRING_CONVERSION_TABLE::enum_type& enu) :
    m_enum(enu)
  {}

public:
  static void Print(const typename ENUM_STRING_CONVERSION_TABLE::enum_type enu, ElementPrinter& printer)
  {
    printer.PrintText(ENUM_STRING_CONVERSION_TABLE::AsString(enu));
  }


protected:
  virtual void ParseContent(const XMLChString& content)
  {
    const persistency::LocalString str(content);

    try
    {
      m_enum = ENUM_STRING_CONVERSION_TABLE::AsEnum(str);
    }
    catch (const BadStringToEnumConversionException& ex)
    {
      throw ParseException(ex.what());
    }
  }

private:
  typename ENUM_STRING_CONVERSION_TABLE::enum_type& m_enum;
};

}

#endif

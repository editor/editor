/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _OUTLINEPERSISTENCY_H_
#define _OUTLINEPERSISTENCY_H_

#include "graphics/persistency/abstractelementreader.h"
#include "graphics/outline.h"
#include "graphics/persistency/elementprinter.h"

namespace persistency
{

class OutlinePersistency:
    public AbstractElementReader
{
public:
  static const char* TAGNAME;

public:
  OutlinePersistency(graphics::Outline& item);

public:
  static void Print(const graphics::Outline& item, ElementPrinter& printer);
  virtual void endedElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname);

protected:
  virtual AbstractElementReaderSP CreateElementReader(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const XNQ Attributes& attrs);

private:
  graphics::Outline& m_item;
  unsigned m_readColor;
  unsigned m_readWidth;
  QColor m_color;
  qreal m_width;
  static const char* m_outlineTagName;
  static const char* m_colorTagName;
  static const char* m_widthTagName;
};

}

#endif

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _QPOINTPERSISTENCY_H_
#define _QPOINTPERSISTENCY_H_

#include "utils/macros.h"
#include <QPoint>
#include <QPointF>
#include "graphics/persistency/abstractelementreader.h"
#include "graphics/persistency/parseexception.h"
#include "graphics/persistency/elementprinter.h"
#include "graphics/persistency/elements/numberpersistency.h"

namespace persistency
{

template <class POINT, typename COORDINATE_TYPE>
class QPointPersistencyT :
    public AbstractElementReader
{
public:
  QPointPersistencyT(POINT& point) :
    m_point(point),
    m_readX(0),
    m_readY(0)
  {}

public:
  static void Print(const POINT& point, ElementPrinter& printer);

protected:
  virtual AbstractElementReaderSP CreateElementReader(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const XNQ Attributes& attrs);
  virtual void endedElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname);

private:
  POINT& m_point;
  COORDINATE_TYPE m_x;
  COORDINATE_TYPE m_y;
  unsigned m_readX;
  unsigned m_readY;
};

typedef QPointPersistencyT<QPoint, int> QPointPersistency;
typedef QPointPersistencyT<QPointF, qreal> QPointFPersistency;

template <class POINT, typename COORDINATE_TYPE>
AbstractElementReaderSP QPointPersistencyT<POINT, COORDINATE_TYPE>::CreateElementReader(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const XNQ Attributes& attrs)
{
  UNUSED_VAR(uri);
  UNUSED_VAR(qname);
  UNUSED_VAR(attrs);

  const LocalString lname(localname);

  if (lname.equals("x"))
  {
    ++m_readX;
    return AbstractElementReaderSP(new NumberPersistency<COORDINATE_TYPE>(m_x));
  }
  else if (lname.equals("y"))
  {
    ++m_readY;
    return AbstractElementReaderSP(new NumberPersistency<COORDINATE_TYPE>(m_y));
  }
  return AbstractElementReaderSP();
}

template <class POINT, typename COORDINATE_TYPE>
void QPointPersistencyT<POINT, COORDINATE_TYPE>::endedElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname)
{
  UNUSED_VAR(uri);
  UNUSED_VAR(localname);
  UNUSED_VAR(qname);

  if (m_readX != 1 || m_readY != 1)
    throw ParseException("invalid point");

  m_point.setX(m_x);
  m_point.setY(m_y);
}

template <class POINT, typename COORDINATE_TYPE>
void QPointPersistencyT<POINT, COORDINATE_TYPE>::Print(const POINT& point, ElementPrinter& printer)
{
  {
    ElementPrinter p(printer, "x");
    NumberPersistency<COORDINATE_TYPE>::Print(point.x(), p);
  }
  {
    ElementPrinter p(printer, "y");
    NumberPersistency<COORDINATE_TYPE>::Print(point.y(), p);
  }
}

}
#endif

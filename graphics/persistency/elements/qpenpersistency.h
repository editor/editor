/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _QPENPERISTENCY_H_
#define _QPENPERISTENCY_H_

#include <QPen>
#include "graphics/persistency/abstractelementreader.h"
#include "graphics/persistency/elementprinter.h"

namespace persistency
{

class QPenPersistency :
    public AbstractElementReader
{
public:
  QPenPersistency(QPen& pen);

public:
  static void Print(const QPen& pen, ElementPrinter& printer);
  virtual void endedElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname);

protected:
  virtual AbstractElementReaderSP CreateElementReader(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const XNQ Attributes& attrs);

private:
  QPen& m_pen;
  QBrush m_brush;
  qreal m_width;
  Qt::PenStyle m_style;
  Qt::PenCapStyle m_capStyle;
  Qt::PenJoinStyle m_joinStyle;
  unsigned m_readBrush;
  unsigned m_readWidth;
  unsigned m_readStyle;
  unsigned m_readCapStyle;
  unsigned m_readJoinStyle;
  static const char* m_brushTagName;
  static const char* m_widthTagName;
  static const char* m_styleTagName;
  static const char* m_capStyleTagName;
  static const char* m_joinStyleTagName;
};
}
#endif

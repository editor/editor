/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "graphics/persistency/elements/numberpersistency.h"
#include "graphics/persistency/elements/qbrushpersistency.h"
#include "graphics/persistency/elements/qpenpersistency.h"
#include "graphics/persistency/elements/qtpencapstylepersistency.h"
#include "graphics/persistency/elements/qtpenjoinstylepersistency.h"
#include "graphics/persistency/elements/qtpenstylepersistency.h"

namespace persistency
{

const char* QPenPersistency::m_brushTagName = "brush";
const char* QPenPersistency::m_widthTagName = "width";
const char* QPenPersistency::m_styleTagName = "style";
const char* QPenPersistency::m_capStyleTagName = "capStyle";
const char* QPenPersistency::m_joinStyleTagName = "joinStyle";


QPenPersistency::QPenPersistency(QPen& pen) :
  m_pen(pen),
  m_width(0),
  m_style(Qt::NoPen),
  m_readBrush(0),
  m_readWidth(0),
  m_readStyle(0),
  m_readCapStyle(0),
  m_readJoinStyle(0)
{
}

AbstractElementReaderSP QPenPersistency::CreateElementReader(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const XNQ Attributes& attrs)
{
  UNUSED_VAR(uri);
  UNUSED_VAR(qname);
  UNUSED_VAR(attrs);

  const LocalString lname(localname);

  if (lname.equals(m_brushTagName))
  {
    ++m_readBrush;
    return AbstractElementReaderSP(new QBrushPersistency(m_brush));
  }
  else if (lname.equals(m_widthTagName))
  {
    ++m_readWidth;
    return AbstractElementReaderSP(new NumberPersistency<qreal>(m_width));
  }
  else if (lname.equals(m_styleTagName))
  {
    ++m_readStyle;
    return AbstractElementReaderSP(new QtPenStylePersistency(m_style));
  }
  else if (lname.equals(m_capStyleTagName))
  {
    ++m_readCapStyle;
    return AbstractElementReaderSP(new QtPenCapStylePersistency(m_capStyle));
  }
  else if (lname.equals(m_joinStyleTagName))
  {
    ++m_readJoinStyle;
    return AbstractElementReaderSP(new QtPenJoinStylePersistency(m_joinStyle));
  }
  return AbstractElementReaderSP();
}

void QPenPersistency::endedElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname)
{
  UNUSED_VAR(uri);
  UNUSED_VAR(localname);
  UNUSED_VAR(qname);

  if (m_readBrush != 1 || m_readWidth != 1 || m_readStyle != 1 || m_readCapStyle != 1 ||  m_readJoinStyle != 1)
    throw ParseException("invalid pen");

  m_pen.setBrush(m_brush);
  m_pen.setWidth(m_width);
  m_pen.setStyle(m_style);
  m_pen.setCapStyle(m_capStyle);
  m_pen.setJoinStyle(m_joinStyle);
}

void QPenPersistency::Print(const QPen& pen, persistency::ElementPrinter& printer)
{
  {
    ElementPrinter p(printer, m_brushTagName);
    QBrushPersistency::Print(pen.brush(), p);
  }
  {
    ElementPrinter p(printer, m_widthTagName);
    NumberPersistency<qreal>::Print(pen.width(), p);
  }
  {
    ElementPrinter p(printer, m_styleTagName);
    QtPenStylePersistency::Print(pen.style(), p);
  }
  {
    ElementPrinter p(printer, m_capStyleTagName);
    QtPenCapStylePersistency::Print(pen.capStyle(), p);
  }
  {
    ElementPrinter p(printer, m_joinStyleTagName);
    QtPenJoinStylePersistency::Print(pen.joinStyle(), p);
  }
}

}


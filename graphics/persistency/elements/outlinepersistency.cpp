/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "utils/macros.h"
#include "graphics/persistency/elements/outlinepersistency.h"
#include "graphics/persistency/elements/qcolorpersistency.h"
#include "graphics/persistency/elements/numberpersistency.h"
#include "graphics/persistency/parseexception.h"

namespace persistency
{

const char* OutlinePersistency::m_colorTagName = "color";
const char* OutlinePersistency::m_widthTagName = "width";

OutlinePersistency::OutlinePersistency(graphics::Outline& item) :
  m_item(item),
  m_readColor(0),
  m_readWidth(0)
{
}

void OutlinePersistency::Print(const graphics::Outline& item, ElementPrinter& printer)
{
  // Color
  {
    ElementPrinter p(printer, m_colorTagName);
    QColorPersistency::Print(item.Color, p);
  }
  // width
  {
    ElementPrinter p(printer, m_widthTagName);
    NumberPersistency<qreal>::Print(item.Width, p);
  }
}

AbstractElementReaderSP OutlinePersistency::CreateElementReader(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const XNQ Attributes& attrs)
{
  UNUSED_VAR(uri);
  UNUSED_VAR(qname);
  UNUSED_VAR(attrs);

  const LocalString lname(localname);

  if (lname.equals(m_colorTagName))
  {
    ++ m_readColor;
    return AbstractElementReaderSP(new QColorPersistency(m_color));
  }
  else if (lname.equals(m_widthTagName))
  {
    ++ m_readWidth;
    return AbstractElementReaderSP(new NumberPersistency<qreal>(m_width));
  }
  return AbstractElementReaderSP();
}

void OutlinePersistency::endedElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname)
{
  UNUSED_VAR(uri);
  UNUSED_VAR(localname);
  UNUSED_VAR(qname);

  if (m_readColor != 1 || m_readWidth != 1)
  {
    throw ParseException("invalid font");
  }
  m_item.Color = m_color;
  m_item.Width = m_width;
}

}


/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "utils/macros.h"
#include "graphics/persistency/elements/textitempersistency.h"
#include "graphics/persistency/parseexception.h"
#include "graphics/persistency/elements/qstringpersistency.h"

namespace persistency
{

const char* TextItemPersistency::TAGNAME = "text";
const char* TextItemPersistency::m_textTagName = "text";

TextItemPersistency::TextItemPersistency(graphics::items::TextItem& item) :
  AbstractItemPersistency(item),
  m_item(item),
  m_readText(0)
{}

AbstractElementReaderSP TextItemPersistency::CreateElementReader(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const XNQ Attributes& attrs)
{
  const LocalString lname(localname);

  if (lname.equals(m_textTagName))
  {
    ++m_readText;
    return AbstractElementReaderSP(new QStringPersistency(m_text));
  }
  return AbstractItemPersistency::CreateElementReader(uri, localname, qname, attrs);
}

void TextItemPersistency::endedElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname)
{
  if (m_readText != 1)
    throw ParseException("invalid TextItem");

  AbstractItemPersistency::endedElement(uri, localname, qname);
  m_item.SetText(m_text);
}

void TextItemPersistency::Print(const graphics::items::TextItem& item, ElementPrinter& printer)
{
  AbstractItemPersistency::Print(item, printer);
  {
    ElementPrinter p(printer, m_textTagName);
    QStringPersistency::Print(item.GetText(), p);
  }
}

}

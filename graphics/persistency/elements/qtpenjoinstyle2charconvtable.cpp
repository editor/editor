/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "graphics/persistency/elements/qtpenjoinstyle2charconvtable.h"

namespace persistency
{

DEFINE_ENUM_STRING_CONVERSION_TABLE_BEGIN(QtPenJoinStyle2CharConvTable, Qt::PenJoinStyle)
ENUM_STRING_MAPPING(Qt::MiterJoin,    "MiterJoin")
ENUM_STRING_MAPPING(Qt::BevelJoin,    "BevelJoin")
ENUM_STRING_MAPPING(Qt::RoundJoin,    "RoundJoin")
ENUM_STRING_MAPPING(Qt::SvgMiterJoin, "SvgMiterJoin")
DEFINE_ENUM_STRING_CONVERSION_TABLE_END(Qt::PenJoinStyle)

}

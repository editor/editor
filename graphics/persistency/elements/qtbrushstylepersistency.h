/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _QTBRUSHSTYLEPERSISTENCY_H_
#define _QTBRUSHSTYLEPERSISTENCY_H_

#include "graphics/persistency/elements/enumpersistency.h"
#include "graphics/persistency/elements/qtbrushstyle2charconvtable.h"

namespace persistency
{

typedef persistency::EnumPersistency<QtBrushStyle2CharConvTable> QtBrushStylePersistency;
}

#endif


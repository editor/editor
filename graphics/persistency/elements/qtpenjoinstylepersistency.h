/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _QTPENJOINSTYLEPERSISTENCY_H_
#define _QTPENJOINSTYLEPERSISTENCY_H_

#include "graphics/persistency/elements/enumpersistency.h"
#include "graphics/persistency/elements/qtpenjoinstyle2charconvtable.h"

namespace persistency
{
typedef EnumPersistency<QtPenJoinStyle2CharConvTable> QtPenJoinStylePersistency;
}
#endif

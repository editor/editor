/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "utils/macros.h"
#include "utils/macros.h"
#include "graphics/persistency/xercestranscodedstring.h"
#include "graphics/persistency/elementprinter.h"
#include "graphics/persistency/elements/numberpersistency.h"
#include "graphics/persistency/parseexception.h"
#include "graphics/persistency/elements/qcolorpersistency.h"
#include "graphics/persistency/elements/qstringpersistency.h"

namespace persistency
{

const char* QColorPersistency::m_isValidAttrName = "isValid";
const char* QColorPersistency::m_isValidAttrNameTrue = "yes";
const char* QColorPersistency::m_isValidAttrNameFalse = "no";
const char* QColorPersistency::m_nameTagName = "name";
const char* QColorPersistency::m_alphaTagName = "alpha";

QColorPersistency::QColorPersistency(QColor& color) :
  m_color(color),
  m_alpha(0),
  m_isValid(true),
  m_readName(0),
  m_readAlpha(0)
{}

void QColorPersistency::startedElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const XNQ Attributes& attrs)
{
  UNUSED_VAR(uri);
  UNUSED_VAR(localname);
  UNUSED_VAR(qname);

  STRING attrVal;
  if (!GetOptionalAttributeValue(attrs, m_isValidAttrName, attrVal))
    return;

  if (attrVal == m_isValidAttrNameFalse)
  {
    m_isValid = false;
    return;
  }

  if (attrVal == m_isValidAttrNameTrue)
    return;

  throw ParseException("invalid color attribute value");
}

AbstractElementReaderSP QColorPersistency::CreateElementReader(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const XNQ Attributes& attrs)
{
  UNUSED_VAR(uri);
  UNUSED_VAR(qname);
  UNUSED_VAR(attrs);

  const LocalString lname(localname);

  if (lname.equals(m_nameTagName))
  {
    ++m_readName;
    return AbstractElementReaderSP(new QStringPersistency(m_name));
  }
  else if (lname.equals(m_alphaTagName))
  {
    ++m_readAlpha;
    return AbstractElementReaderSP(new NumberPersistency<int>(m_alpha));
  }
  return AbstractElementReaderSP();
}

void QColorPersistency::endedElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname)
{
  UNUSED_VAR(uri);
  UNUSED_VAR(localname);
  UNUSED_VAR(qname);

  if (m_readName != 1 || m_readAlpha != 1)
    throw ParseException("invalid color");

  m_color.setNamedColor(m_name);
  m_color.setAlpha(m_alpha);
}

void QColorPersistency::Print(const QColor& color, ElementPrinter& printer)
{
  if (!color.isValid())
    printer.PrintAttribute(m_isValidAttrName, m_isValidAttrNameFalse);

  {
    persistency::ElementPrinter p(printer, m_nameTagName);
    QStringPersistency::Print(color.name(), p);
  }
  {
    persistency::ElementPrinter p(printer, m_alphaTagName);
    NumberPersistency<int>::Print(color.alpha(), p);
  }

}

}

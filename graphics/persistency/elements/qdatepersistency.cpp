/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "utils/macros.h"
#include "graphics/persistency/xercestranscodedstring.h"
#include "graphics/persistency/parseexception.h"
#include "graphics/persistency/elements/qdatepersistency.h"

namespace persistency
{
QDatePersistency::QDatePersistency(QDate& dt):
  m_dt(dt),
  m_isValid(true)
{}

const char* QDatePersistency::m_isValidAttrName = "isValid";
const char* QDatePersistency::m_isValidAttrNameTrue = "yes";
const char* QDatePersistency::m_isValidAttrNameFalse = "no";

void QDatePersistency::startedElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const XNQ Attributes& attrs)
{
  UNUSED_VAR(uri);
  UNUSED_VAR(localname);
  UNUSED_VAR(qname);

  STRING attrVal;
  if (!GetOptionalAttributeValue(attrs, m_isValidAttrName, attrVal))
    return;

  if (attrVal == m_isValidAttrNameFalse)
  {
    m_isValid = false;
    return;
  }

  if (attrVal == m_isValidAttrNameTrue)
    return;

  throw ParseException("invalid date attribute value");
}

void QDatePersistency::ParseContent(const XMLChString& content)
{
  m_dt = QDate::fromString(CHAR_2_QSTR(LocalString(content)), Qt::ISODate);
  // throw exception when datetime was valid before and it's not valid now
  if (!m_dt.isValid() && m_isValid)
    throw persistency::ParseException("invalid date");
}

void QDatePersistency::Print(const QDate& dt, ElementPrinter& printer)
{
  if (!dt.isValid())
    printer.PrintAttribute(m_isValidAttrName, m_isValidAttrNameFalse);
  printer.PrintText(QSTR_2_STR(dt.toString(Qt::ISODate)));
}


}

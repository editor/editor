/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "graphics/persistency/parseexception.h"
#include "graphics/persistency/elements/qpolygonpersistency.h"
#include "graphics/persistency/elements/polylineitempersistency.h"

namespace persistency
{

const char* PolylineItemPersistency::TAGNAME = "polyline";

PolylineItemPersistency::PolylineItemPersistency(graphics::items::PolylineItem& item) :
  AbstractPolygonItemPersistency(item),
  m_item(item)
{}

void PolylineItemPersistency::Print(const graphics::items::PolylineItem& item, ElementPrinter& printer)
{
  AbstractPolygonItemPersistency::Print(item, printer);
}

}


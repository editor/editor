/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _PENITEMPERSISTENCY_H_
#define _PENITEMPERSISTENCY_H_

#include "graphics/persistency/abstractitempersistency.h"
#include "graphics/items/penitem.h"

namespace persistency
{

class PenItemPersistency :
    public AbstractItemPersistency
{
public:
  static const char* TAGNAME;

public:
  PenItemPersistency(graphics::items::PenItem& item);

public:
  static void Print(const graphics::items::PenItem& item, ElementPrinter& printer);
  virtual void endedElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname);

protected:
  virtual AbstractElementReaderSP CreateElementReader(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const XNQ Attributes& attrs);

private:
  graphics::items::PenItem& m_item;
  QPolygonF m_points;
  unsigned m_readPts;
  static const char* m_pointsTagName;
  static const char* m_pointTagName;
  static const char* m_slopeTagName;
};

}

#endif

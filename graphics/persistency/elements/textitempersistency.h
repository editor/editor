/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _TESTITEMPERSISTENCY_H_
#define _TESTITEMPERSISTENCY_H_

#include <QString>
#include "graphics/persistency/abstractitempersistency.h"
#include "graphics/items/textitem.h"

namespace persistency
{
class TextItemPersistency :
    public AbstractItemPersistency
{
public:
  static const char* TAGNAME;

public:
  TextItemPersistency(graphics::items::TextItem& item);

public:
  static void Print(const graphics::items::TextItem& item, ElementPrinter& printer);

protected:
  virtual AbstractElementReaderSP CreateElementReader(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const XNQ Attributes& attrs);
  virtual void endedElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname);

private:
  graphics::items::TextItem& m_item;
  QString m_text;
  unsigned m_readText;
  static const char* m_textTagName;
};

}

#endif

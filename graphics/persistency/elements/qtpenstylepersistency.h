/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _QTPENSTYLEPERSISTENCY_H_
#define _QTPENSTYLEPERSISTENCY_H_

#include "graphics/persistency/elements/enumpersistency.h"
#include "graphics/persistency/elements/qtpenstyle2charconvtable.h"

namespace persistency
{

typedef EnumPersistency<QtPenStyle2CharConvTable> QtPenStylePersistency;

}

#endif


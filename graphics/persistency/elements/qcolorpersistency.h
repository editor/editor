/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _QCOLORSPERSISTENCY_H_
#define _QCOLORSPERSISTENCY_H_

#include <QColor>
#include <QString>
#include "graphics/persistency/abstractelementreader.h"
#include "graphics/persistency/elementprinter.h"

namespace persistency
{

class QColorPersistency :
    public AbstractElementReader
{
public:
  QColorPersistency(QColor& color);

public:
  static void Print(const QColor& color, ElementPrinter& printer);

protected:
  virtual void startedElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const XNQ Attributes& attrs);
  virtual AbstractElementReaderSP CreateElementReader(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const XNQ Attributes& attrs);
  virtual void endedElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname);

private:
  QColor& m_color;
  QString m_name;
  int m_alpha;
  bool m_isValid;
  unsigned m_readName;
  unsigned m_readAlpha;
  static const char* m_isValidAttrName;
  static const char* m_isValidAttrNameTrue;
  static const char* m_isValidAttrNameFalse;
  static const char* m_nameTagName;
  static const char* m_alphaTagName;
};

}

#endif


/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "graphics/persistency/elements/qtpenstyle2charconvtable.h"

namespace persistency
{

DEFINE_ENUM_STRING_CONVERSION_TABLE_BEGIN(QtPenStyle2CharConvTable, Qt::PenStyle)
ENUM_STRING_MAPPING(Qt::NoPen,          "NoPen")
ENUM_STRING_MAPPING(Qt::SolidLine,      "SolidLine")
ENUM_STRING_MAPPING(Qt::DashLine,       "DashLine")
ENUM_STRING_MAPPING(Qt::DotLine,        "DotLine")
ENUM_STRING_MAPPING(Qt::DashDotLine,    "DashDotLine")
ENUM_STRING_MAPPING(Qt::DashDotDotLine, "DashDotDotLine")
DEFINE_ENUM_STRING_CONVERSION_TABLE_END(Qt::PenStyle)

}


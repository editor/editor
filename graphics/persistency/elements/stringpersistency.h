/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _STRINGPERSISTENCY_H_
#define _STRINGPERSISTENCY_H_

#include "utils/types.h"
#include "graphics/persistency/abstractcontentreader.h"
#include "graphics/persistency/elementprinter.h"

namespace persistency
{

template<class STRING, class PARSER>
class STRINGPersistencyT :
    public AbstractContentReader
{
public:
  typedef typename STRING::value_type value_type;
  typedef STRING string_type;

public:
  STRINGPersistencyT(string_type& str) :
    m_str(str)
  {}

public:
  static void Print(const string_type& str, ElementPrinter& printer)
  {
    printer.PrintText(str);
  }

  static void Print(const value_type* const str, ElementPrinter& printer)
  {
    printer.PrintText(str);
  }

protected:
  virtual void ParseContent(const XMLChString& content)
  {
    m_str = PARSER(content);
  }

private:
  string_type& m_str;
};

typedef STRINGPersistencyT<STRING, LocalString> STRINGS11n;

}

#endif

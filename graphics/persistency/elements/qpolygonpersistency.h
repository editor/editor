/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _QPOLYGONPERSISTENCY_H_
#define _QPOLYGONPERSISTENCY_H_

#include <QPolygon>
#include <QPolygonF>
#include "graphics/persistency/containerpersistency.h"
#include "graphics/persistency/elements/qpointpersistency.h"

namespace persistency
{
typedef ContainerPersistency<QPolygon, QPointPersistency> QPolygonPersistency;
typedef ContainerPersistency<QPolygonF, QPointFPersistency> QPolygonFAPersistency;
}

#endif


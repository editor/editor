/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _QSIZEPERSISTENCY_H_
#define _QSIZEPERSISTENCY_H_

#include "utils/macros.h"
#include <QSize>
#include <QSizeF>
#include "graphics/persistency/abstractelementreader.h"
#include "graphics/persistency/parseexception.h"
#include "graphics/persistency/elementprinter.h"
#include "graphics/persistency/elements/numberpersistency.h"

namespace persistency
{

template <class SIZE, typename COORDINATE_TYPE>
class QSizePersistencyT :
    public AbstractElementReader
{
public:
  QSizePersistencyT(SIZE& size) :
    m_size(size),
    m_readWidth(0),
    m_readHeight(0)
  {}

public:
  static void Print(const SIZE& size, ElementPrinter& printer);

protected:
  virtual AbstractElementReaderSP CreateElementReader(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const XNQ Attributes& attrs);
  virtual void endedElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname);

private:
  SIZE& m_size;
  COORDINATE_TYPE m_width;
  COORDINATE_TYPE m_height;
  unsigned m_readWidth;
  unsigned m_readHeight;
};

typedef QSizePersistencyT<QSize, int> QSizeS11n;
typedef QSizePersistencyT<QSizeF, qreal> QSizeFS11n;

template <class SIZE, typename COORDINATE_TYPE>
AbstractElementReaderSP QSizePersistencyT<SIZE, COORDINATE_TYPE>::CreateElementReader(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const XNQ Attributes& attrs)
{
  UNUSED_VAR(uri);
  UNUSED_VAR(qname);
  UNUSED_VAR(attrs);

  const LocalString lname(localname);

  if (lname.equals("width"))
  {
    ++m_readWidth;
    return AbstractElementReaderSP(new NumberPersistency<COORDINATE_TYPE>(m_width));
  }
  else if (lname.equals("height"))
  {
    ++m_readHeight;
    return AbstractElementReaderSP(new NumberPersistency<COORDINATE_TYPE>(m_height));
  }
  return AbstractElementReaderSP();
}

template <class SIZE, typename COORDINATE_TYPE>
void QSizePersistencyT<SIZE, COORDINATE_TYPE>::endedElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname)
{
  UNUSED_VAR(uri);
  UNUSED_VAR(localname);
  UNUSED_VAR(qname);

  if (m_readWidth != 1 || m_readHeight != 1)
    throw ParseException("invalid size");

  m_size.setWidth(m_width);
  m_size.setHeight(m_height);
}

template <class SIZE, typename COORDINATE_TYPE>
void QSizePersistencyT<SIZE, COORDINATE_TYPE>::Print(const SIZE& size, ElementPrinter& printer)
{
  {
    ElementPrinter p(printer, "width");
    NumberPersistency<COORDINATE_TYPE>::Print(size.width(), p);
  }
  {
    ElementPrinter p(printer, "height");
    NumberPersistency<COORDINATE_TYPE>::Print(size.height(), p);
  }
}

}

#endif


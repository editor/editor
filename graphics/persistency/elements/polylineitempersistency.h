/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _POLYLINEITEMPERSISTENCY_H_
#define _POLYLINEITEMPERSISTENCY_H_

#include <QPolygonF>
#include "graphics/persistency/elements/abstractpolygonitempersistency.h"
#include "graphics/items/polylineitem.h"

namespace persistency
{

class PolylineItemPersistency :
    public AbstractPolygonItemPersistency
{
public:
  static const char* TAGNAME;

public:
  PolylineItemPersistency(graphics::items::PolylineItem& item);

public:
  static void Print(const graphics::items::PolylineItem& item, ElementPrinter& printer);

private:
  graphics::items::PolylineItem& m_item;
};

}

#endif

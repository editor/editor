/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "graphics/persistency/parseexception.h"
#include "graphics/persistency/elements/abstractpolygonitempersistency.h"
#include "graphics/persistency/elements/qpolygonpersistency.h"

namespace persistency
{

const char* AbstractPolygonItemPersistency::TAGNAME = "abstractPolygon";
const char* AbstractPolygonItemPersistency::m_pointsTagName = "points";
const char* AbstractPolygonItemPersistency::m_pointTagName = "point";

AbstractPolygonItemPersistency::AbstractPolygonItemPersistency(graphics::items::AbstractPolygonItem& item) :
  AbstractItemPersistency(item),
  m_item(item),
  m_readPts(0)
{
}

AbstractElementReaderSP AbstractPolygonItemPersistency::CreateElementReader(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const XNQ Attributes& attrs)
{
  UNUSED_VAR(uri);
  UNUSED_VAR(qname);
  UNUSED_VAR(attrs);

  const LocalString lname(localname);
  if (lname.equals(m_pointsTagName))
  {
    ++m_readPts;
    return AbstractElementReaderSP(new QPolygonFAPersistency(m_points, m_pointTagName));
  }
  return AbstractItemPersistency::CreateElementReader(uri, localname, qname, attrs);
}

void AbstractPolygonItemPersistency::endedElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname)
{
  if (m_readPts != 1)
    throw ParseException("invalid AbstractPolygonItem");
  m_item.SetPolygon(m_points);
  AbstractItemPersistency::endedElement(uri, localname, qname);
}

void AbstractPolygonItemPersistency::Print(const graphics::items::AbstractPolygonItem& item, ElementPrinter& printer)
{
  AbstractItemPersistency::Print(item, printer);
  {
    ElementPrinter p(printer, m_pointsTagName);
    QPolygonFAPersistency::Print(item.GetPolygon(), m_pointTagName, p);
  }
}

}


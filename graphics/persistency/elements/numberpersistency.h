/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _NUMBERPERSISTENCY_H_
#define _NUMBERPERSISTENCY_H_

#include <boost/lexical_cast.hpp>

#include "graphics/persistency/abstractcontentreader.h"
#include "graphics/persistency/elementprinter.h"
#include "utils/types.h"

namespace persistency
{

template<typename NUMBER>
class NumberPersistency :
    public persistency::AbstractContentReader
{
public:
  NumberPersistency(NUMBER& i) :
    m_i(i)
  {}

public:
  static void Print(const NUMBER& i, ElementPrinter& printer)
  {
    printer.PrintText(boost::lexical_cast<STRING>(i));
  }

protected:
  virtual void ParseContent(const XMLChString& content)
  {
    m_i = boost::lexical_cast<NUMBER>(LocalString(content.c_str()).Get());
  }

private:
  NUMBER& m_i;
};

typedef NumberPersistency<INT32t> INT32PERSISTENCY;
typedef NumberPersistency<double> DoublePERSISTENCY;
typedef NumberPersistency<float>  FloatPERSISTENCY;

}

#endif

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _AREAITEMPERSISTENCY_H_
#define _AREAITEMPERSISTENCY_H_

#include <QPolygonF>
#include "graphics/persistency/elements/abstractpolygonitempersistency.h"
#include "graphics/items/areaitem.h"

namespace persistency
{
class AreaItemPersistency :
    public AbstractPolygonItemPersistency
{
public:
  static const char* TAGNAME;

public:
  AreaItemPersistency(graphics::items::AreaItem& item);

public:
  static void Print(const graphics::items::AreaItem& item, ElementPrinter& printer);

protected:
  virtual void endedElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname);

private:
  graphics::items::AreaItem& m_item;
};

}
#endif

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "graphics/persistency/parseexception.h"
#include "graphics/persistency/elements/areaitempersistency.h"
#include "graphics/persistency/elements/qpolygonpersistency.h"

namespace persistency
{

const char* AreaItemPersistency::TAGNAME = "area";

AreaItemPersistency::AreaItemPersistency(graphics::items::AreaItem& item) :
  AbstractPolygonItemPersistency(item),
  m_item(item)
{}

void AreaItemPersistency::endedElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname)
{
  m_item.SetClosed();
  AbstractPolygonItemPersistency::endedElement(uri, localname, qname);
}

void AreaItemPersistency::Print(const graphics::items::AreaItem& item, ElementPrinter& printer)
{
  AbstractPolygonItemPersistency::Print(item, printer);
}

}

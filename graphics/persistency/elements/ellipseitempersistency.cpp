/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "graphics/persistency/elements/ellipseitempersistency.h"
#include "graphics/persistency/elements/qsizepersistency.h"

namespace persistency
{

const char* EllipseItemPersistency::TAGNAME = "ellipse";
const char* EllipseItemPersistency::m_sizeTagName = "size";

EllipseItemPersistency::EllipseItemPersistency(graphics::items::EllipseItem& item) :
  AbstractItemPersistency(item),
  m_item(item),
  m_readSize(0)
{}

AbstractElementReaderSP EllipseItemPersistency::CreateElementReader(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const XNQ Attributes& attrs)
{
  UNUSED_VAR(uri);
  UNUSED_VAR(qname);
  UNUSED_VAR(attrs);

  const LocalString lname(localname);

  if (lname.equals(m_sizeTagName))
  {
    ++ m_readSize;
    return AbstractElementReaderSP(new QSizeFS11n(m_size));
  }
  return AbstractItemPersistency::CreateElementReader(uri, localname, qname, attrs);
}

void EllipseItemPersistency::endedElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname)
{
  if (m_readSize != 1)
  {
    throw ParseException("invalid ellipse");
  }
  m_item.SetSize(m_size);
  AbstractItemPersistency::endedElement(uri, localname, qname);
}

void EllipseItemPersistency::Print(const graphics::items::EllipseItem& item, ElementPrinter& printer)
{
  AbstractItemPersistency::Print(item, printer);
  {
    ElementPrinter p(printer, m_sizeTagName);
    QSizeFS11n::Print(item.GetSize(), p);
  }
}

}


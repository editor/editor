/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _ELLIPSEITEMPERSISTENCY_H_
#define _ELLIPSEITEMPERSISTENCY_H_

#include <QRectF>
#include "graphics/persistency/abstractitempersistency.h"
#include "graphics/items/ellipseitem.h"

namespace persistency
{

class EllipseItemPersistency :
    public AbstractItemPersistency
{
public:
  static const char* TAGNAME;

public:
  EllipseItemPersistency(graphics::items::EllipseItem& item);

public:
  static void Print(const graphics::items::EllipseItem& item, ElementPrinter& printer);
  virtual void endedElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname);

protected:
  virtual AbstractElementReaderSP CreateElementReader(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const XNQ Attributes& attrs);

private:
  graphics::items::EllipseItem& m_item;
  QSizeF m_size;
  unsigned m_readSize;
  static const char* m_sizeTagName;
};

}

#endif


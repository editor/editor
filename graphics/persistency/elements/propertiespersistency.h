/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _PROPERTIESPERSISTENCY_H_
#define _PROPERTIESPERSISTENCY_H_

#include <QBrush>
#include <QFont>
#include <QPen>
#include <QPointF>
#include "graphics/iproperties.h"
#include "graphics/outline.h"
#include "graphics/persistency/abstractelementreader.h"
#include "graphics/persistency/elementprinter.h"

namespace persistency
{

class PropertiesPersistency :
    public AbstractElementReader
{
public:
  PropertiesPersistency(graphics::IProperties& props);

public:
  static void Print(const graphics::IProperties& props, ElementPrinter& printer);

protected:
  virtual AbstractElementReaderSP CreateElementReader(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const XNQ Attributes& attrs);
  virtual void endedElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname);

private:
  void MarkPropertyRead(graphics::Property prop);

private:
  graphics::IProperties& m_props;
  graphics::Properties m_readProperties;
  QBrush m_brush;
  QFont m_font;
  QPen m_pen;
  graphics::Outline m_outlinePen;
  QPointF m_posn;
  qreal m_zValue;
  static const char* m_brushTagName;
  static const char* m_fontTagName;
  static const char* m_penTagName;
  static const char* m_outlinePenTagName;
  static const char* m_posnTagName;
};

}

#endif

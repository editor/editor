/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _QBRUSHPERSISTENCY_H_
#define _QBRUSHPERSISTENCY_H_

#include <QBrush>
#include "graphics/persistency/abstractelementreader.h"
#include "graphics/persistency/elementprinter.h"

namespace persistency
{

class QBrushPersistency :
    public AbstractElementReader
{
public:
  QBrushPersistency(QBrush& brush);

public:
  static void Print(const QBrush& brush, ElementPrinter& printer);

protected:
  virtual AbstractElementReaderSP CreateElementReader(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const XNQ Attributes& attrs);
  virtual void endedElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname);

private:
  QBrush& m_brush;
  QColor m_color;
  Qt::BrushStyle m_style;
  unsigned m_readColor;
  unsigned m_readStyle;
  static const char* m_colorTagName;
  static const char* m_styleTagName;

};

}
#endif

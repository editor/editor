/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "utils/macros.h"
#include "graphics/persistency/elementprinter.h"
#include "graphics/persistency/elements/numberpersistency.h"
#include "graphics/persistency/parseexception.h"
#include "graphics/persistency/elements/qfontpersistency.h"
#include "graphics/persistency/elements/qstringpersistency.h"

namespace persistency
{

const char* QFontPersistency::m_familyTagName = "family";
const char* QFontPersistency::m_ptSizeTagName = "pointSize";
const char* QFontPersistency::m_weightTagName = "weight";

QFontPersistency::QFontPersistency(QFont& font) :
  m_font(font),
  m_readFamily(0),
  m_readPtSize(0),
  m_readWeight(0)
{}

AbstractElementReaderSP QFontPersistency::CreateElementReader(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const XNQ Attributes& attrs)
{
  UNUSED_VAR(uri);
  UNUSED_VAR(qname);
  UNUSED_VAR(attrs);

  const LocalString lname(localname);

  if (lname.equals(m_familyTagName))
  {
    ++m_readFamily;
    return AbstractElementReaderSP(new QStringPersistency(m_family));
  }
  else if (lname.equals(m_ptSizeTagName))
  {
    ++m_readPtSize;
    return AbstractElementReaderSP(new NumberPersistency<int>(m_ptSize));
  }
  else if (lname.equals(m_weightTagName))
  {
    ++m_readWeight;
    return AbstractElementReaderSP(new NumberPersistency<int>(m_weight));
  }
  return AbstractElementReaderSP();
}

void QFontPersistency::endedElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname)
{
  UNUSED_VAR(uri);
  UNUSED_VAR(localname);
  UNUSED_VAR(qname);

  if (m_readFamily != 1 || m_readPtSize != 1 || m_readWeight != 1)
    throw ParseException("invalid font");

  m_font.setFamily(m_family);
  m_font.setPointSize(m_ptSize);
  m_font.setWeight(m_weight);
}

void QFontPersistency::Print(const QFont& font, ElementPrinter& printer)
{
  {
    ElementPrinter p(printer, m_familyTagName);
    QStringPersistency::Print(font.family(), p);
  }
  {
    ElementPrinter p(printer, m_ptSizeTagName);
    NumberPersistency<int>::Print(font.pointSize(), p);
  }
  {
    ElementPrinter p(printer, m_weightTagName);
    NumberPersistency<int>::Print(font.weight(), p);
  }
}

}

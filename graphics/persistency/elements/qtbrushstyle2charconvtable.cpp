/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "graphics/persistency/elements/qtbrushstyle2charconvtable.h"

namespace persistency
{

DEFINE_ENUM_STRING_CONVERSION_TABLE_BEGIN(QtBrushStyle2CharConvTable, Qt::BrushStyle)
ENUM_STRING_MAPPING(Qt::NoBrush,          "NoBrush")
ENUM_STRING_MAPPING(Qt::SolidPattern,     "Solid")
ENUM_STRING_MAPPING(Qt::Dense1Pattern,    "Dense1")
ENUM_STRING_MAPPING(Qt::Dense2Pattern,    "Dense2")
ENUM_STRING_MAPPING(Qt::Dense3Pattern,    "Dense3")
ENUM_STRING_MAPPING(Qt::Dense4Pattern,    "Dense4")
ENUM_STRING_MAPPING(Qt::Dense5Pattern,    "Dense5")
ENUM_STRING_MAPPING(Qt::Dense6Pattern,    "Dense6")
ENUM_STRING_MAPPING(Qt::Dense7Pattern,    "Dense7")
ENUM_STRING_MAPPING(Qt::HorPattern,       "Hor")
ENUM_STRING_MAPPING(Qt::VerPattern,       "Ver")
ENUM_STRING_MAPPING(Qt::CrossPattern,     "Cross")
ENUM_STRING_MAPPING(Qt::BDiagPattern,     "BDiag")
ENUM_STRING_MAPPING(Qt::FDiagPattern,     "FDiag")
ENUM_STRING_MAPPING(Qt::DiagCrossPattern, "DiagCross")
DEFINE_ENUM_STRING_CONVERSION_TABLE_END(Qt::BrushStyle)
}

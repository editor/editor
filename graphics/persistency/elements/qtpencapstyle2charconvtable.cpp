/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "graphics/persistency/elements/qtpencapstyle2charconvtable.h"

namespace persistency
{

DEFINE_ENUM_STRING_CONVERSION_TABLE_BEGIN(QtPenCapStyle2CharConvTable, Qt::PenCapStyle)
ENUM_STRING_MAPPING(Qt::FlatCap,    "FlatCap")
ENUM_STRING_MAPPING(Qt::SquareCap,  "SquareCap")
ENUM_STRING_MAPPING(Qt::RoundCap,   "RoundCap")
DEFINE_ENUM_STRING_CONVERSION_TABLE_END(Qt::PenCapStyle)

}


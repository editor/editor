/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "utils/macros.h"
#include "graphics/persistency/parseexception.h"
#include "graphics/persistency/xercestranscodedstring.h"
#include "graphics/persistency/elementprinter.h"
#include "graphics/persistency/elements/qstringpersistency.h"

namespace persistency
{

const char* QStringPersistency::m_isNullAttrName = "isNull";
const char* QStringPersistency::m_isNullAttrNameTrue = "yes";
const char* QStringPersistency::m_isNullAttrNameFalse = "no";

QStringPersistency::QStringPersistency(QString& str) :
  m_str(str),
  m_isNull(false)
{}

void QStringPersistency::startedElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const XNQ Attributes& attrs)
{
  UNUSED_VAR(uri);
  UNUSED_VAR(localname);
  UNUSED_VAR(qname);

  STRING attrVal;
  if (!GetOptionalAttributeValue(attrs, m_isNullAttrName, attrVal))
    return;

  if (attrVal == m_isNullAttrNameFalse)
    return; // not null, default value

  if (attrVal == m_isNullAttrNameTrue)
  {
    m_isNull = true;
    return;
  }

  throw ParseException("invalid string attribute value");
}

void QStringPersistency::ParseContent(const XMLChString& content)
{
  if (m_isNull)
  {
    m_str = QString();
  }
  else
  {
    m_str.setUtf16((const ushort*)&(content[0]), content.size());
  }
}

void QStringPersistency::Print(const QString& str, persistency::ElementPrinter& printer)
{
  if (str.isNull())
  {
    printer.PrintAttribute(m_isNullAttrName, m_isNullAttrNameTrue);
  }
  else
  {
    printer.PrintText(str.toStdString());
  }
}

}

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "graphics/persistency/elements/penitempersistency.h"
#include "graphics/persistency/elements/qpolygonpersistency.h"
#include "graphics/persistency/elements/stringpersistency.h"

namespace persistency
{

const char* PenItemPersistency::TAGNAME = "pen";
const char* PenItemPersistency::m_pointsTagName = "points";
const char* PenItemPersistency::m_pointTagName = "point";

PenItemPersistency::PenItemPersistency(graphics::items::PenItem& item) :
  AbstractItemPersistency(item),
  m_item(item),
  m_readPts(0)
{
}

AbstractElementReaderSP PenItemPersistency::CreateElementReader(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const XNQ Attributes& attrs)
{
  UNUSED_VAR(uri);
  UNUSED_VAR(qname);
  UNUSED_VAR(attrs);

  const LocalString lname(localname);

  if (lname.equals(m_pointsTagName))
  {
    ++m_readPts;
    return AbstractElementReaderSP(new QPolygonFAPersistency(m_points, m_pointTagName));
  }
  return AbstractItemPersistency::CreateElementReader(uri, localname, qname, attrs);
}

void PenItemPersistency::endedElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname)
{
  if (m_readPts != 1)
    throw ParseException("invalid PenItem");

  AbstractItemPersistency::endedElement(uri, localname, qname);
  m_item.SetPolyline(m_points);
}

void PenItemPersistency::Print(const graphics::items::PenItem& item, ElementPrinter& printer)
{
  AbstractItemPersistency::Print(item, printer);
  {
    ElementPrinter p(printer, m_pointsTagName);
    QPolygonFAPersistency::Print(item.GetPolyline(), m_pointTagName, p);
  }
}

}

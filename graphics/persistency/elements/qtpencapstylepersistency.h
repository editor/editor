/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _QTPENCAPSTYLEPERSISTENCY_H_
#define _QTPENCAPSTYLEPERSISTENCY_H_

#include "graphics/persistency/elements/enumpersistency.h"
#include "graphics/persistency/elements/qtpencapstyle2charconvtable.h"

namespace persistency
{
typedef EnumPersistency<QtPenCapStyle2CharConvTable> QtPenCapStylePersistency;
}

#endif


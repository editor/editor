/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _ABSTRACTPOLYGONITEMPERSISTENCY_H_
#define _ABSTRACTPOLYGONITEMPERSISTENCY_H_

#include <QPolygonF>
#include "graphics/persistency/abstractitempersistency.h"
#include "graphics/items/abstractpolygonitem.h"

namespace persistency
{

class AbstractPolygonItemPersistency :
    public AbstractItemPersistency
{
public:
  static const char* TAGNAME;

public:
  AbstractPolygonItemPersistency(graphics::items::AbstractPolygonItem& item);

public:
  static void Print(const graphics::items::AbstractPolygonItem& item, ElementPrinter& printer);

protected:
  virtual AbstractElementReaderSP CreateElementReader(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const XNQ Attributes& attrs);
  virtual void endedElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname);

private:
  graphics::items::AbstractPolygonItem& m_item;
  QPolygonF m_points;
  unsigned m_readPts;
  static const char* m_pointsTagName;
  static const char* m_pointTagName;
};
}
#endif

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _QFONTPERSISTENCY_H_
#define _QFONTPERSISTENCY_H_


#include <QFont>
#include <QString>
#include "graphics/persistency/abstractelementreader.h"
#include "graphics/persistency/elementprinter.h"

namespace persistency
{

class QFontPersistency :
    public AbstractElementReader
{
public:
  QFontPersistency(QFont& font);

public:
  static void Print(const QFont& font, ElementPrinter& printer);

protected:
  virtual AbstractElementReaderSP CreateElementReader(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const XNQ Attributes& attrs);
  virtual void endedElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname);

private:
  QFont& m_font;
  QString m_family;
  int m_ptSize;
  int m_weight;
  unsigned m_readFamily;
  unsigned m_readPtSize;
  unsigned m_readWeight;
  static const char* m_familyTagName;
  static const char* m_ptSizeTagName;
  static const char* m_weightTagName;
};

}

#endif


/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "utils/macros.h"
#include "graphics/persistency/parseexception.h"
#include "graphics/persistency/elements/qbrushpersistency.h"
#include "graphics/persistency/elements/qcolorpersistency.h"
#include "graphics/persistency/elements/qtbrushstylepersistency.h"

namespace persistency
{

const char* QBrushPersistency::m_colorTagName = "color";
const char* QBrushPersistency::m_styleTagName = "style";

QBrushPersistency::QBrushPersistency(QBrush& brush) :
  m_brush(brush),
  m_readColor(0),
  m_readStyle(0)
{
}

AbstractElementReaderSP QBrushPersistency::CreateElementReader(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const XNQ Attributes& attrs)
{
  UNUSED_VAR(uri);
  UNUSED_VAR(qname);
  UNUSED_VAR(attrs);

  const LocalString lname(localname);

  if (lname.equals(m_colorTagName))
  {
    ++m_readColor;
    return AbstractElementReaderSP(new QColorPersistency(m_color));
  }
  else if (lname.equals(m_styleTagName))
  {
    ++m_readStyle;
    return AbstractElementReaderSP(new QtBrushStylePersistency(m_style));
  }
  return AbstractElementReaderSP();
}

void QBrushPersistency::endedElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname)
{
  UNUSED_VAR(uri);
  UNUSED_VAR(localname);
  UNUSED_VAR(qname);

  if (m_readColor != 1 || m_readStyle != 1)
    throw ParseException("invalid brush");

  m_brush.setColor(m_color);
  m_brush.setStyle(m_style);
}

void QBrushPersistency::Print(const QBrush& brush, persistency::ElementPrinter& printer)
{
  {
    persistency::ElementPrinter p(printer, m_colorTagName);
    QColorPersistency::Print(brush.color(), p);
  }
  {
    persistency::ElementPrinter p(printer, m_styleTagName);
    QtBrushStylePersistency::Print(brush.style(), p);
  }
}

}

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _DEFAULTPRINTITEMVISITOR_H_
#define _DEFAULTPRINTITEMVISITOR_H_

#include "graphics/persistency/abstractprintvisitor.h"
#include "graphics/items/areaitem.h"
#include "graphics/items/ellipseitem.h"
#include "graphics/items/penitem.h"
#include "graphics/items/polylineitem.h"
#include "graphics/items/textitem.h"

namespace persistency
{

class DefaultPrintItemVisitor :
    public AbstractPrintVisitor
{
public:
  DefaultPrintItemVisitor();

public:
  virtual void Visit(graphics::items::AreaItem& item);
  virtual void Visit(graphics::items::EllipseItem& item);
  virtual void Visit(graphics::items::PenItem& item);
  virtual void Visit(graphics::items::PolylineItem& item);
  virtual void Visit(graphics::items::TextItem& item);
};

}

#endif

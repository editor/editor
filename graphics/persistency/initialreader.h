/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _INITIALREADER_H_
#define _INITIALREADER_H_

#include "graphics/persistency/abstractreader.h"

namespace persistency
{

  template
  <
    class DOCREADER,
    typename T
  >
  class InitialReader :
    public AbstractReader
  {
  public:
    InitialReader(const STRING& elemName, T& obj) :
      m_elemName(elemName),
      m_obj(obj)
    {}

  public:
    virtual void startDocument()
    {
      AbstractDocumentReaderSP dr(new DOCREADER(m_elemName, m_obj));
      PushDocumentReader(dr);
    }
  private:
    const STRING m_elemName;
    T& m_obj;
  };
}
#endif

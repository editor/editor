/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#ifndef _XERCESTRANSCCODEDSTRING_H_
#define _XERCESTRANSCCODEDSTRING_H_

#include <string>
#include <boost/utility.hpp>
#include <boost/static_assert.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/PlatformUtils.hpp>

#include "utils/types.h"
#include "utils/assert.h"
#include "graphics/persistency/xercesinitguard.h"

namespace persistency
{

/**
 * Type Traits for XercesTranscodedString
 * @tparam CHARTYPE character type
 */
template <typename CHARTYPE>
struct XercesTranscodedStringTraitsT {};

/**
 * Type Traits for XercesTranscodedString, specialized for XMLCh
 */
template <>
struct XercesTranscodedStringTraitsT<XMLCh>
{
  typedef XMLSize_t size_type; ///< type to measure size for this character type
};

/**
 * Type Traits for XercesTranscodedString, specialized for char
 */
template <>
struct XercesTranscodedStringTraitsT<char>
{
  typedef size_t size_type; ///< type to measure size for this character type
};


/**
 * Check combinations supported
 * @tparam FROMTYPE convert from this type
 * @tparam TOTYPE to this other type
 */
template<typename FROMTYPE, typename TOTYPE>
struct XercesTranscodedStringSupportedT
{
  static const bool Supported = false; ///< not any combination supported
};

/**
 * char to XMLCh is supported
 */
template<>
struct XercesTranscodedStringSupportedT<char, XMLCh>
{
  static const bool Supported = true;
};

/**
 * XMLCh to char is supported
 */
template<>
struct XercesTranscodedStringSupportedT<XMLCh, char>
{
  static const bool Supported = true;
};



/**
 * Helper class, which allows to convert local from Xerces XML string and vice versa
 */
template<typename FROMTYPE, typename TOTYPE>
class XercesTranscodedStringT :
    private boost::noncopyable
{
  BOOST_STATIC_ASSERT((XercesTranscodedStringSupportedT<FROMTYPE,TOTYPE>::Supported));

public:

  typedef typename XercesTranscodedStringTraitsT<TOTYPE>::size_type size_type;
  typedef TOTYPE value_type;

  /**
   * Constructor
   * @param toTranscode C string, which should be transcoded to Xerces String.
   */
  XercesTranscodedStringT(const FROMTYPE* const toTranscode) :
    m_initGuard(),
    m_str(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode(toTranscode)),
    m_length(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::stringLen(m_str))
  {}


  /**
   * Constructor
   * @param toTranscode STRING which should be transcoded to Xerces String.
   */
  XercesTranscodedStringT(const std::basic_string<FROMTYPE>& toTranscode) :
    m_initGuard(),
    m_str(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode(toTranscode.c_str())),
    m_length(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::stringLen(m_str))
  {}


  /**
   * Constructor
   * @param toCopy STRING which should be replicated.
   */
  XercesTranscodedStringT(const TOTYPE* const toCopy) :
    m_initGuard(),
    m_str(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::replicate(toCopy)),
    m_length(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::stringLen(m_str))
  {}


  /**
   * Destructor
   * @note MAKE VIRTUAL IF YOU DERIVE FROM THIS CLASS
   */
  ~XercesTranscodedStringT()
  {
    XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(const_cast<value_type**>(&m_str));
  }

public:
  /**
   * Get the converted string
   * @return The converted string
   */
  const value_type* Get() const
  {
    return m_str;
  }

  /**
   * Get the length of the string (not including the terminatig '\0'
   * @return the length
   */
  size_type Length() const
  {
    return m_length;
  }


  /**
   * Cast operator to const XMLCh* const
   * @return const pointer to the first character
   */
  operator const value_type* () const
  {
    return m_str;
  }

  /**
   * compares two strings
   * @param other string to compare with
   * @return true if equal in length and all characters
   */
  bool equals(const TOTYPE* const other) const
  {
    return XERCES_CPP_NAMESPACE_QUALIFIER XMLString::equals(m_str, other );
  }

  /**
   * compares two strings
   * @param other string to compare with
   * @return true if equal in length and all characters
   */
  bool equals(const std::basic_string<TOTYPE>& other) const
  {
    return equals(other.c_str());
  }



  /**
   * index operator
   * @param i the index to the element to access
   * @return reference to the i-th character
   */
  const value_type& operator[](const size_t i) const
  {
    ASSERT(static_cast<XMLSize_t>(i) <= m_length); // allow access to terminating 0
    return m_str[i];
  }


  /**
   * index operator
   * @param i the index to the element to access
   * @return reference to the i-th character
   */
  const value_type& operator[](const int i) const
  {
    ASSERT(i >= 0); // no negative index
    return m_str[static_cast<size_t>(i)];
  }


private:
  const XercesInitGuard m_initGuard; ///< make sure platform xerces utils are initialized, this is reference counted
  const value_type* const m_str; ///< pointer to the xerces string, memory allocated from Xerces memory manager
  const XMLSize_t m_length; ///< the length of the transcoded string (may differ from the length of the original string)
};


/**
 * Transcoding from local string to xerces string
 */
typedef XercesTranscodedStringT<char, XMLCh> XercesString;

/**
 * Transcoding from xerces to local string
 */
typedef XercesTranscodedStringT<XMLCh, char> LocalString;

}

#endif

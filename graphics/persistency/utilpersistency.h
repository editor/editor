/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _UTILPERSISTENCY_H_
#define _UTILPERSISTENCY_H_

#include <stdexcept>
#include <boost/filesystem/path.hpp>
#include <xercesc/sax/SAXParseException.hpp>
#include <xercesc/sax2/SAX2XMLReader.hpp>
#include <xercesc/sax2/XMLReaderFactory.hpp>
#include <xercesc/util/XMLException.hpp>
#include "utils/log.h"
#include "graphics/persistency/documentreader.h"
#include "graphics/persistency/initialreader.h"
#include "graphics/persistency/xercesinitguard.h"
#include "graphics/persistency/xercestranscodedstring.h"
#include "graphics/persistency/elementprinter.h"
#include "graphics/persistency/documentprinter.h"
#include "graphics/persistency/parseexception.h"
#include "graphics/persistency/serializeexception.h"
#include "utils/macros.h"
#include "utils/types.h"

namespace persistency
{

  /** Helper function, which should be used, to read an object from a XML-file.
  * @tparam S11N Type of the very first AbstractElementReader, which is used to parse the XML-file
  * @tparam T Type of the object, which should be read
  * @param obj Reference to the object, which should be read
  * @param rootTagName Name of XML root tag
  * @param filename Filename
  * @throw ParseException if something gets wrong while loading the file, e.g. syntax error or file doesn't exist
  */
  template
  <
      class S11N,
      typename T
      >
  void Load(T& obj, const STRING& rootTagName, const boost::filesystem::path& filename)
  {
    const XercesInitGuard initXercesPlatform;

    try
    {
      //create SAX2 parser
      LOG_FUNC_DEBUG("create SAX2 parser ...");
      typedef std::unique_ptr<XNQ SAX2XMLReader> SAX2XMLReaderAP;
      SAX2XMLReaderAP parser(XNQ XMLReaderFactory::createXMLReader());
      ASSERT(parser.get());
      LOG_FUNC_DEBUG("... done");

      //create StackHandler
      StackHandler stackHandler;

      //create initial Reader
      typedef DocumentReader<S11N, T> DocReader;
      typedef InitialReader< DocReader , T> InitReader;
      AbstractReaderSP initReader(new InitReader(rootTagName, obj));

      //push intial reader on stack
      initReader->SetStackHandler(&stackHandler);
      stackHandler.Push(initReader);

      //register stack handler
      stackHandler.RegisterToParser(*(parser.get()));

      //let's go
      LOG_FUNC_DEBUG("parse '%s' ...", PATH_2_CHAR(filename));
      //parser->parse(PATH_2_CHAR(filename));
      parser->parse(filename.c_str());
      LOG_FUNC_DEBUG("... '%s' parsed", PATH_2_CHAR(filename));

      ASSERT(stackHandler._stack.size() == 1);
    }
    catch (const XNQ XMLException& toCatch)
    {
      throw ParseException(persistency::LocalString(toCatch.getMessage()).Get());
    }
    catch (const XNQ SAXParseException& toCatch)
    {
      throw ParseException(persistency::LocalString(toCatch.getMessage()).Get());
    }
    catch (const ParseException& toCatch)
    {
      throw;
    }
    catch (const std::exception& ex)
    {
      throw SerializeException(ex.what());
    }
    catch (...)
    {
      throw ParseException("unknown exception");
    }
  }

  /** Helper function, which should be used, to read an object from a XML-file.
  * @tparam S11N Type of the very first AbstractElementReader, which is used to parse the XML-file
  * @tparam T Type of the object, which should be printed
  * @param obj Reference to the object, which should be read
  * @param rootTagName Name of XML root tag
  * @param filename Filename
  * @throw SerializeException if something gets wrong while saving the file, e.g. disk is full*/
  template
  <
      class S11N,
      typename T
      >
  void Save(const T& obj, const STRING& rootTagName, const boost::filesystem::path& filename, bool standalone = true, DocumentPrinter::Encoding encoding = DocumentPrinter::EncodingUTF8, bool prettyPrint = true)
  {
    const XercesInitGuard initXercesPlatform;

    try
    {
      DocumentPrinter printer(filename, encoding, standalone, prettyPrint);
      printer.CreateRootElement(rootTagName);
      S11N::Print(obj, printer.RootElement());
    }
    catch (const XNQ XMLException& toCatch)
    {
      throw SerializeException(persistency::LocalString(toCatch.getMessage()).Get());
    }
    catch (const SerializeException& toCatch)
    {
      throw;
    }
    catch (const std::exception& ex)
    {
      throw SerializeException(ex.what());
    }
    catch (...)
    {
      throw SerializeException("unknown exception");
    }
  }

}

#endif

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _ABSTRACTITEMPERSISTENCY_H_
#define _ABSTRACTITEMPERSISTENCY_H_

#include "graphics/items/abstractitem.h"
#include "graphics/persistency/elements/propertiespersistency.h"

namespace persistency
{
class AbstractItemPersistency :
  public PropertiesPersistency
{
protected:
  AbstractItemPersistency(graphics::items::AbstractItem& item);

public:
  static void Print(const graphics::items::AbstractItem& item, ElementPrinter& printer);
};

}
#endif

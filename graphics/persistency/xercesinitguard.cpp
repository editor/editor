/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <xercesc/util/PlatformUtils.hpp>
#include "graphics/persistency/xercesinitguard.h"
#include "utils/assert.h"

namespace persistency
{

DEFINE_CLASS_LOGGER(XercesInitGuard);

boost::mutex XercesInitGuard::m_mutex;
bool XercesInitGuard::m_initialized = false;
unsigned XercesInitGuard::m_instanceCounter = 0;
bool XercesInitGuard::m_neverRelease = false;
XercesPanicHandler XercesInitGuard::m_panicHandler;

XercesInitGuard::XercesInitGuard()
{
  MutexGuard guard(m_mutex);
  if (!m_initialized)
    Init();
  ASSERT(m_initialized);
  ++m_instanceCounter;
}

XercesInitGuard::~XercesInitGuard()
{
  MutexGuard guard(m_mutex);
  --m_instanceCounter;
  ASSERT(m_initialized);
  if (m_instanceCounter == 0 && !m_neverRelease)
    Release();
}

void XercesInitGuard::SetNeverRelease(bool nr)
{
  MutexGuard guard(m_mutex);
  if (nr == m_neverRelease)
    return;
  m_neverRelease = nr;
  if (m_instanceCounter == 0 && m_initialized && !m_neverRelease)
    Release();
}

void XercesInitGuard::Init()
{
  LOG_DEBUG("init Xerces ...");
  ASSERT(!m_initialized);
  XERCES_CPP_NAMESPACE_QUALIFIER XMLPlatformUtils::Initialize(
        XERCES_CPP_NAMESPACE_QUALIFIER XMLUni::fgXercescDefaultLocale,
        nullptr,
        &m_panicHandler
        );
  m_initialized = true;
  LOG_DEBUG("... done");
}

void XercesInitGuard::Release()
{
  try
  {
    LOG_DEBUG("terminate Xerces ...");
    ASSERT(m_initialized);
    XERCES_CPP_NAMESPACE_QUALIFIER XMLPlatformUtils::Terminate();
    m_initialized = false;
    LOG_DEBUG("... done");
  }
  catch (...)
  {
    m_initialized = false;
    ASSERT(false);
  }
}

}

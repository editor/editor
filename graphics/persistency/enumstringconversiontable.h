/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _ENUMSTRINGCONVERSIONTABLE_H_
#define _ENUMSTRINGCONVERSIONTABLE_H_

#include <stdexcept>
#include <typeinfo>
#include <xercesc/util/XMLString.hpp>
#include "utils/assert.h"
#include "utils/macros.h"
#include "utils/types.h"

namespace persistency
{

class BadStringToEnumConversionException :
    public std::runtime_error
{
public:
  BadStringToEnumConversionException(const std::string& msg) :
    std::runtime_error(msg)
  {}
};

class UnknownEnumToStringConversionException :
    public std::logic_error
{
public:
  UnknownEnumToStringConversionException(const std::string& msg) :
    std::logic_error(msg)
  {}
};

class StandardConversionTable {};

#define DECLARE_ENUM_STRING_CONVERSION_TABLE(NAME, ENUM)      \
  typedef ::persistency::EnumStringConversionTable<ENUM> NAME;

#define DEFINE_ENUM_STRING_CONVERSION_TABLE_BEGIN(NAME, ENUM) \
  template<>                                                  \
  const NAME::TableEntry NAME::_items[] =                     \
{

#define ENUM_STRING_MAPPING(ENUM, STRING)                     \
{true, ENUM, STRING},

#define DEFINE_ENUM_STRING_CONVERSION_TABLE_END(ENUM)         \
{false, (ENUM)(0), ""}                                        \
};

/**
 * Conversion table
 * @tparam ENUM the enumeration type
 * @tparam CHAR the character type used for the strings, default is char
 * @tparam VARIANT a type to distinguish conversion tables for same enum and char type but different
 *                 strings within the same namespace. Default is detail::StandardConversionTable
 *
 *  /// Usage Example:
 *  /// declare a table type (in the header or cpp file)
 *	DECLARE_ENUM_STRING_CONVERSION_TABLE(RectangleColorEnumStringConversionTable, Rectangle::Color)
 *
 *	/// define the conversion table (in the cpp file)
 *	DEFINE_ENUM_STRING_CONVERSION_TABLE_BEGIN(RectangleColorEnumStringConversionTable, Rectangle::Color)
 *		ENUM_STRING_MAPPING(Rectangle::CO_Black, "Black")
 *		ENUM_STRING_MAPPING(Rectangle::CO_Yellow, "Yellow")
 *	DEFINE_ENUM_STRING_CONVERSION_TABLE_END(Rectangle::Color)
 *
 *  /// Lookup
 *  ( RectangleColorEnumStringConversionTable::AsEnum("Green") == Rectangle::CO_Green ) yields true;
 *  ( RectangleColorEnumStringConversionTable::AsString(Rectangle::CO_Yellow) == std::string("Yellow") ) yields true;
 */
template <typename ENUM, typename CHAR = char, typename VARIANT = StandardConversionTable>
struct EnumStringConversionTable
{
  typedef ENUM enum_type; ///< the enumeration type, passed as template param ENUM
  typedef CHAR char_type; ///< the character type for the strings, passed as template param CHAR

  /**
   * Conversion from enum value to a string
   * @param asEnum the enum value
   * @return the character string
   * @note throws detail::UnknownEnumToStringConversionException if the table is incomplete (no translation)
   */
  static const char_type* AsString(const enum_type asEnum)
  {
    for (size_t i = 0; _items[i].IsValid; ++i)
    {
      if (_items[i].AsEnum == asEnum)
      {
        ASSERT(_items[i].AsString)
            return _items[i].AsString;
      }
    }
    ASSERT(false);
    OSTRINGSTREAM os;
    os << "unknown enum value: " << typeid(enum_type).name() << " " << asEnum;
    throw UnknownEnumToStringConversionException(os.str());
  }


  /**
   * Conversion from string to enum value
   * @param asString the string
   * @return the enum value
   * @note throws detail::BadStringToEnumConversionException if the string does not map
   */
  static const enum_type AsEnum(const char_type* const asString)
  {
    ASSERT(asString);
    for (size_t i = 0; _items[i].IsValid; ++i)
    {
      if (XERCES_CPP_NAMESPACE_QUALIFIER
          XMLString::equals(_items[i].AsString, asString))
      {
        return _items[i].AsEnum;
      }
    }
    OSTRINGSTREAM os;
    os << "unknown enum string: " << typeid(enum_type).name() << " " << asString;
    throw BadStringToEnumConversionException(os.str());
  }

private:
  /** the table is a list of enum to string pairs */
  struct TableEntry
  {
    bool IsValid; ///< contains true for all valid table entries
    enum_type AsEnum; ///< the enum value
    const char_type* AsString; ///< the corresponding string
  };
  static const TableEntry _items[]; ///< the table
};

}
#endif

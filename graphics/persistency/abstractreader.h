/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _ABSTRACTREADER_H_
#define _ABSTRACTREADER_H_

#include <memory.h>
#include "graphics/persistency/stackhandler.h"

namespace persistency
{

class AbstractElementReader;
typedef std::shared_ptr<AbstractElementReader> AbstractElementReaderSP;


class AbstractDocumentReader;
typedef std::shared_ptr<AbstractDocumentReader> AbstractDocumentReaderSP;

class AbstractReader;
typedef std::shared_ptr<AbstractReader> AbstractReaderSP;

class AbstractReader :
    public XNQ DefaultHandler
{
protected:
  AbstractReader();

public:
  virtual ~AbstractReader();

  void SetStackHandler(StackHandler* sh);
  StackHandler* GetStackHandler();

protected:
  void PushDocumentReader(AbstractDocumentReaderSP reader);
  void PushElementReader(AbstractElementReaderSP reader, const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const XNQ Attributes& attrs);
  void Pop();

private:
  void Push(AbstractReaderSP reader);

private:
  StackHandler* m_stackHandler;
};

}

#endif


/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _HEAPALLOCATEDITEM_H_
#define _HEAPALLOCATEDITEM_H_

#include <memory>

namespace persistency
{

template <class T>
class HeapAllocatedItem :
    public std::unique_ptr<T>
{
public:
  HeapAllocatedItem() :
    std::unique_ptr<T>(new T())
  {}
};
}
#endif

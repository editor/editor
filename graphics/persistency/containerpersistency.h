/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _CONTAINERPERSISTENCY_H_
#define _CONTAINERPERSISTENCY_H_

#include "utils/types.h"
#include "graphics/persistency/abstractelementreader.h"
#include "graphics/persistency/containeradapterfactory.h"
#include "graphics/persistency/elementprinter.h"
#include "graphics/persistency/xercestranscodedstring.h"
#include "graphics/persistency/parseexception.h"

namespace persistency
{

template <class CONTAINER, class ITEM_PERSISTENCY>
class ContainerPersistency :
    public AbstractElementReader
{
public:
  typedef CONTAINER Container;
  typedef typename ContainerAdapterFactory<ITEM_PERSISTENCY, CONTAINER>::Adapter Adapter;

public:
  ContainerPersistency(CONTAINER& container, const STRING& tagName) :
    m_container(container),
    m_tagName(tagName)
  {}

public:
  static void Print(const CONTAINER& container, const STRING& tagName, ElementPrinter& printer)
  {
    for (typename CONTAINER::const_iterator it = container.begin();
         it != container.end();
         ++it)
    {
      ElementPrinter itemPrinter(printer, tagName);
      Adapter::PrintIterator(it, itemPrinter);
    }
  }

public:
  virtual void startedElement(const XMLCh* const /*uri*/, const XMLCh* const /*localname*/, const XMLCh* const /*qname*/, const XNQ Attributes& /*attrs*/)
  {
    m_container.clear();
  }

protected:
  virtual AbstractElementReaderSP CreateElementReader(const XMLCh* const /*uri*/, const XMLCh* const localname, const XMLCh* const /*qname*/, const XNQ Attributes& /*attrs*/)
  {
    const LocalString lname(localname);

    if (!lname.equals(m_tagName))
      return AbstractElementReaderSP();

    typename Adapter::ItemType item;
    return AbstractElementReaderSP(new Adapter(m_container, item));
  }

private:
  CONTAINER& m_container;
  const STRING m_tagName;
};

#define DECLARE_CONTAINER_S11N(CLASS_NAME, CONTAINER, ITEM_PERSISTENCY, TAG_NAME)             \
  class CLASS_NAME :                                                                          \
  public ::persistency::ContainerPersistency<CONTAINER, ITEM_PERSISTENCY>                     \
{                                                                                             \
  public:                                                                                     \
  inline CLASS_NAME(CONTAINER& c) :                                                           \
  ::persistency::ContainerPersistency<CONTAINER, ITEM_PERSISTENCY>(c, TAG_NAME)               \
{}                                                                                            \
                                                                                              \
  public:                                                                                     \
  inline static void Print(const CONTAINER& pair, ::s11n::ElementPrinter& printer)            \
{                                                                                             \
  ::persistency::ContainerPersistency<CONTAINER, ITEM_PERSISTENCY>::Print(pair, TAG_NAME, printer);  \
}                                                                                             \
};

}

#endif

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <stdexcept>
#include "graphics/persistency/xercespanichandler.h"

namespace persistency
{

void XercesPanicHandler::panic(const PanicReasons reason)
{
  throw std::runtime_error(getPanicReasonString(reason));
}

}


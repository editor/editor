#ifndef _ABSTRACTCREATEREADERVISITOR_H_
#define _ABSTRACTCREATEREADERVISITOR_H_

#include <memory>
#include "utils/basevisitor.h"
#include "graphics/persistency/abstractelementreader.h"

namespace persistency
{

class AbstractCreateReaderVisitor :
    public BaseVisitor
{
protected:
  AbstractCreateReaderVisitor()
  {}

public:
  AbstractElementReaderSP GetReader()
  {
    return m_reader;
  }

protected:
  void SetReader(AbstractElementReaderSP reader)
  {
    m_reader = reader;
  }

private:
  AbstractElementReaderSP m_reader;
};

typedef std::unique_ptr<AbstractCreateReaderVisitor> CreateReaderVisitorAP;

}

#endif


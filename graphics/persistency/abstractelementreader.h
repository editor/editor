/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _ABSTRACTELEMENTREADER_H_
#define _ABSTRACTELEMENTREADER_H_

#include "utils/log.h"
#include "graphics/persistency/abstractreader.h"
#include "utils/types.h"

namespace persistency
{

class AbstractElementReader :
    public AbstractReader
{
  DECLARE_CLASS_LOGGER

  public:
    virtual void startedElement(const XMLCh* const /*uri*/, const XMLCh* const /*localname*/, const XMLCh* const /*qname*/, const XNQ Attributes& /*attrs*/) {}
  virtual void endedElement(const XMLCh* const /*uri*/, const XMLCh* const /*localname*/, const XMLCh* const /*qname*/) {}

public:
  virtual void startElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const XNQ Attributes& attrs); //do not overwrite
  virtual void endElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname); //do not overwrite

protected:
  virtual AbstractElementReaderSP CreateElementReader(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const XNQ Attributes& attrs) = 0;

protected:
  static STRING GetAttributeValue(const XNQ Attributes& attrs, const char* const attrName);
  static bool GetOptionalAttributeValue(const XNQ Attributes& attrs, const char* const attrName, STRING& value);
};

}

#endif

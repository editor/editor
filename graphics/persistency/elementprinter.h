/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _ELEMENTPRINTER_H_
#define _ELEMENTPRINTER_H_


#include <boost/utility.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/scoped_ptr.hpp>

#include "utils/types.h"

#include "graphics/persistency/xercestranscodedstring.h"

namespace persistency
{
class XMLPrinter;

class ElementPrinter :
    private boost::noncopyable
{
  friend class DocumentPrinter;

private:
  ElementPrinter(persistency::XMLPrinter& rootPrinter, const STRING& elementTag);


public:
  ElementPrinter(ElementPrinter& parentPrinter, const STRING& elementTag);
  ~ElementPrinter();

public:
  void PrintText(const STRING& text);
  void PrintText(const XMLCh* text);

  template<typename TEXT>
  void PrintText(const TEXT& val)
  {
    PrintText(boost::lexical_cast<STRING>(val));
  }

  void PrintAttribute(const STRING& attributeName, const STRING& attributeValue);

  template<typename ATTRIBUTE>
  void PrintAttribute(const STRING& attributeName, const ATTRIBUTE& attributeValue)
  {
    PrintAttribute(attributeName, boost::lexical_cast<STRING>(attributeValue));
  }

private:
  persistency::XMLPrinter& m_xmlPrinter; ///< the printer, maintains the open tag
  persistency::XercesString m_elementTag; ///< the tag name, used again when writing the closing tag in the destructor
};

typedef boost::scoped_ptr<ElementPrinter> ElementPrinterAP;

}

#endif

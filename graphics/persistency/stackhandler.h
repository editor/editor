/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _STACKHANDLER_H_
#define _STACKHANDLER_H_

#include <xercesc/sax2/DefaultHandler.hpp>
#include <xercesc/sax2/SAX2XMLReader.hpp>

#include <stack>
#include <memory>

#include "utils/log.h"
#include "utils/assert.h"
#include "utils/types.h"

#define XNQ XERCES_CPP_NAMESPACE_QUALIFIER

namespace persistency
{

typedef std::shared_ptr<XERCES_CPP_NAMESPACE_QUALIFIER DefaultHandler> DefaultHandlerSP;

class StackHandler :
    public XNQ DefaultHandler
{
  DECLARE_CLASS_LOGGER

  public:
    StackHandler();
  virtual ~StackHandler();

public:
  void Push(DefaultHandlerSP handler);
  void Pop();
  void RegisterToParser(XNQ SAX2XMLReader& parser);

public: //ContentHandler Interface
  virtual void characters(const XMLCh* const chars, const XMLSize_t length);
  virtual void endDocument();
  virtual void endElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname);
  virtual void ignorableWhitespace(const XMLCh* const chars, const XMLSize_t length);
  virtual void processingInstruction(const XMLCh* const target, const XMLCh* const data);
  virtual void resetDocument();
  virtual void setDocumentLocator(const XNQ Locator* const locator);
  virtual void startDocument();
  virtual void startElement(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const XNQ Attributes& attrs);
  virtual void startPrefixMapping(const XMLCh* const prefix, const XMLCh* const uri);
  virtual void endPrefixMapping(const XMLCh* const prefix);
  virtual void skippedEntity(const XMLCh* const name);
  virtual XNQ InputSource* resolveEntity(const XMLCh* const publicId, const XMLCh* const systemId);
  virtual void error(const XNQ SAXParseException& exc);
  virtual void fatalError(const XNQ SAXParseException& exc);
  virtual void warning(const XNQ SAXParseException& exc);
  virtual void resetErrors();
  virtual void notationDecl(const XMLCh* const name, const XMLCh* const publicId, const XMLCh* const systemId);
  virtual void resetDocType();
  virtual void unparsedEntityDecl(const XMLCh* const name, const XMLCh* const publicId, const XMLCh* const systemId, const XMLCh* const notationName);
  virtual void comment(const XMLCh* const chars, const XMLSize_t length);
  virtual void endCDATA();
  virtual void endDTD();
  virtual void endEntity(const XMLCh* const name);
  virtual void startCDATA();
  virtual void startDTD(const XMLCh* const name, const XMLCh* const publicId, const XMLCh* const systemId);
  virtual void startEntity(const XMLCh* const name);
  virtual void elementDecl(const XMLCh* const name, const XMLCh* const model);
  virtual void attributeDecl(const XMLCh* const eName, const XMLCh* const aName, const XMLCh* const type, const XMLCh* const mode, const XMLCh* const value);
  virtual void internalEntityDecl(const XMLCh* const name, const XMLCh* const value);
  virtual void externalEntityDecl(const XMLCh* const name, const XMLCh* const publicId, const XMLCh* const systemId);

public:
  DefaultHandlerSP TopHandler() const;

#ifdef SUPPORT_LOGGING_DEBUG
private:
  static STRING IndentStr(UINT32t depth);
#endif

#ifdef SUPPORT_ASSERT
public:
#else
private:
#endif
  typedef std::stack<DefaultHandlerSP> Stack;
  Stack m_stack;
};

}
#endif

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _SIMPLEASSOCIATIVECONTAINER_H_
#define _SIMPLEASSOCIATIVECONTAINER_H_

#include "graphics/persistency/elementprinter.h"
#include "graphics/persistency/heapallocateditem.h"

namespace persistency
{

template <class ITEM_S11N, class CONTAINER>
class SimpleAssociativeContainerAdapter :
    public ITEM_S11N
{
public:
  typedef typename CONTAINER::const_iterator ConstIterator;
  typedef HeapAllocatedItem<typename CONTAINER::value_type> ItemType;

public:
  SimpleAssociativeContainerAdapter(CONTAINER& container, ItemType item) :
    ITEM_S11N(*(item.get())),
    m_container(container),
    m_item(item)
  {}

  virtual ~SimpleAssociativeContainerAdapter()
  {
    m_container.insert(*(m_item.get()));
  }

public:
  static void PrintIterator(ConstIterator it, ElementPrinter& printer)
  {
    Print(*it, printer);
  }

private:
  CONTAINER& m_container;
  ItemType m_item;
};

}

#endif


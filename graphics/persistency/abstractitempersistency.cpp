/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "graphics/persistency/abstractitempersistency.h"

namespace persistency
{

AbstractItemPersistency::AbstractItemPersistency(graphics::items::AbstractItem& item) :
  PropertiesPersistency(item)
{}

void AbstractItemPersistency::Print(const graphics::items::AbstractItem& item, ElementPrinter& printer)
{
  PropertiesPersistency::Print(item, printer);
}

}


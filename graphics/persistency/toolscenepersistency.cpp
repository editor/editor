/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "graphics/persistency/elements/propertiespersistency.h"
#include "graphics/persistency/toolsceneitemspersistency.h"
#include "graphics/persistency/toolscenepersistency.h"

namespace persistency
{

 const char* ToolScenePersistency::m_propertiesTagName = "defaults";
 const char* ToolScenePersistency::m_itemsTagName = "items";

  ToolScenePersistency::ToolScenePersistency(graphics::ToolScene& scene) :
    m_scene(scene)
  {}

   AbstractElementReaderSP ToolScenePersistency::CreateElementReader(const XMLCh* const /*uri*/, const XMLCh* const localname, const XMLCh* const /*qname*/, const XNQ Attributes& /*attrs*/)
   {
     const LocalString lname(localname);

     if (lname.equals(m_propertiesTagName))
     {
       return AbstractElementReaderSP(new PropertiesPersistency(m_scene));
     }
     else if (lname.equals(m_itemsTagName))
     {
       return AbstractElementReaderSP(new ToolSceneItemsPersistency(m_scene));
     }
     return AbstractElementReaderSP();
   }

    void ToolScenePersistency::Print(const graphics::ToolScene& scene, ElementPrinter& printer)
    {
      printer.PrintAttribute("xmlns", "http://noob.com/VERS/1");
      {
        ElementPrinter p(printer, m_propertiesTagName);
        PropertiesPersistency::Print(scene, p);
      }
      {
        ElementPrinter p(printer, m_itemsTagName);
        ToolSceneItemsPersistency::Print(scene, p);
      }
    }

}

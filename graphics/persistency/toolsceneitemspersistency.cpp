/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "graphics/persistency/parseexception.h"
#include "graphics/persistency/serializeexception.h"
#include "graphics/items/itemregistry.h"
#include "graphics/items/abstractitem.h"
//#include "graphics/throwexceptionxatchall.h"
#include "graphics/persistency/toolsceneitemspersistency.h"
#include "utils/types.h"

namespace persistency
{

DEFINE_CLASS_LOGGER(ToolSceneItemsPersistency)

ToolSceneItemsPersistency::ToolSceneItemsPersistency(graphics::ToolScene& scene) :
  m_scene(scene)
{}

AbstractElementReaderSP ToolSceneItemsPersistency::CreateElementReader(const XMLCh* const uri, const XMLCh* const localname, const XMLCh* const qname, const XNQ Attributes& attrs)
{
  UNUSED_VAR(uri);
  UNUSED_VAR(qname);
  UNUSED_VAR(attrs);
  try
  {
    const LocalString lname(localname);
    graphics::items::AbstractItemAP item = GRAPHICS_ITEM_REGISTRY.Create(STRING(lname));
    m_scene.AddItem(item.release(), false)->Accept(m_scene.GetCreateItemReaderVisitor());
    return m_scene.GetCreateItemReaderVisitor().GetReader();
  }
  catch (const pattern::UnknownKeyException& ex)
  {
    throw ParseException(ex.what());
  }
  catch (const std::exception& ex)
  {
    throw ParseException(ex.what());
  }
}

void ToolSceneItemsPersistency::Print(const graphics::ToolScene& scene, ElementPrinter& printer)
{
  try
  {
    persistency::AbstractPrintVisitor& v = const_cast<graphics::ToolScene&>(scene).GetPrintItemVisitor();
    v.SetPrinter(&printer);
    graphics::items::ItemList items = scene.Items();
    for (graphics::items::ItemList::const_iterator it = items.begin(); it != items.end(); ++it)
    {
      (*it)->Accept(v);
    }
    v.SetPrinter(nullptr);
  }
  catch (const std::exception& ex)
  {
    throw SerializeException(ex.what());
  }
}

}


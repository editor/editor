/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _STANDARDCONTAINERADAPTER_H_
#define _STANDARDCONTAINERADAPTER_H_

#include "graphics/persistency/elementprinter.h"

namespace persistency
{

template <class ITEM_PERSISTENCY, class CONTAINER>
class StandardContainerAdapter :
    public ITEM_PERSISTENCY
{
public:
  typedef typename CONTAINER::const_iterator ConstIterator;
  typedef typename CONTAINER::value_type ItemType;

public:
  StandardContainerAdapter(CONTAINER& container, ItemType& item) :
    ITEM_PERSISTENCY(*(container.insert(container.end(), item)))
  {}

public:
  static void PrintIterator(ConstIterator it, ElementPrinter& printer)
  {
    ITEM_PERSISTENCY::Print(*it, printer);
  }
};

}

#endif

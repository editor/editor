/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TOOLSCENE_H
#define TOOLSCENE_H

#include <boost/filesystem.hpp>
#include <QMap>
#include <QObject>
#include <QPixmap>

#include <QGraphicsSceneMouseEvent>
#include <QGraphicsScene>

#include "graphics/property.h"

#include "graphics/items/abstractitem.h"
#include "graphics/igettext.h"
#include "graphics/items/itemlist.h"
//#include "graphics/graphicsscene.h"
//#include <graphics/graphics/ms/MeasureModelList.h>
#include "graphics/scenestates/abstractscenestate.h"
#include "graphics/persistency/abstractcreatereadervisitor.h"
#include "graphics/persistency/abstractprintvisitor.h"
#include "utils/log.h"

namespace graphics
{
class ToolScene :
    public QObject ,
    public IProperties
{
  friend class GraphicsScene;

  Q_OBJECT
  DECLARE_CLASS_LOGGER

  public:
    ToolScene(QObject* parent = NULL);

  // Qt ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
private:
  /**
 * DrawBackground
 * draw the background of the scene
 * @param painter
 * @param rect
 */
  void DrawBackground(QPainter& painter, const QRectF& rect);
  /**
   * MousePressEvent
   * handler (called from a Graphics Scene, here: graphicsScene)
   * @param mouseEvent
   * @return
   */
  bool MousePressEvent(QGraphicsSceneMouseEvent& mouseEvent);
  /**
   * @brief MouseMoveEvent
   * handler (called from a Graphics Scene, here: graphicsScene)
   * @param mouseEvent
   * @return
   */
  bool MouseMoveEvent(QGraphicsSceneMouseEvent& mouseEvent);
  /**
   * @brief MouseReleaseEvent
   * handler (called from a Graphics Scene, here: graphicsScene)
   * @param mouseEvent
   * @return
   */
  bool MouseReleaseEvent(QGraphicsSceneMouseEvent& mouseEvent);

  //state handling /////////////////////////////////////////////////////////////////////////////////////////////////////
public:
  /**
   * ChangeState change the tool state
   * @param newState
   */
  void ChangeState(scenestate::AbstractSceneStateAP newState);

  /**
   * GetState get the active state
   * @return state
   */
  const graphics::scenestate::AbstractSceneState& GetState() const;

  /**
   * FinalizeState
   * terminate state from outside
   */
  void FinalizeState();

private slots:
  /**
   * StateFinalized
   * called from inside the library.
   * Emits SigStateFinalized
   */
  void StateFinalized();

private:
  graphics::scenestate::AbstractSceneStateAP m_state;

signals:
  /**
   * SigStateChanged
   * signal emitted, when state is changed
   */
  void SigStateChanged();
  /**
   * SigStateFinalized
   * signal emitted, when state is finalized
   */
  void SigStateFinalized();
  // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  //properties handling /////////////////////////////////////////////////////////////////////////////////////////////////////
public:
  virtual Properties GetSupportedProperties() const;
  /**
   * GetPropertyValue
   * get the value for a given property p.
   * @return QVariant
   * @throw std::logic_error if property isn't supported
   */
  virtual QVariant GetPropertyValue(Property p) const;
  /**
   * SetPropertyValue
   * set the value (value) for a given property p.
   * @param value
   * @throw std::logic_error if property isn't supported
   */
  virtual void SetPropertyValue(Property p, const QVariant& value);

  virtual void RegisterToSigPropertyValueChanged(QObject* receiver, const char* slot);

  /**
   * SetSelectedItemsPropertyValue set property for selected items
   * @param prop property name
   * @param value property value
   */
  void SetSelectedItemsPropertyValue(Property prop, const QVariant& value);

private:
  typedef QMap<Property, QVariant> PropertyMap;
  PropertyMap m_defaultProperties;

signals:
  void SigPropertyValueChanged(Property p, QVariant value);
  // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  //items handling //////////////////////////////////////////////////////////////////////////////////////////////////////
public:
  /**
   * Items get all items on the scene
   * @return item list
   */
  graphics::items::ItemList Items() const;

  /**
   * SelectedItems
   * returns an ItemList with all selected items.
   * @return ItemList
   */
  graphics::items::ItemList SelectedItems() const; //convenience

  /**
   * AddItem
   * add an item to the scene
   * @param item item to be added
   * @param applyDefaults if true, all default values will be copied
   * @return AbstractItem, the added item
   */
  graphics::items::AbstractItem* AddItem(graphics::items::AbstractItem* item, bool applyDefaults = true);

  /**
   * RemoveAllItems remove all graphic items
   */
  void RemoveAllItems();

  /**
   * RemoveSelectedItems remove selected items from the graphics scene
   */
  void RemoveSelectedItems();

  /**
   * DeselectAll unselect all selected items on the graphics scene
   */
  void DeselectAll();

  /**
   * GetTopItem
   * get the topmost item
   * @return  AbstractItem
   */
  graphics::items::AbstractItem* GetTopItem();

  /**
   * MoveSelectedBy
   * move all selected item by the given delta
   *
   * @param delta
   * @return true:  items can be moved,
   *         false: items can not be moved, since the center of some items might not be within the scene rect
   */
  bool MoveSelectedBy(const QPointF& delta);
  // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  //serialization ////////////////////////////////////////////////////////////////////////////////////////////////////////
public:
  void ConfigurePersistency(persistency::CreateReaderVisitorAP rv, persistency::PrintVisitorAP pv);
  persistency::AbstractCreateReaderVisitor& GetCreateItemReaderVisitor();
  persistency::AbstractPrintVisitor& GetPrintItemVisitor();

public:
  /**
   * Load load data from file
   * @param filename
   */
  void Load(const boost::filesystem::path& filename);

  /**
   * Save save data to file
   * @param filename
   */
  void Save(const boost::filesystem::path& filename) const;

  /**
   * IsDirty check if data on scene is changed
   * @return true: data has changed
   */
  bool IsDirty() const;

private slots:
  /**
   * ItemChanged
   * an item has changed. (calls SetDirty)
   */
  void ItemChanged();
  /**
   * StateChanged
   * an item state has changed.
   */
  void StateChanged();

private:
  /**
   * SetDirty
   * set the dirty state of the toolscene
   * @param dirty
   */
  void SetDirty(bool dirty = true);

private:
  bool m_dirty;
  persistency::CreateReaderVisitorAP m_createItemReaderVisitor;
  persistency::PrintVisitorAP m_printItemVisitor;

signals:
  /**
   * SigDirtyChanged
   * signal is emitted if dirty state changes
   * @param dirty
   */
  void SigDirtyChanged(bool dirty);
  // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  //background handling //////////////////////////////////////////////////////////////////////////////////////////////////
public:
  /**
   * SetBackgroundImage set the background image
   * @param img image
   */
  void SetBackgroundImage(const QPixmap& img);

  /**
   * ClearBackgroundImage
   * clear the background image
   */
  void ClearBackgroundImage();

  /**
   * GetSceneRect get the rectangle of the scene
   * @return rect
   */
  QRectF GetSceneRect() const;

  /**
   * SetSceneRect set rect of scene
   * @param rect
   */
  void SetSceneRect(const QRectF& rect);

private:
  QPixmap m_bkgndImage;
  // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  //text input handling //////////////////////////////////////////////////////////////////////////////////////////////////
public:
  /**
   * SetText set the text editor widget.
   * This is used, when a text element is edited, created
   * @param gt object with IGetText interface
   */
  void SetIGetText(graphics::IGetText* gt);

  /**
   * GetIGetText get the text editor widget
   * @return IGetText
   */
  graphics::IGetText* GetIGetText();

private:
  graphics::IGetText* m_textDialog;

  // get access to the graphicsScene //////////////////////////////////////////////////////////////////////////////////////////
public:
  /**
   * GetgraphicsScene get the graphics scene
   * @return graphicsScene
   */
  const QGraphicsScene& GetGraphicsScene() const; //needed by AbstractItem (which is a friend)
  QGraphicsScene& GetGraphicsScene(); //needed by AbstractItem (which is a friend)

private:
  QGraphicsScene* m_graphicsScene;
  // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
};
};


#endif // TOOLSCENE_H

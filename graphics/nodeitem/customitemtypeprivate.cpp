/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include "graphics/nodeitem/customitemtypeprivate.h"

namespace graphics
{
namespace nodeitem
{

#define STRINGIFY(ENUM) case ENUM: return #ENUM;

const char* CustomItemType2CharPrivate(int it)
{
  switch (it)
  {
  STRINGIFY(CI_EDIT_NODE_ITEM_TYPE)
      default: return "";
  }
}

} // namespace nodeitem
} // namespace graphics

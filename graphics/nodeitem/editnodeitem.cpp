/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include "utils/cast.h"
#include "utils/assert.h"
#include "utils/macros.h"
#include "graphics/items/areaitem.h"
#include "graphics/items/ellipseitem.h" //????
#include "graphics/nodeitem/editnodeitem.h"

namespace graphics
{
namespace nodeitem
{

DEFINE_CLASS_LOGGER(EditNodeItem);

EditNodeItem::EditNodeItem(NodeId id, QGraphicsItem* parent) :
  QGraphicsEllipseItem(0, 0, m_radius * 2, m_radius * 2, parent),
  m_id(id)
{
  setBrush(Qt::red);
  setFlag(ItemIgnoresParentOpacity);
}

int EditNodeItem::type() const
{
  return Type;
}

void EditNodeItem::mousePressEvent(QGraphicsSceneMouseEvent* event)
{
  // needed, side effect: Center of button must correspond to tip of mouse pointer
  m_offsetCenter = mapFromScene(event->scenePos()) - rect().center();
}

void EditNodeItem::mouseMoveEvent(QGraphicsSceneMouseEvent* mouseEvent)
{
  // node must stay on screen
  if (!scene()->sceneRect().contains(mouseEvent->scenePos()))
    return;

  // adjust the parent
  const QPointF scenePos = mouseEvent->scenePos();
  const QPointF itemPosition = parentItem()->mapFromScene(scenePos);
  items::AbstractItem* parent = safe_static_cast<items::AbstractItem*>(parentItem());
  parent->MoveEditNodeTo(m_id, itemPosition);
}

}
} // graphics


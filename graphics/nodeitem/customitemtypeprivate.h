/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _CUSTOMITEMTYPEPRIVATE_H_
#define _CUSTOMITEMTYPEPRIVATE_H_

#include <QGraphicsItem>

namespace graphics
{
namespace nodeitem
{

enum CustomItemTypePrivate
{
  _CI_FIRST_ITEM_TYPE = QGraphicsItem::UserType,
  CI_EDIT_NODE_ITEM_TYPE,
  _CI_LAST_PRIVATE
};

const char* CustomItemType2CharPrivate(int it);

}
}

#endif

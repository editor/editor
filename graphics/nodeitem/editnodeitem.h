/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _EDITNODEITEM_H_
#define _EDITNODEITEM_H_

#include <QGraphicsSceneMouseEvent>
#include "utils/log.h"
#include "graphics/qpointlist.h"
#include "graphics/nodeitem/customitemtypeprivate.h"

namespace graphics
{
namespace nodeitem
{

typedef QPointFList::size_type NodeId;

/**
 * The EditNodeItem class
 * the class is responsible for painting a red point at the given position
 */
class EditNodeItem :
  public QGraphicsEllipseItem
{
  DECLARE_CLASS_LOGGER

public:
  EditNodeItem(NodeId id, QGraphicsItem* parent);

public:
  /**
     * type return the type of the class
     * @return type
     */
  virtual int type() const;

protected:
  /**
     * mousePressEvent
     * Event handler to receive mouse press events for this item
     * @param event
     */
  virtual void mousePressEvent(QGraphicsSceneMouseEvent* event);
  /**
     * mouseMoveEvent
     * Event handler to receive mouse move events for this item
     * @param mouseEvent
     */
  virtual void mouseMoveEvent(QGraphicsSceneMouseEvent* mouseEvent);

public:
  static const int Type = CI_EDIT_NODE_ITEM_TYPE;

protected:
  NodeId m_id;
  constexpr static qreal m_radius {5};

private:
  QPointF m_offsetCenter;
};

}
}
#endif

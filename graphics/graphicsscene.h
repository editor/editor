/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _GRAPHICSSCENE_H_
#define _GRAPHICSSCENE_H_

#include <QGraphicsScene>

namespace graphics
{

class ToolScene;

/**
 * The graphicsScene class
 * the class handles the events coming fron QGraphicsScene and redirects them to ToolScene
 */
class GraphicsScene :
    public QGraphicsScene
{
  Q_OBJECT

public:
  /**
   * graphicsScene
   * Constructor
   * @param ts Toolscene. All incoming events are forwarded to the Toolscene.
   */
  GraphicsScene(ToolScene* ts);

protected:
  /**
   * drawBackground
   * Draws the background of the scene using painter, before any items and the foreground are drawn.
   * All painting is done in scene coordinates. The rect parameter is the exposed rectangle.
   * @param painter
   * @param rect
   */
  virtual void drawBackground(QPainter* painter, const QRectF& rect);
  /**
   * mousePressEvent
   * Event handler to receive mouse press events on the QGraphicsScene
   * @param mouseEvent
   */
  virtual void mousePressEvent(QGraphicsSceneMouseEvent* mouseEvent);
  /**
   * mouseMoveEvent
   * Event handler to receive mouse move events on the QGraphicsScene
   * @param mouseEvent
   */
  virtual void mouseMoveEvent(QGraphicsSceneMouseEvent* mouseEvent);
  /**
   * mouseReleaseEvent
   * Event handler to receive mouse release events on the QGraphicsScene
   * @param mouseEvent
   */
  virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent* mouseEvent);

private:
  ToolScene* const m_ts;
};
}
#endif

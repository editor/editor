/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <QVector2D>
#include "graphics/scenestates/newareascenestate.h"

namespace graphics
{
namespace scenestate
{

DEFINE_CLASS_LOGGER(NewAreaSceneState);

NewAreaSceneState::NewAreaSceneState(QObject* parent) :
  AbstractNewToolSceneState(parent)
{}

void NewAreaSceneState::FinalizeState(ToolScene& scene)
{
  UNUSED_VAR(scene);
  if (m_item)
  {
    if (m_item->GetPolygon().size() > 2)
    {
      m_item->RemoveNode(m_item->GetPolygon().size() - 1);
      m_item->SetClosed();
      m_item.release();
      emit SigStateFinalized();
    }
    else
    {
      m_item.reset();
    }
  }
}

void NewAreaSceneState::ExitState(ToolScene& scene)
{
  m_item.reset();
  AbstractNewToolSceneState::ExitState(scene);
}

bool NewAreaSceneState::MousePressEvent(ToolScene& scene, QGraphicsSceneMouseEvent& mouseEvent)
{
  if (!m_item && OkToCreateItemAt(scene, mouseEvent))
  {
    scene.DeselectAll();

    m_item.reset(new graphics::items::AreaItem());
    scene.AddItem(m_item.get());
    m_item->GotoFirstSelectedState();
    m_item->AddNode(mouseEvent.scenePos());
  }
  if (m_item)
  {
    if (m_item->GetPolygon().size() >= 3 && (QVector2D(mouseEvent.scenePos()) - QVector2D(m_item->GetPolygon()[0])).lengthSquared() <= (5*5))
    {
      FinalizeState(scene);
    }
    else
    {
      m_item->AddNode(mouseEvent.scenePos());
    }
    return true;
  }
  return false;
}

bool NewAreaSceneState::MouseMoveEvent(ToolScene& scene, QGraphicsSceneMouseEvent& mouseEvent)
{
  UNUSED_VAR(scene);
  if (!m_item.isNull() && m_item->GetPolygon().size() >= 2)
  {
    m_item->SetNodePosition(m_item->GetPolygon().size() - 1, mouseEvent.scenePos());
    return true;
  }
  return false;
}

bool NewAreaSceneState::MouseReleaseEvent(ToolScene& scene, QGraphicsSceneMouseEvent& mouseEvent)
{
  if (m_item.get() && m_item->IsClosed())
  {
    m_item.release();	// done with this, responsibility for object is in scene
    emit SigStateFinalized();
  }
  return AbstractNewToolSceneState::MouseReleaseEvent(scene, mouseEvent);
}

const char* NewAreaSceneState::GetStateName() const
{
  return "NewAreaSceneState";
}

}
}

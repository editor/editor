/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _NEWELLIPSESCENESTATE_H_
#define _NEWELLIPSESCENESTATE_H_

#include "graphics/scenestates/abstractnewtoolscenestate.h"

namespace graphics
{
namespace scenestate
{
/**
 * The NewEllipseSceneState class
 * class for ellipse item creation
 */
class NewEllipseSceneState :
  public AbstractNewToolSceneState
{
  Q_OBJECT

public:
  NewEllipseSceneState(QObject* parent = NULL);

public:
  /*! @copydoc AbstractSceneState::GetStateName() */
  virtual const char* GetStateName() const;
  /*! @copydoc AbstractSceneState::MousePressEvent() */
  virtual bool MousePressEvent(ToolScene& scene, QGraphicsSceneMouseEvent& mouseEvent);
};

}
}
#endif

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _NEWPENSCENESTATE_H_
#define _NEWPENSCENESTATE_H_

#include "graphics/items/penitem.h"
#include "graphics/toolscene.h"
#include "graphics/scenestates/abstractnewtoolscenestate.h"

namespace graphics
{
namespace scenestate
{
/**
 * The NewPenSceneState class
 * class for creation of a pen item
 */
class NewPenSceneState :
  public AbstractNewToolSceneState
{
  Q_OBJECT

public:
  NewPenSceneState(QPolygonF::size_type maxPoints = 0, QObject* parent = NULL);

public:
  /*! @copydoc AbstractSceneState::GetStateName() */
  virtual const char* GetStateName() const;
  /*! @copydoc AbstractSceneState::ExitState() */
  virtual void ExitState(ToolScene& scene);
  /*! @copydoc AbstractSceneState::MousePressEvent() */
  virtual bool MousePressEvent(ToolScene& scene, QGraphicsSceneMouseEvent& mouseEvent);
  /*! @copydoc AbstractSceneState::MouseMoveEvent() */
  virtual bool MouseMoveEvent(ToolScene& scene, QGraphicsSceneMouseEvent& mouseEvent);
  /*! @copydoc AbstractSceneState::MouseReleaseEvent() */
  virtual bool MouseReleaseEvent(ToolScene& scene, QGraphicsSceneMouseEvent& mouseEvent);

private:
  void AddPoint(const QPointF& pt);

private:
  const QPolygonF::size_type m_maxPoints;
  QPointF m_lastPoint;
  graphics::items::PenItemAP m_penItem;
};

}
}
#endif


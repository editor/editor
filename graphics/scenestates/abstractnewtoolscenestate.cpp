/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <QGraphicsItem>
#include "graphics/items/abstractitem.h"
#include "graphics/scenestates/abstractnewtoolscenestate.h"
#include "graphics/nodeitem/editnodeitem.h"

namespace graphics
{
namespace scenestate
{

AbstractNewToolSceneState::AbstractNewToolSceneState(QObject* parent) :
  AbstractSceneState(parent)
{}

bool AbstractNewToolSceneState::OkToCreateItemAt(const ToolScene& scene, const QGraphicsSceneMouseEvent& mouseEvent)
{
  // helper function that determines if a new item should be created
  // called after a mouse press when a "New" tool is active
  // returns true if a new item should be created
  if (Qt::LeftButton == mouseEvent.button())
  {
    const QGraphicsItem* grItem = scene.GetGraphicsScene().itemAt(mouseEvent.scenePos(), QTransform());
    if (grItem)
    {
      if (nodeitem::CI_EDIT_NODE_ITEM_TYPE == grItem->type())
      {
        return false;	// click to be handled by node item
      }
    }
    return true;
  }
  return false;
}

}
} // graphics

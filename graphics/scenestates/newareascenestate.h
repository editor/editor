/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _NEWAREASCENESTATE_H_
#define _NEWAREASCENESTATE_H_

#include "utils/cast.h"
#include "graphics/items/areaitem.h"
#include "graphics/toolscene.h"
#include "graphics/scenestates/abstractscenestate.h"
#include "graphics/scenestates/abstractnewtoolscenestate.h"

namespace graphics
{
namespace scenestate
{

/**
 * The NewAreaSceneState class
 * state for new area
 */
class NewAreaSceneState :
  public AbstractNewToolSceneState
{
  Q_OBJECT
  DECLARE_CLASS_LOGGER

  public:
    NewAreaSceneState(QObject* parent = NULL);

public:
  /*! @copydoc AbstractSceneState::GetStateName() */
  virtual const char* GetStateName() const;
  /*! @copydoc AbstractSceneState::FinalizeState() */
  virtual void FinalizeState(ToolScene& scene);
  /*! @copydoc AbstractSceneState::ExitState() */
  virtual void ExitState(ToolScene& scene);
  /*! @copydoc AbstractSceneState::MousePressEvent() */
  virtual bool MousePressEvent(ToolScene& scene, QGraphicsSceneMouseEvent& mouseEvent);
  /*! @copydoc AbstractSceneState::MouseMoveEvent() */
  virtual bool MouseMoveEvent(ToolScene& scene, QGraphicsSceneMouseEvent& mouseEvent);
  /*! @copydoc AbstractSceneState::MouseReleaseEvent() */
  virtual bool MouseReleaseEvent(ToolScene& scene, QGraphicsSceneMouseEvent& mouseEvent);

private:
  graphics::items::AreaItemAP m_item;
};

}
}
#endif

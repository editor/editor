/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _NEWPOLYLINESCENESTATE_H_
#define _NEWPOLYLINESCENESTATE_H_

#include "graphics/items/polylineitem.h"
#include "graphics/scenestates/abstractnewtoolscenestate.h"

namespace graphics
{
namespace scenestate
{

/**
 * The NewPolylineSceneState class
 * class for creation of a polyline item
 */
class NewPolylineSceneState :
  public AbstractNewToolSceneState
{
  Q_OBJECT

public:
  NewPolylineSceneState(QPolygonF::size_type maxNodes = 0, QObject* parent = NULL);

public:
  /**
   * GetMaxNodes
   * get the max numbers of nodes on the polygon
   * nodes 0: no limit
   * nodes 2: a simple line
   * @return number of nodes
   */
  QPolygonF::size_type GetMaxNodes() const;

public:
  /*! @copydoc AbstractSceneState::GetStateName() */
  virtual const char* GetStateName() const;
  /*! @copydoc AbstractSceneState::FinalizeState() */
  virtual void FinalizeState(ToolScene& scene);
  /*! @copydoc AbstractSceneState::ExitState() */
  virtual void ExitState(ToolScene& scene);
  /*! @copydoc AbstractSceneState::MousePressEvent() */
  virtual bool MousePressEvent(ToolScene& scene, QGraphicsSceneMouseEvent& mouseEvent);
  /*! @copydoc AbstractSceneState::MouseMoveEvent() */
  virtual bool MouseMoveEvent(ToolScene& scene, QGraphicsSceneMouseEvent& mouseEvent);

protected:
  const QPolygonF::size_type m_maxNodes;
  graphics::items::PolylineItemAP m_item;
};

}
}

#endif


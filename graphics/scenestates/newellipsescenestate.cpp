/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "graphics/toolscene.h"
#include "graphics/items/ellipseitem.h"
#include "graphics/scenestates/newellipsescenestate.h"

namespace graphics
{
namespace scenestate
{

NewEllipseSceneState::NewEllipseSceneState(QObject* parent) :
  AbstractNewToolSceneState(parent)
{}

bool NewEllipseSceneState::MousePressEvent(ToolScene& scene, QGraphicsSceneMouseEvent& mouseEvent)
{
  if (OkToCreateItemAt(scene, mouseEvent))
  {
    scene.DeselectAll();

    graphics::items::EllipseItem* ellip = new graphics::items::EllipseItem();
    scene.AddItem(ellip);
    ellip->GotoFirstSelectedState();
    ellip->setPos(mouseEvent.scenePos());
    ellip->SetSize(QSizeF(100, 100));
    emit SigStateFinalized();

    return true;
  }
  return false;
}

const char* NewEllipseSceneState::GetStateName() const
{
  return "NewEllipseSceneState";
}

}
}


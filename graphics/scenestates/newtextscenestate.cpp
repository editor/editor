/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "graphics/toolscene.h"
#include "graphics/items/customitemtype.h"
#include "graphics/scenestates/newtextscenestate.h"
#include "utils/macros.h"
#include "utils/cast.h"

namespace graphics
{
namespace scenestate
{

NewTextSceneState::NewTextSceneState(QObject* parent) :
  AbstractNewToolSceneState(parent)
{}

bool NewTextSceneState::MousePressEvent(ToolScene& scene, QGraphicsSceneMouseEvent& mouseEvent)
{
  m_edItem.release();

  // A mouse click on the item will edit on release unless the item is moved
  if (Qt::LeftButton == mouseEvent.button())
  {
    QGraphicsItem* grItem = scene.GetGraphicsScene().itemAt(mouseEvent.scenePos(),QTransform());

    if (grItem && (items::CI_TEXT_ITEM_TYPE == grItem->type()))
    {
      items::TextItem* textItem = safe_static_cast<items::TextItem*>(grItem);
      m_edItem.reset(textItem);
    }
  }
  return true;
}

bool NewTextSceneState::MouseMoveEvent(ToolScene& scene, QGraphicsSceneMouseEvent& mouseEvent)
{
  UNUSED_VAR(scene);
  UNUSED_VAR(mouseEvent);
  return false;
}

bool NewTextSceneState::MouseReleaseEvent(ToolScene& scene, QGraphicsSceneMouseEvent& mouseEvent)
{
  if (m_edItem)
  {
    scene.DeselectAll();
    m_edItem->GotoFirstSelectedState();

    QString txt = m_edItem->GetPropertyValue(PROP_TEXT).value<QString>();
    if (NULL == scene.GetIGetText())
      throw std::logic_error("GetText method not defined");
    if (scene.GetIGetText()->GetText(txt))
    {
      // OK was clicked
      if (txt.isEmpty())
      {
        m_edItem.reset();
      }
      else
      {
        if (m_edItem)
        {
          m_edItem->SetPropertyValue(PROP_TEXT, txt);
        }
      }
      m_edItem.release(); // responsibility in graphicsScene
      emit SigStateFinalized();
      return true;
    }
  }


  // Check if no item
  if (OkToCreateItemAt(scene, mouseEvent))
  {
    // prompt user for text string
    QString txt;
    if (NULL == scene.GetIGetText())
    {
      throw std::logic_error("GetText method not defined");
    }
    if (scene.GetIGetText()->GetText(txt) && !txt.isEmpty())
    {
      scene.DeselectAll();

      // responsibility of nextText in graphicsscene
      graphics::items::TextItem* newText = new graphics::items::TextItem();
      scene.AddItem(newText);
      newText->GotoFirstSelectedState();
      newText->setPos(mouseEvent.scenePos());
      newText->SetPropertyValue(PROP_TEXT, txt);
      emit SigStateFinalized();
      return true;
    }
  }
  return false;
}

const char* NewTextSceneState::GetStateName() const
{
  return "NewTextSceneState";
}

}
}

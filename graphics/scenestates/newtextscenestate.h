/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _NEWTEXTSCENESTATE_H_
#define _NEWTEXTSCENESTATE_H_

#include <QPointer>
#include "graphics/igettext.h"
#include "graphics/items/textitem.h"
#include "graphics/scenestates/abstractnewtoolscenestate.h"

namespace graphics
{
namespace scenestate
{
/**
 * The NewTextSceneState class
 * class for creating a new text item
 */
class NewTextSceneState :
  public AbstractNewToolSceneState
{
  Q_OBJECT

public:
  NewTextSceneState(QObject* parent = NULL);

public:
  /*! @copydoc AbstractSceneState::GetStateName() */
  virtual const char* GetStateName() const;
  /*! @copydoc AbstractSceneState::MousePressEvent() */
  virtual bool MousePressEvent(ToolScene& scene, QGraphicsSceneMouseEvent& mouseEvent);
  /*! @copydoc AbstractSceneState::MouseMoveEvent() */
  virtual bool MouseMoveEvent(ToolScene& scene, QGraphicsSceneMouseEvent& mouseEvent);
  /*! @copydoc AbstractSceneState::MouseReleaseEvent() */
  virtual bool MouseReleaseEvent(ToolScene& scene, QGraphicsSceneMouseEvent& mouseEvent);

private:
  graphics::items::TextItemAP m_edItem; // item to edit on mouse release
};

}
}
#endif


/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "graphics/items/abstractitem.h"
#include "graphics/scenestates/abstractscenestate.h"
#include "graphics/toolscene.h"

namespace graphics
{
namespace scenestate
{

AbstractSceneState::AbstractSceneState(QObject* parent) :
  QObject(parent)
{}

AbstractSceneState::~AbstractSceneState()
{}

void AbstractSceneState::FinalizeState(ToolScene& scene)
{
  UNUSED_VAR(scene);
}

void AbstractSceneState::EnterState(ToolScene& scene)
{
  UNUSED_VAR(scene);
}

void AbstractSceneState::ExitState(ToolScene& scene)
{
  UNUSED_VAR(scene);
}

bool AbstractSceneState::MousePressEvent(ToolScene& scene, QGraphicsSceneMouseEvent& mouseEvent)
{
  UNUSED_VAR(scene);
  UNUSED_VAR(mouseEvent);
  return false;
}

bool AbstractSceneState::MouseMoveEvent(ToolScene& scene, QGraphicsSceneMouseEvent& mouseEvent)
{
  UNUSED_VAR(scene);
  UNUSED_VAR(mouseEvent);
  return false;
}

bool AbstractSceneState::MouseReleaseEvent(ToolScene& scene, QGraphicsSceneMouseEvent& mouseEvent)
{
  UNUSED_VAR(scene);
  UNUSED_VAR(mouseEvent);
  return false;
}

}
}

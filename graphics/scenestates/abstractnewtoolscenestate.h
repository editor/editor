/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _ABSTRACTNEWTOOLSCENESTATE_H_
#define _ABSTRACTNEWTOOLSCENESTATE_H_

#include "graphics/toolscene.h"
#include "graphics/scenestates/abstractscenestate.h"

class QGraphicsSceneMouseEvent;

namespace graphics
{
namespace scenestate
{

/**
 * The AbstractNewToolSceneState class
 * Abstract Class for new tool scene states
 */
class AbstractNewToolSceneState :
  public AbstractSceneState
{
  Q_OBJECT

protected:
  AbstractNewToolSceneState(QObject* parent = NULL);

protected:
  /**
   * OkToCreateItemAt
   * check if creation an item is permitted at mouse position
   * @param scene ToolScene
   * @param mouseEvent mouseEvent used for Evaluation of position
   * @return  true: permitted
   */
  static bool OkToCreateItemAt(const ToolScene& scene, const QGraphicsSceneMouseEvent& mouseEvent);
};

}   // scenestate
}   // graphics

#endif

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "graphics/toolscene.h"
#include "graphics/scenestates/newpolylinescenestate.h"

namespace graphics
{
namespace scenestate
{

NewPolylineSceneState::NewPolylineSceneState(QPolygonF::size_type maxNodes, QObject* parent) :
  AbstractNewToolSceneState(parent),
  m_maxNodes(maxNodes)
{}

void NewPolylineSceneState::ExitState(ToolScene& scene)
{
  m_item.reset();
  AbstractNewToolSceneState::ExitState(scene);
}

const char* NewPolylineSceneState::GetStateName() const
{
  return "NewPolylineSceneState";
}

QPolygonF::size_type NewPolylineSceneState::GetMaxNodes() const
{
  return m_maxNodes;
}

void NewPolylineSceneState::FinalizeState(ToolScene& scene)
{
  UNUSED_VAR(scene);
  if (!m_item.isNull())
  {
    if (m_item->GetPolygon().size() > 2)
    {
      m_item->RemoveNode(m_item->GetPolygon().size() - 1);
      m_item.release();
      emit SigStateFinalized();
    }
    else
    {
      m_item.reset();
    }
  }
}

bool NewPolylineSceneState::MousePressEvent(ToolScene& scene, QGraphicsSceneMouseEvent& mouseEvent)
{
  if (!m_item.isNull() && m_item->GetPolygon().size() == m_maxNodes)
  {
    m_item.release(); // responsibility in graphicsscene
    emit SigStateFinalized();
    return true;
  }
  else
  {
    if (m_item.isNull() && OkToCreateItemAt(scene, mouseEvent))
    {
      scene.DeselectAll();

      m_item.reset(new graphics::items::PolylineItem());
      scene.AddItem(m_item.get());
      m_item->GotoFirstSelectedState();
      m_item->AddNode(mouseEvent.scenePos());
    }
    if (!m_item.isNull())
    {
      m_item->AddNode(mouseEvent.scenePos());
      return true;
    }
  }
  return false;
}

bool NewPolylineSceneState::MouseMoveEvent(ToolScene& scene, QGraphicsSceneMouseEvent& mouseEvent)
{
  UNUSED_VAR(scene)
  if (!m_item.isNull() && m_item->GetPolygon().size() >= 2)
  {
    m_item->SetNodePosition(m_item->GetPolygon().size() - 1, mouseEvent.scenePos());
    return true;
  }
  return false;
}

}
}



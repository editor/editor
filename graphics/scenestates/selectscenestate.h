/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _SELECTSCENESTATE_H_
#define _SELECTSCENESTATE_H_

#include <QPointer>
#include "graphics/graphicsscene.h"
#include "graphics/items/abstractitem.h"
#include "graphics/scenestates/abstractscenestate.h"

namespace graphics
{
namespace scenestate
{
/**
 * The SelectSceneState class
 * class for item selection
 */
class SelectSceneState :
    public AbstractSceneState
{
  Q_OBJECT
  DECLARE_CLASS_LOGGER

  public:
    SelectSceneState(QObject* parent = NULL);

public:
  /*! @copydoc AbstractSceneState::GetStateName() */
  virtual const char* GetStateName() const;
  /*! @copydoc AbstractSceneState::EnterState() */
  virtual void EnterState(ToolScene& scene);
  /*! @copydoc AbstractSceneState::ExitState() */
  virtual void ExitState(ToolScene& scene);
  /*! @copydoc AbstractSceneState::MousePressEvent() */
  virtual bool MousePressEvent(ToolScene& scene, QGraphicsSceneMouseEvent& mouseEvent);
  /*! @copydoc AbstractSceneState::MouseMoveEvent() */
  virtual bool MouseMoveEvent(ToolScene& scene, QGraphicsSceneMouseEvent& mouseEvent);
  /*! @copydoc AbstractSceneState::MouseReleaseEvent() */
  virtual bool MouseReleaseEvent(ToolScene& scene, QGraphicsSceneMouseEvent& mouseEvent);

private:
  bool m_lastMousePosIsValid;
  QPointF m_lastMousePos;
  QPointer<items::AbstractItem> m_lastPressedItem;
};

}
}
#endif


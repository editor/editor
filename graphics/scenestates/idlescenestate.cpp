/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "graphics/scenestates/idlescenestate.h"

namespace graphics
{
namespace scenestate
{

IdleSceneState::IdleSceneState(QObject* parent) :
  AbstractSceneState(parent)
{}

void IdleSceneState::EnterState(ToolScene& scene)
{
  AbstractSceneState::EnterState(scene);
  scene.DeselectAll();
}

const char* IdleSceneState::GetStateName() const
{
  return "IdleSceneState";
}

}
}

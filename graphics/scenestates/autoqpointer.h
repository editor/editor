/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _AUTOQPOINTER_H_
#define _AUTOQPOINTER_H_

#include <QPointer>
#include <QObject>

namespace graphics
{
namespace scenestate
{

/**
 *  A wrapper class to provide AutoQPointer with reference semantics.
 *  For example, an AutoQPointer can be assigned (or constructed from)
 *  the result of a function which returns an AutoQPointer by value.
 *
 *  All the AutoQPointer stuff should happen behind the scenes.
 */
template<typename _Tp1>
struct AutoQPointer_ref
{
  _Tp1* _M_ptr;

  explicit
  AutoQPointer_ref(_Tp1* __p):
    _M_ptr(__p)
  {}
};

template <class _Tp>
class AutoQPointer
{
public:

  /// The pointed-to type.
  typedef _Tp element_type;

  /**
   * AutoQPointer  An %AutoQPointer is usually constructed from a raw pointer.
   * @param p A pointer (defaults to NULL).
   */
  explicit AutoQPointer(element_type* p=0) throw():
    _objectQPtr(p)
  {}

  /**
   * AutoQPointer An %AutoQPointer can be constructed from another %AutoQPointer.
   * @param __a Another %AutoQPointer of the same type.
   *
   * This object now owns the object previously owned by __a.
   */
  AutoQPointer(AutoQPointer& __a) throw():
    _objectQPtr(__a.release())
  {}

  /**
   * An %AutoQPointer can be constructed from another %AutoQPointer.
   *  @param  a  Another %AutoQPointer of a different but related type.
   *
   *  A pointer-to-Tp1 must be convertible to a
   *  pointer-to-Tp/element_type.
   *
   *  This object now owns the object previously owned by  __a
   */
  template<typename _Tp1>
  AutoQPointer(AutoQPointer<_Tp1>& __a) throw():
    _objectQPtr(__a.release())
  {}

  /**
   *  When the %AutoQPointer goes out of scope, the object it owns is
   *  deleted.  If it no longer owns anything (i.e., get() is
   *  NULL), then this has no effect.
   *
   *  The C++ standard says there is supposed to be an empty throw
   *  specification here, but omitting it is standard conforming.  Its
   *  presence can be detected only if _Tp::~_Tp() throws, but this is
   *  prohibited.  [17.4.3.6]/2
   */
  ~AutoQPointer()
  {
    delete _objectQPtr;
  }

  /**
   *  @brief  %AutoQPointer assignment operator.
   *  @param  a  Another %AutoQPointer of the same type.
   *
   *  This object now owns the object previously owned by __a,
   *  which has given up ownership.  The object that this one
   *  used to own and track has been deleted.
   */
  AutoQPointer& operator=(AutoQPointer& __a) throw()
  {
    reset(__a.release());
    return *this;
  }

  /**
   *  %AutoQPointer assignment operator.
   *  @param  a  Another %AutoQPointer of a different but related type.
   *
   *  A pointer-to-Tp1 must be convertible to a pointer-to-Tp/element_type.
   *
   *  This object now owns the object previously owned by __a,
   *  which has given up ownership.  The object that this one
   *  used to own and track has been deleted.
   */
  template<typename _Tp1>
  AutoQPointer& operator=(AutoQPointer<_Tp1>& __a) throw()
  {
    reset(__a.release());
    return *this;
  }

  /**
   *  Bypassing the smart pointer.
   *  @return  The raw pointer being managed.
   *
   *  You can get a copy of the pointer that this object owns, for
   *  situations such as passing to a function which only accepts
   *  a raw pointer.
   *
   *  @note  This %AutoQPointer still owns the memory.
   */
  element_type * data() const
  {
    return static_cast<element_type*>(_objectQPtr.data());
  }

  /**
   *  Bypassing the smart pointer.
   *  @return  The raw pointer being managed.
   *
   *  You can get a copy of the pointer that this object owns, for
   *  situations such as passing to a function which only accepts
   *  a raw pointer.
   *
   *  @note  This %AutoQPointer still owns the memory.
   */
  element_type * get() const
  {
    return data();
  }

  /**
   *  Forcibly deletes the managed object.
   *  @param  __p  A pointer (defaults to NULL).
   *
   *  This object now owns the object pointed to by __p.
   *  The previous object has been deleted.
   */
  void reset (element_type* __p=0) throw()
  {
    if (_objectQPtr != __p)
    {
      delete _objectQPtr;
      _objectQPtr = __p;
    }
  }

  /**
   * Bypassing the smart pointer.
   *  @return  The raw pointer being managed.
   *
   *  You can get a copy of the pointer that this object owns, for
   *  situations such as passing to a function which only accepts
   *  a raw pointer.
   *
   *  @note  This %AutoQPointer no longer owns the memory.
   *  When this object goes out of scope, nothing will happen.
   */
  element_type* release() throw()
  {
    element_type* __tmp = data();
    _objectQPtr = NULL;
    return __tmp;
  }

  /**
   * isNull: check if object is null
   * @return true: object is null
   */
  bool isNull() const
  {
    return _objectQPtr.isNull();
  }

  /**
   *  @brief  Smart pointer dereferencing.
   *
   *  This returns the pointer itself, which the language then will
   *  automatically cause to be dereferenced.
   */
  element_type * operator->() const
  {
    return data();
  }

  /**
   *  @brief  Smart pointer dereferencing.
   *
   *  If this %AutoQPointer no longer owns anything, then this
   *  operation will crash.  (For a smart pointer, <em>no longer owns
   *  anything</em> is the same as being a null pointer, and you know
   *  what happens when you dereference one of those...)
   */
  element_type& operator*() const
  {
    return *data();
  }

#if 0
  /**
   *  Smart pointer dereferencing.
   *
   *  This returns the pointer itself, which the language then will
   *  automatically cause to be dereferenced.
   */
  operator element_type*() const
  {
    return get();
  }
#endif

  /**
   * operator bool: check if object exists
   * @return true: object exists
   */
  operator bool() const
  {
    return !isNull();
  }

  /**
   *  @brief  Automatic conversions
   *
   *  These operations convert an %auto_ptr into and from an auto_ptr_ref
   *  automatically as needed.  This allows constructs such as
   *  @code
   *    AutoQPointer<Derived>  func_returning_AutoQPointer(.....);
   *    ...
   *    AutoQPointer<Base> ptr = func_returning_AutoQPointer(.....);
   *  @endcode
   */
  AutoQPointer(AutoQPointer_ref<element_type> __ref) throw()
    : _objectQPtr(__ref._M_ptr)
  {}

  AutoQPointer& operator=(AutoQPointer_ref<element_type> __ref) throw()
  {
    if (__ref._M_ptr != this->get())
    {
      delete _objectQPtr;
      _objectQPtr = __ref._M_ptr;
    }
    return *this;
  }

  template<typename _Tp1>
  operator AutoQPointer_ref<_Tp1>() throw()
  {
    return AutoQPointer_ref<_Tp1>(this->release());
  }

  template<typename _Tp1>
  operator AutoQPointer<_Tp1>() throw()
  {
    return AutoQPointer<_Tp1>(this->release());
  }

private:
  QPointer<QObject> _objectQPtr;
};

}
}

#endif

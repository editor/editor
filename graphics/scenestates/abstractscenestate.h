/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _ABSTRACTSCENESTATE_H_
#define _ABSTRACTSCENESTATE_H_

#include <memory>
#include <QObject>
#include <QGraphicsSceneMouseEvent>

namespace graphics
{
class ToolScene;

namespace scenestate
{

/**
 * The AbstractSceneState class
 * Abstract class for scenes
 */
class AbstractSceneState :
  public QObject
{
  Q_OBJECT

protected:
  AbstractSceneState(QObject* parent = NULL);

public:
  virtual ~AbstractSceneState();

public:
  /**
   * GetStateName
   * pure virtual, get the state name
   * @return char*
   */
  virtual const char* GetStateName() const = 0;
  /**
   * EnterState
   * called, when entering a Scene state.
   * @param scene
   */
  virtual void EnterState(ToolScene& scene);
  /**
   * FinalizeState
   * called when a scene is finalized
   * @param scene
   */
  virtual void FinalizeState(ToolScene& scene);
  /**
   * ExitState
   * called when exiting a scene state.
   * @param scene
   */
  virtual void ExitState(ToolScene& scene);
  /**
   * MousePressEvent
   * called when a mouse press event occurs on the scene
   * @param scene
   * @param mouseEvent
   * @return
   */
  virtual bool MousePressEvent(ToolScene& scene, QGraphicsSceneMouseEvent& mouseEvent);
  /**
   * MouseMoveEvent
   * called when a mouse press event occurs on the scene
   * @param scene
   * @param mouseEvent
   * @return
   */
  virtual bool MouseMoveEvent(ToolScene& scene, QGraphicsSceneMouseEvent& mouseEvent);
  /**
   * MouseReleaseEvent
   * called when a mousse move event occurs on the scene
   * @param scene
   * @param mouseEvent
   * @return
   */
  virtual bool MouseReleaseEvent(ToolScene& scene, QGraphicsSceneMouseEvent& mouseEvent);

signals:
  /**
   * SigStateFinalized
   * emitted when a state can be finalized.
   */
  void SigStateFinalized();
};

typedef std::unique_ptr<AbstractSceneState> AbstractSceneStateAP;

}
}

#endif

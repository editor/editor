/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "graphics/scenestates/newpenscenestate.h"

namespace graphics
{
namespace scenestate
{

NewPenSceneState::NewPenSceneState(QPolygonF::size_type maxPoints, QObject* parent) :
  AbstractNewToolSceneState(parent),
  m_maxPoints(maxPoints),
  m_lastPoint(0, 0)
{}

void NewPenSceneState::ExitState(ToolScene& scene)
{
  m_penItem.reset();
  AbstractNewToolSceneState::ExitState(scene);
}

bool NewPenSceneState::MousePressEvent(ToolScene& scene, QGraphicsSceneMouseEvent& mouseEvent)
{
  // Note that pen items may have a stretched shape and the scene does not
  // consider a mouse hit on the expanded area to be a hit on the item.
  // This is good because it allows new pen items to be started adjacent
  // to an item that was just created.
  if (OkToCreateItemAt(scene, mouseEvent))
  {
    scene.DeselectAll();

    m_penItem.reset(new graphics::items::PenItem());
    scene.AddItem(m_penItem.get());
    m_penItem->GotoFirstSelectedState();
    AddPoint(mouseEvent.scenePos());

    m_lastPoint = mouseEvent.scenePos();
    return true;
  }
  return false;
}

bool NewPenSceneState::MouseMoveEvent(ToolScene& scene, QGraphicsSceneMouseEvent& mouseEvent)
{
  if (m_penItem.get() && scene.GetGraphicsScene().sceneRect().contains(mouseEvent.scenePos()))
  {
    // 3 pixel minumum between points
    QPointF distance = mouseEvent.scenePos() - m_lastPoint;
    if (distance.x()*distance.x() + distance.y()*distance.y() >= 9)
    {
      m_lastPoint = mouseEvent.scenePos();
      AddPoint(m_lastPoint);
    }
    return true;
  }
  return false;
}

bool NewPenSceneState::MouseReleaseEvent(ToolScene& scene, QGraphicsSceneMouseEvent& mouseEvent)
{
  UNUSED_VAR(scene);
  UNUSED_VAR(mouseEvent);
  if (m_penItem.get())
  {
    if (!m_penItem->GetPolyline().empty())
    {
      m_penItem->update();
      m_penItem.release(); // responsibility in graphics scene
      emit SigStateFinalized();
    }
    else
    {
      // delete single point
      m_penItem.reset();
    }
    return true;
  }
  return false;
}

const char* NewPenSceneState::GetStateName() const
{
  return "NewPenSceneState";
}

void NewPenSceneState::AddPoint(const QPointF& pt)
{
  ASSERT(m_penItem);
  if (m_maxPoints > 0)
  {
    while (m_maxPoints <= m_penItem->GetPolyline().size())
    {
      m_penItem->RemovePoint(0);
    }
  }
  m_penItem->AddPoint(pt);
}

}
}

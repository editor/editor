/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _IDLESCENESTATE_H_
#define _IDLESCENESTATE_H_

#include "graphics/toolscene.h"
#include "abstractscenestate.h"

namespace graphics
{
namespace scenestate
{

/**
 * The IdleSceneState class
 * the idle state
 */
class IdleSceneState :
  public AbstractSceneState
{
  Q_OBJECT

public:
  IdleSceneState(QObject* parent = NULL);

public:
  /*! @copydoc AbstractSceneState::GetStateName() */
  virtual const char* GetStateName() const;
  /*! @copydoc AbstractSceneState::EnterState() */
  virtual void EnterState(ToolScene& scene);
};

}
}
#endif


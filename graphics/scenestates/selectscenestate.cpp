/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "graphics/toolscene.h"
#include "graphics/scenestates/selectscenestate.h"

namespace graphics
{
namespace scenestate
{

DEFINE_CLASS_LOGGER(SelectSceneState)

SelectSceneState::SelectSceneState(QObject* parent) :
  AbstractSceneState(parent)
{}

void SelectSceneState::EnterState(ToolScene& scene)
{
  UNUSED_VAR(scene);
  m_lastPressedItem = 0;
  m_lastMousePosIsValid = false;
}

void SelectSceneState::ExitState(ToolScene& scene)
{
  scene.DeselectAll();
}

bool SelectSceneState::MousePressEvent(ToolScene& scene, QGraphicsSceneMouseEvent& mouseEvent)
{
  if (Qt::LeftButton == mouseEvent.button())
  {
    LOG_DEBUG("mouse press");
    QGraphicsItem* item = scene.GetGraphicsScene().itemAt(mouseEvent.scenePos(),QTransform());
    if (item)
    {
      graphics::items::AbstractItem* absItem = dynamic_cast<graphics::items::AbstractItem*>(item);
      if (absItem)
      {
        LOG_DEBUG("hit an abstract item");
        m_lastPressedItem = absItem;

        if (absItem->IsMovable())
        {
          m_lastMousePos = mouseEvent.scenePos(); // save position for moves
          m_lastMousePosIsValid = true;
        }
        return true;
      }
      else
      {
        LOG_DEBUG("hit a qgraphics item");
        return false;	// pass along the event
      }
    }
    else
    {
      // Mouse click over empty area
      scene.DeselectAll();
      return true;
    }
  }
  return false;
}

bool SelectSceneState::MouseMoveEvent(ToolScene& scene, QGraphicsSceneMouseEvent& mouseEvent)
{
  if (m_lastMousePosIsValid)
  {
    if (m_lastPressedItem)
    {
      if (!m_lastPressedItem->IsSelected())
      {
        m_lastPressedItem->GotoFirstSelectedState();
      }
      m_lastPressedItem = nullptr;
    }

    const QPointF delta =  mouseEvent.scenePos() - m_lastMousePos;
    if (scene.MoveSelectedBy(delta))
    {
      m_lastMousePos = mouseEvent.scenePos();
      //setZValue(_pendingZValue);			// this is now the top item
    }
    return true;
  }
  return false;
}

bool SelectSceneState::MouseReleaseEvent(ToolScene& /*scene*/, QGraphicsSceneMouseEvent& /*mouseEvent*/)
{
  m_lastMousePosIsValid = false;
  if (m_lastPressedItem)
  {
    m_lastPressedItem->GotoNextState();
    m_lastPressedItem =  nullptr;
    return true;
  }
  return false;
}

const char* SelectSceneState::GetStateName() const
{
  return "SelectSceneState";
}

}
}
